PACKAGE_VERSION ?= $(shell cat package.json | grep version | head -1 | awk -F: '{ print $$2 }' | sed 's/[",]//g' | tr -d '[[:space:]]')
BUILD_DATE   ?=$(shell date -u +"%Y-%m-%dT%H:%M:%SZ")
DOCKER_ARGS  ?=
DOCKER_NS    ?= registry.gitlab.com/digiresilience/waterbear/cuwei-backend
DOCKER_TAG   ?= test
DOCKER_BUILD := docker build ${DOCKER_ARGS} --build-arg BUILD_DATE=${BUILD_DATE}
DOCKER_BUILD_FRESH := ${DOCKER_BUILD} --pull --no-cache
DOCKER_BUILD_ARGS := --build-arg VCS_REF=${CI_COMMIT_SHORT_SHA}
DOCKER_PUSH  := docker push
DOCKER_BUILD_TAG := ${DOCKER_NS}:${DOCKER_TAG}

.PHONY: serve lint test fmt clean distclean build build-fresh push build-push build-fresh-push add-tag clean list

list:
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$'

build: .npmrc
	${DOCKER_BUILD} ${DOCKER_BUILD_ARGS} -t ${DOCKER_BUILD_TAG} ${PWD}

build-fresh: .npmrc
	${DOCKER_BUILD_FRESH} ${DOCKER_BUILD_ARGS} -t ${DOCKER_BUILD_TAG} ${PWD}

add-tag:
	docker pull ${DOCKER_NS}:${DOCKER_TAG}
	docker tag ${DOCKER_NS}:${DOCKER_TAG} ${DOCKER_NS}:${DOCKER_TAG_NEW}
	docker push ${DOCKER_NS}:${DOCKER_TAG_NEW}

push:
	${DOCKER_PUSH} ${DOCKER_BUILD_TAG}

build-push: build push
build-fresh-push: build-fresh push

.npmrc:
	echo '@digiresilience:registry=https://gitlab.com/api/v4/packages/npm/' > .npmrc
	echo '//gitlab.com/api/v4/packages/npm/:_authToken=${CI_JOB_TOKEN}' >> .npmrc
	echo '//gitlab.com/api/v4/projects/:_authToken=${CI_JOB_TOKEN}' >> .npmrc

start: db-up current serve

dev: db-up current serve-dev

serve:
	yarn start

serve-dev:
	yarn dev:start

lint:
	yarn lint

test: prepare-docker-test
	yarn test

ci-test: .npmrc

fmt:
	yarn fmt:all

clean:
	rm -rf coverage yarn-error.log


distclean: clean
	rm -rf node_modules

prepare-docker-test:
	docker-compose rm --force --stop -v db_test
	docker-compose up --remove-orphans --build --detach db_test
	# wait for dbs to initialize
	sleep 2

reset-docker:
	docker-compose down -v
	docker-compose up --remove-orphans --build  --detach
# wait for dbs to initialize
	sleep 1

db-up:
	docker-compose up --remove-orphans --build  --detach
	sleep 1

db-stop:
	docker-compose stop

current:
	yarn db:current

reset: reset-docker
	sleep 1
	yarn db:reset
	yarn db:current
