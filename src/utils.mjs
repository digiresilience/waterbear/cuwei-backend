/* eslint no-console: "off" */
import R from "ramda";
import RA from "ramda-adjunct";
import { logger } from "./logging.mjs";

export const isEmpty = R.either(R.isNil, R.isEmpty);
export const isNotEmpty = R.complement(isEmpty);
export const tap = msg => R.tap(v => console.log(msg, v));
export const taptap = msg => R.tap(v => console.log(msg));
export const log = (msg, v) => {
  // logger.info(msg, { v });
  console.log(msg, v);
  return v;
};
export const debug = (msg, v) => {
  logger.debug(msg, { v });
  return v;
};
export const devnull = () => {};
export const prAll = Promise.all.bind(Promise);

/**
 * `Applies `queryFunc` to every item of `list`, inside a pg-promise task.
 * Returns a promise that resolves to the mapped list
 */
export const taskBatchMap = R.curry((pgp, queryFunc, list) =>
  pgp.tx(t => t.batch(R.map(queryFunc(t), list)))
);

/**
 * Like R.mapObjIndexed, but the callback has a fourth parameter: the current index number
 */
export const mapObjIndexed = R.addIndex(R.mapObjIndexed);

/**
 * Given a list of keys, returns a function (obj) => boolean which returns true
 * if obj has non-nil values for all keys, and false otherwise.
 *
 */
export const hasAllKeys = R.curry((keys, obj) =>
  R.pipe(
    R.pickAll(keys),
    R.values,
    R.map(RA.isNotNil),
    R.reduce(R.and, true)
  )(obj)
);

/**
 * Returns the argument but with the trailing slash removed, if it exists.
 * @param {string} path a uri or path
 * @return {string}
 */
export const removeTrailingSlash = R.when(
  R.pipe(R.last, R.equals("/")),
  R.dropLast(1)
);

export const ensureProp = (prop, def) =>
  R.over(R.lensProp(prop), R.defaultTo(def));
export const groupByTrueFalse = R.curry((pred, list) =>
  R.pipe(R.groupBy(pred), ensureProp("false", []), ensureProp("true", []))(list)
);
export const addMinutes = R.curry(
  (minutes, date) => new Date(date.getTime() + minutes * 60000)
);
export const addMs = R.curry((ms, date) => new Date(date.getTime() + ms));
/*
export const filterAsync = asyncFn => async (err, x, push, next) => {
  if (err) {
    // move along
    push(err, null);
    next();
  } else if (x === highland.nil) {
    push(null, x);
  } else {
    let fnVal;
    let fnErr;
    try {
      fnVal = await asyncFn(x);
    } catch (e) {
      fnErr = e;
    }

    if (fnErr) {
      push(fnErr);
    } else if (fnVal) {
      push(null, x);
    }
    next();
  }
}
*/
