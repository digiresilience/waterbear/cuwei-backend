import R from "ramda";
import RA from "ramda-adjunct";
import got from "got";
import { removeTrailingSlash, prAll } from "../utils.mjs";

const quote = v => `"${v}"`;
const maybeQuote = R.ifElse(R.isNil, R.identity, quote);
const mapJoin = R.pipe(R.map, R.join("\n"));

export const idsLookupQuery = () => `{
    ticketStates {
      nodes {
        id
        name
      }
    }
    objectLookups {
      nodes {
        id
        name
      }
    }
    storeObjects {
        nodes {
          id
          name
          note
        }
      }
}`;

export const objectManagerAttributesQuery = objectLookupId => `{
 objectManagerAttributes(condition: {objectLookupId: ${objectLookupId}}) {
    nodes {
      id
      name
      objectLookup {
        name
        id
      }
    }
  }
}`;

//
export const ticketsByStateQuery = (
  stateId,
  fields,
  limit = 2,
  cursor = null
) => `{
    tickets(condition: {stateId: ${stateId}}, first: ${limit}, after: ${maybeQuote(
  cursor
)}, ) {
      edges {
        node {
          id
          createdAt
          ${fields}
          state {
            name
            id
          }
        ticketArticles {
          nodes {
            id
            from
            to
            subject
            body
            contentType
            createdAt
            createdById
            updatedAt
            internal
            sender {
              id
              name
            }
          }
        }
      }
      cursor
      }
      pageInfo {
        endCursor
        hasNextPage
        startCursor
        hasPreviousPage
      }
    }
  }`;

export const storesForStoreObjectQuery = storeObjectId => `{
stores(condition: {storeObjectId: ${storeObjectId}}) {
    nodes {
      id
      storeFileId
      storeObjectId
      size
      filename
      storeFile {
        sha
        id
        nodeId
      }
      updatedAt
      createdById
      createdAt
    }
  }
}`;

const singleStoreProviderDb = id => `
  file_${id}: storeProviderDb(id: ${id}) {
      sha
      data
  }`;

export const getStoresData = ids => `{
  ${mapJoin(singleStoreProviderDb, ids)}
}`;

export const storesQuery = (storeObjectId, ticketArticleId) => `{
  stores(condition: {storeObjectId: ${storeObjectId}, oId: "${ticketArticleId}"}) {
    nodes {
      id
      storeFileId
      storeObjectId
      oId
      size
      filename
      storeFile {
        sha
        id
      }
      updatedAt
      createdById
      createdAt
    }
  }
}`;

const tagsForTicketIdFragment = id => `
  ticket_${id}: tags(condition: {oId: ${id}}) {
    nodes {
      oId
      tagItem {
        name
      }
    }
  }`;

export const tagsForTickets = ids => `{
  ${mapJoin(tagsForTicketIdFragment, ids)}
}`;

export const lookupIds = graphql => async () => {
  const r = await graphql(idsLookupQuery());
  return {
    closedStateId: R.find(R.propEq("name", "closed"), r.ticketStates.nodes).id,
    ticketObjectId: R.find(R.propEq("name", "Ticket"), r.objectLookups.nodes)
      .id,
    storeObjectId: R.find(
      R.propEq("name", "Ticket::Article"),
      r.storeObjects.nodes
    ).id
  };
};

export const getTicketsByState = graphql => async (
  stateId,
  fields,
  { limit, cursor }
) => {
  return graphql(ticketsByStateQuery(stateId, fields, limit, cursor));
};

export const getObjectManagerAttributeNames = graphql => async ticketObjectId => {
  const r = await graphql(objectManagerAttributesQuery(ticketObjectId));
  return R.pluck("name", r.objectManagerAttributes.nodes);
};

export const getStoreFiles = graphql => async storeFileIds => {
  const r = await graphql(getStoresData(storeFileIds));
  return RA.renameKeysWith(RA.sliceFrom(5), r);
};

export const getTicketsTags = graphql => async ticketIds => {
  const r = await graphql(tagsForTickets(ticketIds));
  return RA.renameKeysWith(RA.sliceFrom(7), r);
};

/**
 * @typedef Attachment
 * @type {object}
 * @property {number} id
 * @property {number} storeFileId
 * @property {string} sha
 * @property {string} filename
 * @property {number} size
 * @property {string} data
 *
 */

/**
 * @param {number} ticketArticleId
 * @param {number} storeObjectId
 * @returns {Promise<Attachment[]>} list of attachment objects
 */
export const getAttachmentsForTicketArticle = graphql => async (
  storeObjectId,
  ticketArticleId
) => {
  // fetch the stores for this ticket article
  // in zammad a store is a record of a file with just its metadata
  const stores = await graphql(storesQuery(storeObjectId, ticketArticleId));
  // the "storeFileId" prop refers to the actual storeFile record containing the data
  // if an attachment is included inline and attached two stores will exist pointing to the same storeFile
  // this is why we remove duplicates here.
  const fileIds = R.uniq(R.map(R.prop("storeFileId"), stores.stores.nodes));

  if (R.isEmpty(fileIds))
    // no attachments for this ticket article
    return null;

  // fetch the actual storeFile that contains the base64 encoded data of the attachment
  const filesById = await getStoreFiles(graphql)(fileIds);

  // build up a list of attachment objects
  return R.pipe(
    R.map(
      R.pipe(
        R.pick(["filename", "storeFileId", "size"]),
        R.over(R.lensProp("size"), Number.parseInt)
      )
    ),
    R.indexBy(R.prop("storeFileId")),
    R.mergeDeepRight(filesById),
    R.values
  )(stores.stores.nodes);
};

/**
 * @param {number} ticketArticleId
 * @param {number} storeObjectId
 * @return {Promise<Object.<string, Attachment>>} list of attachment objects
 */
export const getAttachmentsForTicketArticles = graphql => async (
  storeObjectId,
  articleIds
) =>
  R.pipe(
    R.map(id => getAttachmentsForTicketArticle(graphql)(storeObjectId, id)),
    prAll,
    R.andThen(
      R.pipe(
        R.zipObj(articleIds)
        // R.reject(R.isNil)
      )
    )
  )(articleIds);

export const getTagsForTickets = graphql => async ticketIds => {
  const result = await getTicketsTags(graphql)(ticketIds);

  const tagsById = R.map(
    R.pipe(R.prop("nodes"), R.map(R.path(["tagItem", "name"]))),
    result
  );
  return tagsById;
};

/**
 * @typedef ZammadGraphql
 * @type {object}
 */

/**
 * Zammad graphql client wrapper
 * @type {ZammadGraphql}
 */
export const ZammadGraphql = graphql => ({
  lookupIds: lookupIds(graphql),
  getTicketsByState: getTicketsByState(graphql),
  getObjectManagerAttributeNames: getObjectManagerAttributeNames(graphql),
  getStoreFiles: getStoreFiles(graphql),
  getAttachmentsForTicketArticle: getAttachmentsForTicketArticle(graphql),
  getAttachmentsForTicketArticles: getAttachmentsForTicketArticles(graphql),
  getTagsForTickets: getTagsForTickets(graphql)
});

export const setTicketState = client => async (state, id) =>
  client.put(`tickets/${id}`, { json: { id, state } }).json();

export const ZammadApi = (url, headers) => {
  const client = got.extend({
    prefixUrl: `${removeTrailingSlash(url)}/api/v1`,
    responseType: "json",
    requestType: "json",
    headers
  });
  return {
    setTicketState: setTicketState(client)
  };
};
