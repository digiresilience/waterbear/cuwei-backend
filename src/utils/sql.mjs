import path from "path";
import fs from "fs";
import dirname from "../dirname.mjs";

export const migrations = () => {
  return path.resolve(dirname(import.meta.url), "../../sql/migrations");
};
export const sql = file => {
  const p = path.resolve(dirname(import.meta.url), "../../sql/", file);
  return fs.readFileSync(p, "utf8");
};
