import highland from "highland";
/**
 *
 * Creates a highlandjs Generator function for iterating through pages
 * (presumably from an http api), and pushing them into a Stream.
 *
 * https://highlandjs.org/#_(source)
 *
 * This function uses an opaque page object as the representation of the page.
 * You define what that is, it could be a simple page number or complex object.
 *
 * The semantics are as follows:
 *
 * - `nextPage` is a function that returns the opaque page data. When
 *    called with no arguments, it should return the page data for the first
 *    page. When called with the page data it should return the page data for
 *    the next page. If there are no more pages, it should return false.
 *
 * - `fetchPage` is a function that accepts the opaque page data and returns a Promise resolving to the response.
 *
 * - `processResponse` is a function that accepts `push` and `response`. The
 *    function should process the `response` data and `push(null, item)` for all
 *    items in the response that should go into the stream. It returns a promise
 *    that resolves once the processing is complete.
 *
 * @param {function} nextPage
 * @param {function} fetchPage
 * @param {function} processResponse
 * @returns {function}
 */
const paginated = (nextPage, fetchPage, processResponse) => {
  let page = nextPage();
  return async (push, next) => {
    const response = await fetchPage(page);
    await processResponse(push, response);
    page = nextPage(response);
    if (page) {
      next();
    } else {
      // terminate the stream
      push(null, highland.nil);
    }
  };
};

export default paginated;
