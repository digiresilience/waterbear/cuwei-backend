import R from "ramda";
import got from "got";
import { logger } from "../logging.mjs";

const extractMessage = response => {
  try {
    return response.errors[0].message;
  } catch (e) {
    return `GraphQL Error (Code: ${response.status})`;
  }
};

export class ClientError extends Error {
  constructor(response, request) {
    const message = extractMessage(response);

    super(message);

    this.response = response;
    this.request = request;

    Error.captureStackTrace(this, ClientError);
  }
}

export const graphqlFactory = (endpoint, headers, clientDefault = null) => {
  const client =
    clientDefault ||
    got.extend({
      /*
      hooks: {
        beforeRedirect: [
          (options, response) =>
            console.log("GREPMEbeforeRedirect", options, response)
        ],
        beforeRetry: [
          (options, error, retryCount) =>
            console.log("GREPMEbeforeRetry", options, error, retryCount)
        ],
        beforeRequest: [options => console.log("GREPMEbeforeRequest", options)],
        afterResponse: [
          response => {
            console.log("GREPMEafterResponse", response);
            return response;
          }
        ],
        beforeError: [error => console.log("GREPMEbeforeError", error)]
      },
      */
      timeout: 60 * 1000,
      retries: 0,
      responseType: "json",
      requestType: "json",
      headers
    });
  // eslint-disable-next-line consistent-return
  return async (query, variables) => {
    try {
      // logger.debug("graphql request: ", { endpoint, query, variables });
      const response = await client
        .post(endpoint, {
          json: {
            query
          }
        })
        .json();
      return response.data;
    } catch (e) {
      if (!e.response) {
        logger.debug(
          "graphql request failed",
          R.mergeRight(headers, { endpoint, query })
        );
        logger.error(e);
        throw e;
      } else {
        const result = e.response.body;
        const errorResponse =
          typeof result === "string" ? { error: result } : result;
        throw new ClientError(
          { ...errorResponse, status: e.response.statusCode },
          { query, variables }
        );
      }
    }
  };
};
