import fs from "fs";
import path from "path";
import dirname from "./dirname.mjs";

export default function logo() {
  return fs.readFileSync(
    path.join(dirname(import.meta.url), "logo.ans"),
    "utf8"
  );
}
