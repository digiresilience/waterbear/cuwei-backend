import graphileWorker from "graphile-worker";
import R from "ramda";
import { logger } from "./logging.mjs";
import config from "./config.mjs";
import zammadSyncTask from "./tasks/zammad-sync.mjs";
import exportTicketDataTask from "./tasks/export-ticket-data.mjs";

const { Logger, run, quickAddJob } = graphileWorker;

const logFactory = scope => (level, message, meta) => {
  let extras = meta;
  if (meta) {
    const { job } = meta;
    if (job) {
      extras = R.omit(["payload"], R.mergeLeft(R.omit(["job"], meta), job));
    }
  }
  logger.log(level, message, extras);
};

async function scheduleZammadSync() {
  logger.debug("scheduling zammad sync job");
  await quickAddJob(
    {},
    "zammadSyncTask",
    {},
    {
      maxAttempts: 5,
      jobKey: "zammad-sync"
    }
  );
}
let zammadSyncTimer = null;
export async function start() {
  const concurrency = config.get("worker.concurrency");
  const pollInterval = config.get("worker.pollInterval");
  logger.info("Starting worker", { concurrency, pollInterval });
  const runner = await run({
    concurrency,
    pollInterval,
    logger: new Logger(logFactory),
    connectionString: config.get("worker.connection"),
    taskList: {
      zammadSyncTask,
      exportTicketDataTask
    }
  });
  logger.debug("Running zammd sync with interval", {
    zammadSyncInterval: config.get("worker.zammadSyncInterval")
  });
  zammadSyncTimer = setInterval(
    scheduleZammadSync,
    config.get("worker.zammadSyncInterval")
  );
  return runner;
}

export async function stop(runner) {
  clearInterval(zammadSyncTimer);
  runner.stop();
}

export async function startWorker() {
  start().catch(err => {
    logger.error("error caught exiting", err);
    process.exit(1);
  });
}
