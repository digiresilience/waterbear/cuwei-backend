import Koa from "koa";
import bodyParser from "koa-bodyparser";
import serve from "koa-static";
import mount from "koa-mount";
import prometheus from "@echo-health/koa-prometheus-exporter";
import R from "ramda";
import dirname from "./dirname.mjs";
import config from "./config.mjs";
import routes from "./routes.mjs";
import logo from "./logo.mjs";
import { logger, requestLogger } from "./logging.mjs";
import cors from "./middleware/cors.mjs";
import postgraphileMiddleware from "./middleware/postgraphile.mjs";
import cloudflareAccess from "./middleware/cloudflare-access.mjs";
import requestId from "./middleware/request-id.mjs";
import requestLoggerMiddleware from "./middleware/request-logger.mjs";
import installDatabasePools from "./middleware/database-pools.mjs";
import installDataModels from "./middleware/data-models.mjs";
import installSessionMiddleware from "./middleware/sessions.mjs";

// import errorHandler from "./middleware/error-handler.mjs";

class App extends Koa {
  constructor(...params) {
    super(...params);
    this.servers = [];
  }

  listen(...args) {
    const server = super.listen(...args);
    this.servers.push(server);
    return server;
  }

  terminate() {
    // this.context.rootPgPool.end()
    R.forEach((s) => s.close(), this.servers);
  }
}

export function setupApp() {
  const app = new App();

  // register prometheus metric tracking middleware
  app.use(prometheus.httpMetricMiddleware());

  app.logger = logger;
  // register requestId middleware
  app.use(requestId());

  app.use(cors());

  // default error handler, renders nice errors
  //  app.use(errorHandler());

  app.use(requestLoggerMiddleware(app, requestLogger, config.isProd));

  installDatabasePools(app);
  installDataModels(app);

  /*
  app.use(async (ctx, next) => {
    return next();
  });
  */

  // validate cloudflare authorization cookie
  cloudflareAccess(app, config.get("cfaccess"));

  installSessionMiddleware(app);

  // trust proxy
  app.proxy = true;

  // parse the body of json and form requests and make the available at ctx.request.body
  app.use(bodyParser());

  // postgraphile
  app.use((ctx, next) => {
    // PostGraphile deals with (req, res) but we want access to sessions from
    // `pgSettings`, so we make the ctx available on req.
    ctx.req.ctx = ctx;
    return next();
  });

  app.use(
    postgraphileMiddleware({
      ownerPgPool: app.context.ownerPgPool,
      schema: config.get("postgraphile.schema"),
      pool: app.context.graphqlPgPool,
      options: config.get("postgraphile.options"),
    })
  );

  // serve static assets
  app.use(mount("/assets", serve(`${dirname(import.meta.url)}/assets`)));

  // configure routes
  routes(app);

  if (!config.isTest) {
    // eslint-disable-next-line no-console
    console.log(logo());
    // eslint-disable-next-line no-console
    console.log(`
          the waterbear is awake`);
    // eslint-disable-next-line no-console
    console.log(`               version: ${config.get("app_version")}
`);
  }

  return app;
}

export function start() {
  const { host, port } = config.get("server");
  const app = setupApp();
  app.logger.info(
    `${
      config.get("site").name
    } and metrics are now listening on port ${host}:${port}`
  );
  return app.listen(port, host);
}
