/* eslint camelcase: "off" */
const selectProps = [
  "id",
  "origin",
  "total_processed",
  "total_new",
  "total_duplicates",
  "total_errors",
  "started_at",
  "ended_at",
  "created_at",
  "updated_at"
];

const table = "wb_public.batches";
const selectCols = selectProps.join(",");

export const findById = db => id =>
  db.oneOrNone(`SELECT ${selectCols} FROM ${table} WHERE id = $1`, [id]);

export const startBatch = db => batch =>
  db.one(
    `INSERT INTO ${table} (origin, started_at) VALUES ($<origin>, $<startedAt>)
          RETURNING *`,
    batch
  );

export const finishBatch = db => batch =>
  db.one(
    `UPDATE ${table} SET total_processed = $<totalProcessed>,
                         total_new = $<totalNew>,
                         total_duplicates = $<totalDuplicates>,
                         total_errors = $<totalErrors>,
                         ended_at     = $<endedAt>
                     WHERE id = $<id>
          RETURNING *`,
    batch
  );

export const Batches = db => {
  return {
    findById: findById(db),
    startBatch: startBatch(db),
    finishBatch: finishBatch(db)
  };
};
