import R from "ramda";

// const log = msg => R.tap(x => console.log(msg, x));
// const isError = e => e instanceof Error;
// const hasError = o => R.complement(R.isNil(o.error))
const hasError = o => R.not(R.isNil(o.error));

export const makeHandler = (validator, operation) =>
  R.pipe(
    validator,
    // log("aftervalid"),
    R.ifElse(hasError, R.prop("error"), R.pipe(R.prop("value"), operation))
  );

const genericValidator = (schema, props) =>
  schema.validate(props, { abortEarly: false });

export const makeValidator = R.curry(genericValidator);

export const validatingHandler = (schema, operation) =>
  makeHandler(makeValidator(schema), operation);
