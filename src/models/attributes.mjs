/* eslint camelcase: "off" */

const selectProps = [
  "id",
  "metadata",
  "value",
  "value_edited",
  "value_binary",
  "value_binary_edited",
  "is_binary",
  "is_held",
  "event_id",
  "field_id",
  "field_option_id",
  "field_option_id_edited",
  "created_at",
  "updated_at"
];

const table = "wb_public.attributes";
const selectCols = selectProps.join(",");

export const findById = db => id =>
  db.oneOrNone(`SELECT ${selectCols} FROM ${table} WHERE id = $1`, [id]);

export const findByEventId = db => id =>
  db.oneOrNone(`SELECT ${selectCols} FROM ${table} WHERE event_id = $1`, [id]);

export const insertAttribute = db => attr =>
  db.one(
    `INSERT INTO ${table} (metadata, value, value_binary, is_binary, event_id, field_id, field_option_id)
            VALUES ($<metadata>, $<value>, $<value_binary>, $<is_binary>, $<event_id>, $<field_id>, $<field_option_id>)
          RETURNING *`,
    attr
  );

export const Attributes = db => {
  return {
    findById: findById(db),
    findByEventId: findByEventId(db),
    insertAttribute: insertAttribute(db)
  };
};
