/* eslint camelcase: "off" */
import R from "ramda";

const selectProps = ["id", "name", "display_name", "created_at", "updated_at"];

const table = "wb_public.tags";
const selectCols = selectProps.join(",");

export const findTagById = db => id =>
  db.oneOrNone(`SELECT ${selectCols} FROM ${table} WHERE id = $1`, [id]);

export const findTagByName = R.curry((db, name) =>
  db.oneOrNone(`SELECT ${selectCols} FROM ${table} WHERE name = $1`, [name])
);

export const insertTag = R.curry((db, tag) =>
  db.one(
    `INSERT INTO ${table} (name, display_name)
            VALUES ($<name>, $<displayName>)
          RETURNING *`,
    tag
  )
);

export const Tags = db => {
  return {
    findById: findTagById(db),
    findByName: findTagByName(db),
    insertTag: insertTag(db)
  };
};

/*
const selectPropsET = [
  "tag_id",
  "event_id",
  "created_at",
  "updated_at"
];
*/

const tableET = "wb_public.events_tags";
// const selectColsET = selectProps.join(",");

export const insertEventTag = R.curry((db, eventTag) =>
  db.one(
    `INSERT INTO ${tableET} (tag_id, event_id)
            VALUES ($<tagId>, $<eventId>)
          RETURNING *`,
    eventTag
  )
);

export const EventTags = db => {
  return {
    insertEventTag: insertEventTag(db)
  };
};
