/* eslint camelcase: "off" */
/* eslint no-underscore-dangle: "off" */
import R from "ramda";

const selectProps = [
  "id",
  "name",
  "field_type",
  "display_name",
  "description",
  "is_archived",
  "created_at",
  "updated_at"
];

const selectCols = selectProps.join(",");

const selectPropsOptions = [
  "id",
  "name",
  "display_name",
  "description",
  "is_archived",
  "created_at",
  "updated_at"
];

const selectColsOptions = selectPropsOptions.join(",");
export const findById = db => id =>
  db.oneOrNone(`SELECT ${selectCols} FROM wb_public.fields WHERE id = $1`, [
    id
  ]);

export const findByName = R.curry((db, name) =>
  db.oneOrNone(`SELECT ${selectCols} FROM wb_public.fields WHERE name = $1`, [
    name
  ])
);

export const findFieldOptionByNameAndField = R.curry((db, field_id, name) =>
  db.oneOrNone(
    `SELECT ${selectColsOptions} FROM wb_public.field_options WHERE name = $1 AND field_id = $2`,
    [name, field_id]
  )
);

export const saveField = db => field =>
  db.one(
    `INSERT INTO wb_public.fields(name, field_type, display_name, description) VALUES ($<name>, $<field_type>, $<display_name>, $<description>)
            ON CONFLICT (name)
            DO UPDATE SET display_name = EXCLUDED.display_name,
                          description  = EXCLUDED.description,
                          field_type   = EXCLUDED.field_type
          RETURNING *`,
    field
  );

export const saveFieldOption = db => fieldOpt =>
  db.one(
    `INSERT INTO wb_public.field_options(name, display_name, description, field_id) VALUES ($<name>, $<display_name>, $<description>, $<field_id>)
            ON CONFLICT (name, field_id)
            DO UPDATE SET display_name = EXCLUDED.display_name,
                          description  = EXCLUDED.description
          RETURNING *`,
    fieldOpt
  );

export const Fields = db => {
  return {
    saveField: saveField(db),
    saveFieldOption: saveFieldOption(db),
    findById: findById(db),
    findByName: findByName(db)
  };
};
