/* eslint camelcase: "off" */
import R from "ramda";
import { debug } from "../utils.mjs";

const selectProps = [
  "id",
  "reported_at",
  "event_number",
  "is_locked",
  "is_archived",
  "is_held",
  "is_approved",
  "batch_id",
  "checked_out_at",
  "checked_out_user_id",
  "import_id",
  "created_at",
  "updated_at"
];

const selectCols = selectProps.join(",");

export const findEventById = R.curry((db, id) =>
  db.oneOrNone(`SELECT ${selectCols} FROM wb_public.events WHERE id = $1`, [id])
);

export const findEventByImportId = R.curry((db, importId) =>
  db.oneOrNone(
    `SELECT ${selectCols} FROM wb_public.events WHERE import_id = $1`,
    [importId]
  )
);

export const insertEvent = R.curry((db, event) =>
  db.one(
    debug(
      "insertEvent",
      `INSERT INTO wb_public.events(reported_at, batch_id, import_id) VALUES ($<reported_at>, $<batch_id>, $<import_id>)
          RETURNING *`
    ),
    event
  )
);

export const Events = db => {
  return {
    insertEvent: insertEvent(db),
    findById: findEventById(db),
    findByImportId: findEventByImportId(db)
  };
};
