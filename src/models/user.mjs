/* eslint camelcase: "off" */
import joi from "@hapi/joi";
import R from "ramda";
import { validatingHandler, makeValidator } from "./base.mjs";

const schema = joi.object({
  index: joi.number().integer(),
  name: joi.string(),
  avatar_url: joi.string(),
  user_role: joi.string(),
  is_active: joi.boolean(),
  filter_tags: joi.string(),
  email: joi.string().email().required(),
  created_at: joi.number().integer(),
  created_by: joi.string(),
});

const pickProps = (props) => R.when(R.complement(R.isNil), R.pick(props));

const sessionProps = ["email", "userRole", "id", "isActive"];
const pickSessionProps = pickProps(sessionProps);

const User = (db) => {
  const selectProps = [
    "id",
    "email",
    "name",
    "user_role",
    "is_active",
    "filter_tags",
    "avatar_url",
    "created_by",
    "created_at",
    "updated_at",
  ];
  const selectCols = selectProps.join(",");
  const findByEmailWithInactive = (email) =>
    db.oneOrNone(
      `SELECT  ${selectCols}
      FROM wb_public.users WHERE email = $1`,
      [email]
    );

  const findByEmail = (email) =>
    db.oneOrNone(
      `SELECT ${selectCols}
      FROM wb_public.users WHERE email = $1 AND is_active = true`,
      [email]
    );

  const findById = (userId) =>
    db.oneOrNone(`SELECT ${selectCols} FROM wb_public.users WHERE id = $1`, [
      userId,
    ]);

  const resolveUserForPostgraphql = async (cfEmail) =>
    pickSessionProps(await findByEmail(cfEmail));

  const create = validatingHandler(schema, (props) =>
    db.one(
      `INSERT INTO wb_public.users (email, name, is_active, user_role, created_by)
          VALUES ($<email>, $<name>, $<is_active>, $<user_role>, $<created_by>)
          RETURNING *`,
      props
    )
  );

  const count = async () =>
    parseInt(
      (await db.oneOrNone(`SELECT count(id) FROM wb_public.users`)).count,
      10
    );

  return {
    props: selectProps,
    validate: makeValidator(schema),
    create,
    count,
    findById,
    findByEmailWithInactive,
    findByEmail,
    resolveUserForPostgraphql,
  };
};

export default User;
