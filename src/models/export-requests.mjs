/* eslint camelcase: "off" */
import R from "ramda";
import { debug } from "../utils.mjs";

const selectProps = [
  "id",
  "requested_at",
  "completed_at",
  "export_request_id",
  "requesting_user_id",
  "download_url",
  "status",
  "created_at",
  "updated_at",
];

const selectCols = selectProps.join(",");

export const findExportRequestById = R.curry((db, id) =>
  db.oneOrNone(
    `SELECT ${selectCols} FROM wb_public.export_requests WHERE id = $1`,
    [id]
  )
);

export const updateExportRequest = R.curry((db, exportRequest) =>
  db.one(
    debug(
      "updateExportRequest",
      `UPDATE wb_public.export_requests SET status = $<status>, completed_at = $<completedAt>, download_url = $<downloadURL> WHERE id = $<id>
          RETURNING *`
    ),
    exportRequest
  )
);

export const ExportRequests = (db) => {
  return {
    findById: findExportRequestById(db),
    update: updateExportRequest(db),
  };
};
