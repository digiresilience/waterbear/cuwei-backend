/* eslint camelcase: "off" */
import joi from "@hapi/joi";
import { makeValidator } from "./base.mjs";

const schema = joi.object({
  uuid: joi.string().guid(),
  user_id: joi.number().integer(),
  created_at: joi.date().timestamp(),
  last_active_at: joi.date().timestamp()
});

const Session = db => {
  // maybe someday use an upsert here instead?

  const findSession = userId =>
    db.oneOrNone("select uuid from wb_private.sessions where user_id = $1", [
      userId
    ]);

  const createSession = userId =>
    db.one(
      "insert into wb_private.sessions (user_id) values ($1) returning uuid",
      [userId]
    );

  const findOrCreateSession = async userId => {
    const session = await findSession(userId);
    if (session) return session.uuid;
    const { uuid } = await createSession(userId);
    return uuid;
  };

  return {
    validate: makeValidator(schema),
    findSession,
    createSession,
    findOrCreateSession
  };
};

export default Session;
