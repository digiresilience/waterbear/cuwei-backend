import Router from "koa-router";
import main from "./controllers/main.mjs";

function init(app) {
  const router = new Router();

  router.get("/", main.index);
  app.use(router.routes());
  app.use(router.allowedMethods());
}

export default init;
