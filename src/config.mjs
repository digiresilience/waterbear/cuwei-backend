/* eslint no-console: "off" */
import PgSimplifyInflectorPlugin from "@graphile-contrib/pg-simplify-inflector";
import PgManyToManyPlugin from "@graphile-contrib/pg-many-to-many";
import ConnectionFilterPlugin from "postgraphile-plugin-connection-filter";
import GraphileBuild from "graphile-build";
import dotenv from "dotenv";
import dotenvExpand from "dotenv-expand";
import convict from "convict";
import convictValidator from "convict-format-with-validator";
import path from "path";
import handleErrors from "./utils/graphile-errors.mjs";
import dirname from "./dirname.mjs";

if (!process.env.NODE_ENV) {
  process.env.NODE_ENV = "development";
  console.log("Config assuming development mode.");
}

const { NodePlugin } = GraphileBuild;
if (process.env.NODE_ENV === "test") {
  dotenvExpand(
    dotenv.config({ path: path.resolve(process.cwd(), "db/env-test") })
  );
} else if (process.env.NODE_ENV === "development") {
  dotenvExpand(dotenv.config());
} // we do not load env files in prod

convict.addFormats(convictValidator);

const config = convict({
  site: {
    name: {
      doc: "Application name",
      format: String,
      env: "npm_package_name",
      default: null,
    },
  },
  app_version: {
    doc: "The current application version",
    format: String,
    env: "npm_package_version",
    default: null,
  },
  env: {
    doc: "The application environment.",
    format: ["production", "development", "test"],
    default: "development",
    env: "NODE_ENV",
  },
  cors: {
    allowedMethods: {
      doc: "The allowed CORS methods",
      format: "Array",
      env: "CORS_ALLOWED_METHODS",
      default: ["GET", "PUT", "POST", "PATCH", "DELETE", "HEAD", "OPTIONS"],
    },
    allowedOrigins: {
      doc: "The allowed origins",
      format: "Array",
      env: "CORS_ALLOWED_ORIGINS",
      default: [],
    },
    allowedHeaders: {
      doc: "The allowed headers",
      format: "Array",
      env: "CORS_ALLOWED_HEADERS",
      default: [
        "content-type",
        "authorization",
        "cf-access-authenticated-user-email",
      ],
    },
  },
  logging: {
    level: {
      doc: "The logging level",
      format: String,
      default: "info",
      env: "LOG_LEVEL",
    },
    sql: {
      doc: "Whether to debug sql statements or not",
      format: "Boolean",
      default: false,
      env: "LOG_SQL",
    },
  },
  server: {
    host: {
      doc: "The IP address to bind to",
      format: "ipaddress",
      default: "127.0.0.1",
      env: "HOST",
    },
    port: {
      doc: "The port to bind.",
      format: "port",
      default: 3000,
      env: "PORT",
      arg: "port",
    },
  },
  worker: {
    connection: {
      doc: "The postgres connection url for the worker database.",
      format: String,
      default: null,
      env: "WORKER_DATABASE_URL",
    },
    concurrency: {
      doc: "The number of jobs to run concurrently",
      default: 1,
      format: "Number",
      env: "WORKER_CONCURRENT_JOBS",
    },
    pollInterval: {
      doc:
        "How long to wait between polling for jobs in milliseconds (for jobs scheduled in the future/retries)",
      default: 2000,
      format: "Number",
      env: "WORKER_POLL_INTERVAL_MS",
    },
    zammadSyncInterval: {
      doc:
        "The interval at which the zammad sync job will be run in milliseconds",
      default: 5 * 60000,
      format: "Number",
      env: "ZAMMAD_SYNC_INTERVAL_MS",
    },
  },
  db: {
    name: {
      doc: "The postgres database name",
      format: String,
      default: "waterbear",
      env: "DATABASE_NAME",
    },
    owner: {
      doc: "The postgres owner role",
      format: String,
      default: "waterbear",
      env: "DATABASE_OWNER",
    },
    connection: {
      doc:
        "The postgres connection url that can access everything in the waterbear database. Must not be superuser.",
      format: String,
      default: null,
      env: "DATABASE_URL",
    },
    /*
    shadowConnection: {
      doc: "The postgres connection url used by the migration script, database should not exist",
      format: String,
      default: null,
      env: "SHADOW_DATABASE_URL"
    },
    */
    rootConnection: {
      doc:
        "The postgres root/superuser connection url for testing only, database must NOT be the app database.",
      format: String,
      default: "",
      env: "ROOT_DATABASE_URL",
    },
  },
  postgraphile: {
    auth: {
      doc: "The postgres role that postgraphile logs in with",
      format: String,
      default: "waterbear_graphile_auth",
      env: "DATABASE_AUTHENTICATOR",
    },
    appRootConnection: {
      doc:
        "The postgres root/superuser connection url for development mode so PG can watch the schema changes, this is strangely named in the postgraphile API 'ownerConnectionString'",
      format: String,
      default: null,
      env: "APP_ROOT_DATABASE_URL",
    },
    authConnection: {
      doc:
        "The postgres connection URL for postgraphile, must not be superuser and must have limited privs.",
      format: String,
      default: null,
      env: "DATABASE_AUTH_URL",
    },
    visitor: {
      doc: "The postgres role that postgraphile switches to",
      format: String,
      default: "wb_postgraphile",
      env: "DATABASE_VISITOR",
    },
    schema: {
      doc: "The schema postgraphile should expose with graphql",
      format: String,
      default: "wb_public",
    },
    enableGraphiql: {
      doc: "Whether to enable the graphiql web interface or not",
      format: "Boolean",
      default: false,
      env: "ENABLE_GRAPHIQL",
    },
  },
  cfaccess: {
    url: {
      doc: "The Cloudflare Access Login Page url, no trailing slash",
      format: "url",
      env: "CLOUDFLARE_ACCESS_URL",
      default: null,
    },
    audience: {
      doc: "The Cloudflare Access audience id for this application",
      format: String,
      env: "CLOUDFLARE_ACCESS_AUDIENCE",
      default: null,
    },
  },
  zammadApi: {
    url: {
      doc: "The zammad rest api base url",
      format: "url",
      env: "ZAMMAD_API_URL",
      default: null,
    },
    token: {
      doc: "The zammad rest api token",
      format: String,
      env: "ZAMMAD_API_TOKEN",
      default: null,
    },
    sentState: {
      doc: "The zammad sent state",
      format: String,
      env: "ZAMMAD_API_SENT_STATE",
      default: "sent",
    },
  },
  zammadGraphql: {
    url: {
      doc: "The zammad graphql endpoint url",
      format: "url",
      env: "ZAMMAD_GRAPHQL_URL",
      default: null,
    },
    serviceTokenId: {
      doc:
        "The Cloudflare Access Service Token Client Id to access the graphql service",
      format: String,
      env: "CLOUDFLARE_ACCESS_CLIENT_ID",
      default: null,
    },
    serviceTokenSecret: {
      doc:
        "The Cloudflare Access Service Token Client secret to access the graphql service",
      format: String,
      env: "CLOUDFLARE_ACCESS_CLIENT_SECRET",
      default: null,
    },
  },
});

const env = config.get("env");
config.isProd = env === "production";
config.isTest = env === "test";
config.isDev = env === "development";

// load config from config files if provided
if (config.isTest) {
  config.loadFile(`${process.env.PWD}/test/config.test.json`);
}

config.validate({ allowed: "strict" });

// add computed values
config.set(
  "cfaccess.jwksUrl",
  `${config.get("cfaccess.url")}/cdn-cgi/access/certs`
);

if (config.isDev || config.isTest) {
  const devAllowedOrigins = ["http://localhost:3000", "http://127.0.0.1:3000"];
  const passedOrigins = config.get("cors.allowedOrigins");
  config.set("cors.allowedOrigins", devAllowedOrigins.concat(passedOrigins));
}

config.set("postgraphile.options", {
  ownerConnectionString: config.get("postgraphile.appRootConnection"),
  subscriptions: false,
  retryOnInitFail: !config.isTest,
  enableQueryBatching: true,
  dynamicJson: true,
  ignoreRBAC: false,
  ignoreIndexes: false,
  setofFunctionsContainNulls: false,
  graphiql: config.isDev || config.get("postgraphile.enableGraphiql"),
  enhanceGraphiql: true,
  allowExplain: config.isDev,
  enableCors: false,
  disableQueryLog: true,
  handleErrors: handleErrors(config.isDev, config.isTest),
  watchPg: config.isDev,
  sortExport: true,
  exportGqlSchemaPath: config.isDev
    ? `${dirname(import.meta.url)}/../data/schema.graphql`
    : undefined,
  classicIds: false,
  appendPlugins: [
    PgSimplifyInflectorPlugin,
    PgManyToManyPlugin,
    ConnectionFilterPlugin,
  ],
  skipPlugins: config.isDev ? [NodePlugin] : [],
  graphileBuildOptions: {
    /*
     * Any properties here are merged into the settings passed to each Graphile
     * Engine plugin - useful for configuring how the plugins operate.
     */
  },
});

// extendedErrors: ['severity', 'code', 'detail', 'hint', 'position', 'internalPosition', 'internalQuery', 'where', 'schema', 'table', 'column', 'dataType', 'constraint', 'file', 'line', 'routine'],
// showErrorStack: config.isDev || config.isTest,
export default config;
