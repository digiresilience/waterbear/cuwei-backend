/* eslint no-underscore-dangle: "off" */
/* eslint no-param-reassign: "off" */
import R from "ramda";
import RA from "ramda-adjunct";
import highland from "highland";
import waterbearFields from "@digiresilience/waterbear-fields";
import waterbearFieldsFuncs from "@digiresilience/waterbear-fields/src/fields.js";
import camelCase from "camelcase";
import decamelize from "decamelize";
import assert from "assert";
import FileType from "file-type";
import pMemoize from "p-memoize";

import { ZammadApi, ZammadGraphql } from "../zammad/queries.mjs";

import { initializePGP } from "../db.mjs";
import config from "../config.mjs";
import paginated from "../utils/api-page-stream.mjs";
import { graphqlFactory } from "../utils/graphql-client.mjs";
import {
  saveField,
  saveFieldOption,
  findByName as findFieldByName,
  findFieldOptionByNameAndField
} from "../models/fields.mjs";
import { insertEvent, findEventByImportId } from "../models/events.mjs";
import { insertAttribute } from "../models/attributes.mjs";
import { findTagByName, insertTag, insertEventTag } from "../models/tags.mjs";
import { Batches } from "../models/batch.mjs";
import { logger } from "../logging.mjs";
import { hasAllKeys, mapObjIndexed, taskBatchMap, prAll } from "../utils.mjs";

const { wrapAsync } = highland;
const { FieldsV1 } = waterbearFields;

const {
  getSubmitterFields,
  getRequiredFieldIds,
  getFieldsByProp,
  getFieldIds,
  getFieldById
} = waterbearFieldsFuncs;

/**
 * Creates an iterator Stream of Zammad tickets.
 *
 * https://highlandjs.org/#_(source)
 *
 * @param {object} config the config params
 * @param {ZammadGraphql} config.zammad the Zammad graphql client
 * @param {integer} config.limit the number of items per page
 * @param {integer} config.stateId filters tickets by the state
 * @param {string} config.graphqlFields the ticket fields to fetch in the graphql query
 * @returns {Stream}
 */
const ticketStream = ({ zammad, limit, stateId, graphqlFields }) => {
  const fetchPage = async page => {
    logger.debug("Zammad TicketStream fetchPage", page);
    const response = await zammad.getTicketsByState(
      stateId,
      graphqlFields,
      page
    );
    return response;
  };

  const nextPage = response => {
    if (R.isNil(response)) return { limit, cursor: null };
    logger.debug("Zammad TicketStream nextPage", response.tickets.pageInfo);
    if (response.tickets.pageInfo.hasNextPage)
      return {
        limit,
        cursor: response.tickets.pageInfo.endCursor
      };
    return false;
  };

  const processResponse = async (push, response) => {
    R.forEach(
      item => push(null, item),
      R.pluck("node", response.tickets.edges)
    );
  };

  return highland(paginated(nextPage, fetchPage, processResponse));
};

export const castArrays = fields => {
  const arrayFields = RA.concatAll([
    getFieldsByProp("type", "multiselect")(fields),
    getFieldsByProp("type", "stringlist")(fields),
    getFieldsByProp("type", "select")(fields)
  ]);
  const multiSelects = getFieldIds(arrayFields);

  const cast = R.pipe(
    R.tryCatch(JSON.parse, (_, b) => b),
    R.when(R.isEmpty, R.always([])),
    R.unless(R.either(R.is(Array), R.isNil), R.curry(Array.of))
  );

  const xforms = R.zipObj(multiSelects, R.map(R.always(cast), multiSelects));

  return R.evolve(xforms);
};

/*
 * we ignore these fields because they are handled separately in zammad
 *  - comments are articles
 *  - attachments are separate
 *  - submitterEmail is just ignored
 */
const ignoredFieldSpecIds = ["comments", "attachments", "submitterEmail"];
const knownFieldSpecIds = R.without(ignoredFieldSpecIds, getFieldIds(FieldsV1));

const fieldToAttrs = R.curry((fieldMapping, ticket, fieldSpecId) => {
  const values = ticket[fieldSpecId];
  if (!values) {
    return [];
  }

  const { id, type, specType, options } = fieldMapping[fieldSpecId];
  assert(RA.isNotNilOrEmpty(id), "sanity check: field_id is null");
  assert(RA.isNotNilOrEmpty(specType), "sanity check: specType is null");
  if (type === "option") {
    assert(RA.isNotNilOrEmpty(options), "sanity check: options is null");
    return R.map(
      v => ({
        metadata: null,
        value: null,
        value_binary: null,
        is_binary: false,
        event_id: null,
        field_id: id,
        field_option_id: options[v]
      }),
      values
    );
  }
  if (specType === "stringlist") {
    return R.map(
      v => ({
        metadata: null,
        value: v,
        value_binary: null,
        is_binary: false,
        event_id: null,
        field_id: id,
        field_option_id: null
      }),
      values
    );
  }

  return [
    {
      metadata: null,
      value: values,
      value_binary: null,
      is_binary: false,
      event_id: null,
      field_id: id,
      field_option_id: null
    }
  ];
});

const toAttrs = (fieldMapping, ticket) => {
  return R.chain(fieldToAttrs(fieldMapping, ticket), knownFieldSpecIds);
};
const ticketToEventAttrs = (batchId, fieldMapping) => ticket => ({
  ticket,
  event: {
    reported_at: ticket.created_at,
    batch_id: batchId,
    import_id: ticket.id
  },
  attributes: toAttrs(fieldMapping, ticket)
});

const articleToCommentAttr = commentFieldId => article => ({
  metadata: {
    author: article.from,
    commented_at: article.createdAt
  },
  value: article.body,
  value_binary: null,
  is_binary: false,
  event_id: null,
  field_id: commentFieldId,
  field_option_id: null
});

/**
 * Converts ticket articles into attributes
 */
export const ticketArticlesToEventAttrs = fieldMapping => bundle => {
  const articles = R.tail(bundle.ticket.ticket_articles.nodes);
  if (R.isEmpty(articles)) return bundle;

  const commentFieldId = fieldMapping.comments.id;

  assert(RA.isNotNil(commentFieldId), "sanity check: field_id is null");
  const comments = R.map(articleToCommentAttr(commentFieldId), articles);

  return R.over(R.lensProp("attributes"), R.concat(comments), bundle);
};

const attachmentToAttr = attachmentFieldId => async attachment => {
  const buffer = Buffer.from(attachment.data, "base64");
  const type = await FileType.fromBuffer(buffer);
  if (!type) {
    logger.info(
      "ignoring attachment of unsupported filetype",
      R.pick(["filename"], attachment)
    );
    return null;
  }
  logger.debug(
    "processing attachment",
    R.mergeRight(type, R.pick(["filename", "size"], attachment))
  );
  return {
    metadata: {
      filename: attachment.filename,
      mimetype: type.mime,
      extension: type.ext,
      size: attachment.size,
      sha: attachment.sha
    },
    value: null,
    value_binary: buffer,
    is_binary: true,
    event_id: null,
    field_id: attachmentFieldId,
    field_option_id: null
  };
};

export const xTicketArticlesAttachments = (
  zammad,
  storeObjectId,
  fieldMapping
) => async bundle => {
  const articles = bundle.ticket.ticket_articles.nodes;

  if (R.isEmpty(articles)) return bundle;

  const articleIds = R.map(R.prop("id"), articles);
  const attachments = await zammad.getAttachmentsForTicketArticles(
    storeObjectId,
    articleIds
  );

  // bundle.attachments = attachments;
  const attributes = await R.pipe(
    R.values,
    R.flatten,
    R.reject(R.isNil),
    R.map(attachmentToAttr(fieldMapping.attachments.id)),
    R.reject(R.isNil),
    prAll
  )(attachments);

  if (R.isEmpty(attributes)) return bundle;
  return R.over(R.lensProp("attributes"), R.concat(attributes), bundle);
};

export const xFetchTags = zammad => async bundles => {
  const ticketIds = R.map(R.path(["ticket", "id"]), bundles);
  const tags = await zammad.getTagsForTickets(ticketIds);
  return R.map(bundle => {
    bundle.tags = tags[bundle.ticket.id];
    return bundle;
  }, bundles);
};

const xRewriteTags = txMap => async bundle => {
  if (R.isEmpty(bundle.tags)) return bundle;

  // a memoized function that returns a tags id, creating it if it doesnt exist
  // side effects!
  const findOrCreateTag = R.curryN(
    2,
    pMemoize(
      async (tx, name) => {
        const tag = await findTagByName(tx, name);
        if (tag) return tag;

        return insertTag(tx, { name, displayName: name });
      },
      {
        cacheKey: args => args[1]
      }
    )
  );

  const tags = await txMap(findOrCreateTag, bundle.tags);
  bundle.tags = R.map(R.prop("id"), tags);
  return bundle;
};

/**
 * Asserts that Zammad's definition contains all the known specs. Does NOT
 * assert that Zammad /only/ contains the known specs (there could be extras).
 * @param {string[]} zammadAttributeNames
 * @param {string[]} knownFieldIds
 */
const verifyFieldDefinitions = (zammadAttributeNames, knownFieldIds) => {
  const missing = R.reject(RA.included(zammadAttributeNames), knownFieldIds);
  if (R.isEmpty(missing)) return;
  throw new Error(`Zammad is missing attributes: ${missing.join(", ")}`);
};

/**
 * Takes a list of column names from the Zammad ObjectManager::Attribute for the Ticket type,
 * and matches them to the fields defined by the waterbear field spec.
 *
 * Asserts that Zammad's definition contains all the known specs. Does NOT
 * assert that Zammad /only/ contains the known specs (there could be extras).
 *
 * Converts the known field spec ids into graphql field names, returning them as a list.
 */
export const fieldSpecsToGrahphqlFields = (
  objectManagerAttributeNames,
  fieldSpecIds
) => {
  verifyFieldDefinitions(objectManagerAttributeNames, fieldSpecIds);
  return R.join("\n", R.map(camelCase, fieldSpecIds));
};

/**
 * Given a field spec id, returns an array containing the ids of the field's options
 *
 * @param {string} id the field id
 * @returns {string[]}
 */
export const pluckOptionIdsFor = R.pipe(
  R.curry(getFieldById),
  R.prop("options"),
  R.unless(R.isNil, R.map(R.prop("id")))
);

/**
 * Returns a map of field spec ids to database field ids
 *
 * @example
    {
      medium_other: {
        id: 'd2a90392-5f98-11ea-ae94-7be0297a270d',
        type: 'text',
        specType: 'text',
        options: undefined
      },
      medium: {
        id: 'd2a8f618-5f98-11ea-ae94-c753bdd9428e',
        type: 'option',
        specType: 'multiselect',
        options: {
          messaging: 'd2ae3b28-5f98-11ea-ae94-d3966f71c45c',
          email: 'd2ae50c2-5f98-11ea-ae94-53d3b6ae44d8',
        }
    }
    @param {FieldSpec[]} fieldSpecs
    @param {function} taskBatchMap
    @returns {object}
 */
export const xGenerateFieldToFieldMapping = async (fieldSpecs, txMap) => {
  const fieldSpecIds = getFieldIds(fieldSpecs);

  const maps = await R.pipe(
    txMap(findFieldByName),
    R.andThen(
      R.pipe(
        R.map(R.pipe(R.props(["id", "fieldType"]), R.zipObj(["id", "type"]))),
        R.zipObj(fieldSpecIds),
        R.mapObjIndexed((v, k, mapping) =>
          R.pipe(
            R.assoc("specType", getFieldById(fieldSpecs, k).type),
            R.assoc("options", pluckOptionIdsFor(fieldSpecs, k))
          )(v)
        )
      )
    )
  )(fieldSpecIds);

  // fetch the field_option ids from the database for each field (if it has options)
  const optionIds = await R.pipe(
    R.map(v =>
      R.unless(
        R.isNil,
        R.pipe(
          txMap(findFieldOptionByNameAndField(R.__, v.id, R.__)),
          R.andThen(R.pipe(R.map(R.prop("id"))))
        )
      )(v.options)
    ),
    R.values,
    prAll
  )(maps);

  return mapObjIndexed((v, k, obj, idx) => {
    if (v.options)
      return R.assoc("options", R.zipObj(v.options, optionIds[idx]), v);
    return v;
  }, maps);
};

export const setEventId = eventId => R.set(R.lensProp("event_id"), eventId);

export const xSaveEvents = txMap => async bundles =>
  R.pipe(
    R.map(R.prop("event")),
    txMap(insertEvent), // Event[] => Promise(Event[])
    R.andThen(result => {
      const r = RA.mapIndexed(
        (savedEvent, idx) => R.assoc("event", savedEvent, bundles[idx]),
        result
      );
      return r;
    })
  )(bundles);

export const xIsTicketAlreadyImported = tx => async ticket => {
  const importId = ticket.id;
  assert(
    RA.isNotNilOrEmpty(importId),
    "fillEvent: sanity check: ticket id is null"
  );
  const event = await findEventByImportId(tx, importId.toString());
  const r = RA.isNotNil(event);
  return r;
};

export const fillAttrEventIds = bundle => {
  const eventId = bundle.event.id;
  assert(
    RA.isNotNilOrEmpty(eventId),
    "fillAttrEventIds: sanity check: event id is null"
  );
  bundle.attributes = R.map(setEventId(eventId), bundle.attributes);
  return bundle;
};

export const xSaveEventTags = txMap => async bundles =>
  Promise.all(
    R.map(async bundle => {
      const eventId = bundle.event.id;
      assert(
        RA.isNotNilOrEmpty(eventId),
        "xSaveEventTags: sanity check: event id is null"
      );
      const { tags } = bundle;
      if (R.isEmpty(bundle.tags)) return bundle;
      const saveTags = await R.pipe(
        R.map(tagId => ({ eventId, tagId })),
        txMap(insertEventTag),
        R.andThen(result => {
          bundle.tags = result;
          return bundle;
        })
      )(tags);
      return saveTags;
    }, bundles)
  );

// note: this function does not return bundles
export const xSaveAttributes = txMap => async bundles => {
  const attrs = R.chain(R.prop("attributes"), bundles);

  assert(
    R.pipe(R.map(R.prop("event_id")), R.all(RA.isNotNilOrEmpty))(attrs),
    "xSaveAttributes: sanity check: all attributes must have event_id"
  );

  return R.pipe(
    txMap(insertAttribute),
    R.andThen(result => {
      return result;
    })
  )(attrs);
};

const xSetStateSent = setState => async ticketId => {
  assert(
    RA.isNotNilOrEmpty(ticketId),
    "xSetStateSent: sanity check: ticket id null"
  );
  try {
    return await setState(ticketId);
  } catch (error) {
    logger.error("failed marking ticket sent", { ticketId, error });
    return null;
  }
};

const xMarkTicketsSent = async (sentState, zammadApi, importedTicketIds) => {
  const setState = R.partial(zammadApi.setTicketState, [sentState]);
  return Promise.all(R.map(xSetStateSent(setState), importedTicketIds));
};

const xSaveEventsStream = saveEvents => async (err, inBundles, push, next) => {
  if (inBundles === highland.nil) {
    push(null, inBundles);
    return;
  }
  if (err) {
    // move along
    push(err, null);
    next();
    return;
  }
  const bundles = await saveEvents(inBundles);
  assert(RA.isNotNil(bundles), "bundles returned must not be nil");
  R.map(i => push(null, i), bundles);
  next();
};

const xSaveTagsStream = saveEventTags => async (err, inBundles, push, next) => {
  if (inBundles === highland.nil) {
    push(null, inBundles);
    return;
  }
  if (err) {
    // move along
    push(err, null);
    next();
    return;
  }

  const bundles = await saveEventTags(inBundles);
  R.map(i => push(null, i), bundles);
  next();
};

const xSaveAttrsStream = saveAttrs => async (err, inBundles, push, next) => {
  if (inBundles === highland.nil) {
    push(null, inBundles);
    return;
  }
  if (err) {
    // move along
    push(err, null);
    next();
    return;
  }
  await saveAttrs(inBundles);
  R.map(i => push(null, i), inBundles);
  next();
};

const xImportTickets = async (batchId, pgp, zammad) => {
  /*
   * Fetch the supporting object ids we need to resolve fk relations properly
   */
  logger.debug("zammad import tickets: lookup object ids");
  const {
    closedStateId,
    ticketObjectId,
    storeObjectId
  } = await zammad.lookupIds();
  assert(RA.isNotNil(closedStateId), "closed state not found");
  assert(RA.isNotNil(ticketObjectId), "Ticket object not found");
  assert(
    RA.isNotNil(storeObjectId),
    "Store object for ticket::article not found"
  );

  logger.debug("zammad import tickets: convert fieldspecs to graphql fields");
  const names = await zammad.getObjectManagerAttributeNames(ticketObjectId);
  const graphqlFields = fieldSpecsToGrahphqlFields(names, knownFieldSpecIds);
  const taskMap = taskBatchMap(pgp);
  const fieldMapping = await xGenerateFieldToFieldMapping(FieldsV1, taskMap);

  /*
   *
   * xforming a ticket into event+attributes
   *
   * every ticket will generate one event row
   *                            and many attributes and tags
   *
   *
   */
  const limit = 2; // 50;

  const requiredFields = getRequiredFieldIds(getSubmitterFields(FieldsV1));

  const source = ticketStream({
    zammad,
    limit,
    stateId: closedStateId,
    graphqlFields
  })
    // convert ticket keys to snake case
    .map(RA.renameKeysWith(decamelize))
    // ignore tickets missing required fields
    .filter(hasAllKeys(requiredFields))
    .map(
      wrapAsync(async ticket => {
        const imported = await xIsTicketAlreadyImported(pgp)(ticket);
        const r = {
          ticket,
          imported
        };
        return r;
      })
    )
    .merge();

  const alreadyImportedStream = source
    .fork()
    .filter(bundle => bundle.imported)
    .map(bundle => bundle.ticket.id);
  const toImportStream = source
    .fork()
    .filter(bundle => !bundle.imported)
    .map(bundle => bundle.ticket)
    // .errors((err, push) => push(null, err))
    // munge values that should be arrays into arrays
    .map(castArrays(FieldsV1))
    // convert ticket into event and attributes
    .map(ticketToEventAttrs(batchId, fieldMapping))
    // convert the ticket articles into attributes
    .map(ticketArticlesToEventAttrs(fieldMapping))
    // fetch attachments from the ticket articles and convert them to attributes
    .map(
      wrapAsync(xTicketArticlesAttachments(zammad, storeObjectId, fieldMapping))
    )
    .merge()
    .batch(limit)
    // fetch tags for the tickets
    .map(wrapAsync(xFetchTags(zammad)))
    .merge()
    .flatten()
    // convert the tags from foo:bar into the tag id in our database
    .map(wrapAsync(xRewriteTags(taskMap)))
    .merge()
    .batch(limit)
    // save the events
    .consume(xSaveEventsStream(xSaveEvents(taskMap)))
    // fill each attribute with the event's id
    .map(fillAttrEventIds)
    .batch(limit)
    // save the events_tags
    .consume(xSaveTagsStream(xSaveEventTags(taskMap)))
    .batch(limit)
    // save the attributes
    .consume(xSaveAttrsStream(xSaveAttributes(taskMap)))

    .map(bundle => bundle.ticket.id);

  const importedIdP = alreadyImportedStream.collect().toPromise(Promise);

  const totalProcessedP = toImportStream.collect().toPromise(Promise);

  logger.debug("zammad import tickets: streaming duplicate tickets");
  const alreadyImportedIds = (await importedIdP) || [];
  logger.debug("zammad import tickets: streaming new tickets");
  const newlyImportedIds = (await totalProcessedP) || [];

  logger.debug("zammad import tickets: stream finished");
  const totalDuplicates = alreadyImportedIds.length || 0;
  const totalNew = newlyImportedIds.length || 0;
  const totalProcessed = totalNew + totalDuplicates;

  return {
    alreadyImportedIds,
    newlyImportedIds,
    totalProcessed,
    totalNew,
    totalDuplicates
  };
};

const fieldTypeMap = {
  text: "text",
  longtext: "text",
  select: "option",
  multiselect: "option",
  date: "text",
  timestamp: "text",
  stringlist: "text"
};

const includesFieldType = RA.included(R.keys(fieldTypeMap));
const mapTypeToType = R.pipe(R.prop("type"), R.flip(R.prop)(fieldTypeMap));
const fieldNameTypeMap = {
  attachments: "binary",
  comments: "text"
};
const mapNameToType = R.pipe(R.prop("id"), R.flip(R.prop)(fieldNameTypeMap));
const includesNameFieldType = RA.included(R.keys(fieldNameTypeMap));

const mapFieldType = R.cond([
  [R.pipe(R.prop("id"), includesNameFieldType), mapNameToType],
  [R.pipe(R.prop("type"), includesFieldType), mapTypeToType]
]);

/**
 * Converts a fieldSpec into a field (the database table) object
 */
export const mapField = fieldSpec => ({
  name: fieldSpec.id,
  field_type: mapFieldType(fieldSpec),
  display_name: fieldSpec.name,
  description: R.either(
    R.prop("comment"),
    R.prop("displayName"),
    R.prop("caption")
  )(fieldSpec)
});

/**
 * Asserts that the field objects (the database table) have known types
 */
const validateFields = fields => {
  const errors = R.filter(R.pipe(R.prop("field_type"), R.isNil), fields);
  if (errors.length > 0)
    throw new Error(
      `Field validation: ${errors.length} fields have unknown type: ${R.map(
        R.prop("name"),
        errors
      ).join(", ")}`
    );
};

/**
 * Returns a function that converts a fieldspec option into a field_option for the fieldId
 */
export const mapFieldOption = fieldId => option => ({
  name: option.id,
  display_name: option.name,
  description: null,
  field_id: fieldId
});

/**
 * Creates/updates field rows in the database for every fieldSpec
 */
const xUpdateFields = async pgp => {
  const fields = R.map(mapField, FieldsV1);
  validateFields(fields);
  return pgp.tx(t => t.batch(R.map(saveField(t), fields)));
};

/**
 * Creates/updates field_option rows in the database for every fieldSpec's option
 */
const xUpdateFieldOptions = async pgp => {
  const optFields = R.concat(
    getFieldsByProp("type", "multiselect")(FieldsV1),
    getFieldsByProp("type", "select")(FieldsV1)
  );

  await pgp.tx(async t => {
    const fieldSpecToFieldOpts = async fieldSpec => {
      logger.debug(`processing options for field spec ${fieldSpec.id}`);
      const field = await findFieldByName(t, fieldSpec.id);
      if (R.isNil(field))
        throw new Error(
          `Database field for field spec ${fieldSpec.id} does not exist`
        );
      return R.map(mapFieldOption(field.id), fieldSpec.options);
    };

    const options = await R.pipe(
      R.map(fieldSpecToFieldOpts),
      prAll,
      R.andThen(R.flatten)
    )(optFields);

    return t.batch(R.map(saveFieldOption(t), options));
  });
};
const main = async () => {
  logger.debug("zammad sync task start");
  const pgp = initializePGP(config.get("db").connection);
  const cfHeaders = {
    "CF-Access-Client-Id": config.get("zammadGraphql.serviceTokenId"),
    "CF-Access-Client-Secret": config.get("zammadGraphql.serviceTokenSecret")
  };
  const graphqlUrl = config.get("zammadGraphql.url");
  const graphql = graphqlFactory(graphqlUrl, cfHeaders);
  const zammad = ZammadGraphql(graphql);

  const apiHeaders = {
    Authorization: `Token token=${config.get("zammadApi.token")}`
  };
  const apiUrl = config.get("zammadApi.url");
  const zammadApi = ZammadApi(apiUrl, R.mergeRight(cfHeaders, apiHeaders));

  let results = {};
  /*
   * Update fields and field options from field specs.
   *
   * We only insert new field/opts and update their display and descriptions, we do not delete.
   */
  await pgp.tx(async tx => {
    logger.debug("zammad syncing field options");
    await xUpdateFields(tx);
    await xUpdateFieldOptions(tx);
  });

  logger.debug("zammad creating batch");
  const batches = Batches(pgp);
  const currentBatch = await batches.startBatch({
    origin: config.get("zammadApi.url"),
    startedAt: new Date()
  });
  logger.debug("zammad started batch", currentBatch);

  try {
    /**
     * Fetch tickets from the Link instance and save them to the db
     */

    logger.debug("zammad starting ticket import");
    await pgp.tx(async tx => {
      results = await xImportTickets(currentBatch.id, tx, zammad);
    });

    const importedTicketIds = R.concat(
      results.alreadyImportedIds,
      results.newlyImportedIds
    );
    logger.debug(`zammad marking ${importedTicketIds.length} tickets sent`);
    await xMarkTicketsSent(
      config.get("zammadApi.sentState"),
      zammadApi,
      importedTicketIds
    );
    if (importedTicketIds.length > 0)
      logger.info("zammad sync task results", results);

    /*
     * Note: any errors here should be fatal, so we do not catch rejected promises
     */
  } finally {
    logger.debug("zammad sync finishing batch");
    await batches.finishBatch(
      R.mergeAll([currentBatch, results, { endedAt: new Date() }])
    );
    logger.debug("zammad sync task end");
  }
};

if (config.isDev && process.env.RUN_ZAMMAD_SYNC) main();

const zammadSyncTask = async (
  payload,
  { logger: _logger, job, withPgClient, addJob }
) => {
  // const { name } = payload;
  return main();
};

export default zammadSyncTask;
