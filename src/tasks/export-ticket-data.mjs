import AWS from "aws-sdk";
import fs, { promises as fsp } from "fs";
import archiver from "archiver";
import { Writable } from "stream";
import QueryStream from "pg-query-stream";
import csv from "csv-stringify";
import { logger } from "../logging.mjs";
import { initializePGP } from "../db.mjs";
import config from "../config.mjs";
import {
  findExportRequestById,
  updateExportRequest,
} from "../models/export-requests.mjs";
import User from "../models/user.mjs";

const zipDirectory = (sourceDir, outPath, outDir) => {
  const archive = archiver("zip", { zlib: { level: 9 } });
  const stream = fs.createWriteStream(outPath);

  return new Promise((resolve, reject) => {
    archive
      .directory(sourceDir, outDir)
      .on("error", (err) => reject(err))
      .pipe(stream);

    stream.on("close", () => resolve());
    archive.finalize();
  });
};

const main = async (id) => {
  logger.debug("export request task start");
  const pgp = initializePGP(config.get("db").connection);
  let exportRequestID;
  let requestingUserID;
  let filterTags;
  let downloadURL;
  let whereClause;

  await pgp.tx(async (tx) => {
    logger.debug("fetching record by id");
    const req = await findExportRequestById(tx, id);
    logger.debug(JSON.stringify(req));
    exportRequestID = req.exportRequestId;
    requestingUserID = req.requestingUserId;
  });

  await pgp.tx(async (tx) => {
    logger.debug("fetching user by id");
    const req = await User(tx).findById(requestingUserID);
    logger.info(JSON.stringify(req));
    filterTags = req.filterTags;
  });

  if (filterTags && filterTags.trim() !== "") {
    whereClause = `WHERE tags LIKE '%${filterTags
      .split(",")
      .map((val) => val.trim())
      .join("%' OR tags LIKE '%")}%'`;
  }
  const baseDir = "/tmp/cuwei-worker";
  const requestDir = `/tmp/cuwei-worker/${exportRequestID}`;
  try {
    await fsp.mkdir(requestDir, { recursive: true });
  } catch (e) {
    if (e) throw e;
  }
  const csvFile = fs.createWriteStream(`${requestDir}/tickets.csv`);
  const ticketStream = new QueryStream(
    `SELECT event_number as ticket_number, description, reason, sighted_on, geography, disinfo_links as links, actor, candidate, comments, communities, medium, medium_other, party, platform, tactic, additional_info, tags FROM wb_public.export_view ${whereClause}`
  );
  try {
    const data = await pgp.stream(ticketStream, (stream) => {
      stream.pipe(csv({ header: true })).pipe(csvFile);
    });
    logger.debug(
      `Total rows processed: ${data.processed} Duration in milliseconds: ${data.duration}`
    );
  } catch (error) {
    logger.error("ERROR:", error);
  }

  const writeBinaryFile = new Writable({
    objectMode: true,
    write(record, encoding, callback) {
      (async () => {
        const file = fs.writeFileSync(
          `${requestDir}/${record.eventNumber}-${record.id}.${record.metadata.extension}`,
          record.value
        );
        callback();
      })();
    },
  });
  const attachmentStream = new QueryStream(
    `SELECT * FROM wb_public.export_binary_view ${whereClause}`
  );
  try {
    const data = await pgp.stream(attachmentStream, (stream) => {
      stream.pipe(writeBinaryFile);
    });
    logger.debug(
      `Total rows processed: ${data.processed} Duration in milliseconds: ${data.duration}`
    );
  } catch (error) {
    logger.error("ERROR:", error);
  }

  const res = await zipDirectory(
    requestDir,
    `${baseDir}/${exportRequestID}.zip`,
    exportRequestID
  );

  try {
    const bucket = process.env.EXPORTS_BUCKET;
    const key = `${exportRequestID}.zip`;
    const readStream = fs.createReadStream(`${baseDir}/${exportRequestID}.zip`);
    const client = new AWS.S3({
      region: "eu-central-1",
      signatureVersion: "v4",
    });
    const result = await client
      .upload({
        Bucket: bucket,
        Key: key,
        Body: readStream,
        ContentType: "application/zip",
      })
      .promise();

    logger.debug("upload complete");

    downloadURL = await client.getSignedUrlPromise("getObject", {
      Bucket: bucket,
      Key: key,
      Expires: 10 * 60,
    });
  } catch (e) {
    logger.error(e);
  }

  await pgp.tx(async (tx) => {
    logger.debug("updating record");
    const req = await updateExportRequest(tx, {
      id,
      status: "completed",
      completedAt: new Date(),
      downloadURL,
    });
    logger.debug(JSON.stringify(req));
  });
  logger.debug("DONE");
};

const exportTicketDataTask = async (payload, { logger: _logger }) => {
  logger.debug(JSON.stringify(payload));
  const { id } = payload;
  return main(id);
};

export default exportTicketDataTask;
