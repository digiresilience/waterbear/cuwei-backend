import _pgp from "pg-promise";
import pg from "pg";
import monitor from "pg-monitor";
import config from "./config.mjs";

export const POSTGRES_ERROR_UNIQUE_VIOLATION = "23505";

function camelizeColumns(data) {
  /* eslint-disable  */
  const tmp = data[0];
  for (const prop in tmp) {
    const camel = _pgp.utils.camelize(prop);
    if (!(camel in tmp)) {
      for (let i = 0; i < data.length; i++) {
        const d = data[i];
        d[camel] = d[prop];
        delete d[prop];
      }
    }
  }
}

const pgPromiseOptions = {
  noWarnings: true,
  receive(data, result, e) {
    camelizeColumns(data);
  }
};
const pgp = _pgp(pgPromiseOptions);

export const attachMonitor = () => {
  if (monitor.isAttached()) return monitor;
  monitor.attach(pgPromiseOptions, ["query", "error"]);
  monitor.setLog((msg, info) => {});
  return monitor;
};

export const detachMonitor = () => {
  if (monitor.isAttached()) monitor.detach();
};

export const initializePGP = opts => {
  if (config.get("logging.sql")) attachMonitor();
  return pgp(opts);
};
export const initializePool = connection =>
  new pg.Pool({ connectionString: connection });

export const testConnection = async db => {
  try {
    await db.func("version");
    return true;
  } catch (error) {
    return false;
  }
};
