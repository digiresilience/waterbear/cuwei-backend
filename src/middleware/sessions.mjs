import R from "ramda";
import { logger } from "../logging.mjs";
import { isEmpty } from "../utils.mjs";

/**
 *
 * Since we are using CF Access+Argo we don't need traditional cookie based
 * sessions, however we still want to track sessions in a limited way at least
 * so that our postgres RBAC is based on a random session id rather than a
 * static user id.
 *
 *
 * This way, if an attacker is able to execute SQL via a graphql vulnerability,
 * they can't elevate their privs.
 *
 * This middleware assumes that the db context has been initialized and attached to the app.
 *
 */
const installSessionMiddleware = (app) => {
  /* eslint-disable no-param-reassign,camelcase */

  const { users, sessions } = app.context.db;

  app.use(async (ctx, next) => {
    const email = ctx.request.header["cf-access-authenticated-user-email"];

    if (isEmpty(email)) {
      ctx.status = 401;
      logger.debug("Missing header cf-access-authenticated-user-email");
      return null;
    }
    try {
      const user = await users.resolveUserForPostgraphql(email);
      const sessionId = await sessions.findOrCreateSession(user.id);
      if (isEmpty(sessionId)) throw new Error("Invalid session for user");
      ctx.state.user = R.mergeLeft(user, { sessionId });
      logger.debug("request with session", ctx.state.user);
      return next();
    } catch (e) {
      const totalUsers = await users.count();
      if (totalUsers === 0) {
        logger.debug("no users created, allowing access");
        return next();
      }
      logger.debug(
        "Failed to resolve user session from cloudflare access header",
        {
          email,
        }
      );
      logger.debug("user lookup error", e);
      ctx.status = 401;
      return null;
    }
  });
};
export default installSessionMiddleware;
