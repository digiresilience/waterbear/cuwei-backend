import R from "ramda";
import postgraphile from "postgraphile";

// TODO: move this elsewhere
const roleMappings = {
  admin: "wb_admin",
  core_analyst: "wb_analyst",
  senior_core_analyst: "wb_analyst",
  investigator: "wb_investigator",
  none: "wb_anonymous"
};
export const selectPgRole = R.pipe(
  /* eslint-disable no-underscore-dangle */
  R.prop("userRole"),
  R.propOr("wb_anonymous", R.__, roleMappings)
);

const postgraphileMiddleware = ({ pool, schema, options, ownerPgPool }) => {
  return postgraphile.postgraphile(pool, schema, {
    // Import our shared options
    ...options,

    /*
     appendPlugins: [
      // All the plugins in our shared config
      ...(options.appendPlugins || [])
    ], */

    /*
     * Postgres transaction settings for each GraphQL query/mutation to
     * indicate to Postgres who is attempting to access the resources. These
     * will be referenced by RLS policies/triggers/etc.
     *
     * Settings set here will be set using the equivalent of `SET LOCAL`, so
     * certain things are not allowed. You can override Postgres settings such
     * as 'role' and 'search_path' here; but for settings indicating the
     * current user, session id, or other privileges to be used by RLS policies
     * the setting names must contain at least one and at most two period
     * symbols (`.`), and the first segment must not clash with any Postgres or
     * extension settings. We find `jwt.claims.*` to be a safe namespace,
     * whether or not you're using JWTs.
     */
    async pgSettings(req) {
      const r = {
        role: selectPgRole(req.ctx.state.user),
        /*
         * Note, though this says "jwt" it's not actually anything to do with
         * JWTs, we just know it's a safe namespace to use
         */
        "jwt.claims.session_id":
          req.ctx.state.user && req.ctx.state.user.sessionId
      };
      return r;
    },

    /*
     * These properties are merged into context (the third argument to GraphQL
     * resolvers). This is useful if you write your own plugins that need
     * access to, e.g., the logged in user.
     */
    additionalGraphQLContextFromRequest(req) {
      return {
        ownerPgPool
      };
    }
  });
};

export default postgraphileMiddleware;
