import config from "../config.mjs";
import { initializePool, initializePGP } from "../db.mjs";

const installDatabasePools = app => {
  /* eslint-disable no-param-reassign */
  // this is the pg-promise database handle we use internally
  app.context.pgp = initializePGP(config.get("db").connection);

  // This pool runs as the database owner, so it can do anything but is not a superuser
  app.context.ownerPgPool = app.context.pgp.$pool;

  // app.context.rootPgPool = app.context.pgp.$pool;

  // This pool runs as the unprivileged user, it's what PostGraphile uses.
  app.context.graphqlPgPool = initializePool(
    config.get("postgraphile.authConnection")
  );
};

export default installDatabasePools;
