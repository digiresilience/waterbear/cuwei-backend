import User from "../models/user.mjs";
import Session from "../models/session.mjs";

const installDataModels = app => {
  /* eslint-disable no-param-reassign */
  app.context.db = {
    users: User(app.context.pgp),
    sessions: Session(app.context.pgp)
  };
};

export default installDataModels;
