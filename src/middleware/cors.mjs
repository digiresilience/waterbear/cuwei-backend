import RA from "ramda-adjunct";
import { logger } from "../logging.mjs";
import config from "../config.mjs";

const cors = () => async (ctx, next) => {
  const origin = ctx.get("Origin");
  const allowedOrigins = config.get("cors.allowedOrigins");
  const allowedMethods = config.get("cors.allowedMethods");
  const allowedHeaders = config.get("cors.allowedHeaders");
  if (RA.isNilOrEmpty(origin)) {
    return next();
  }

  logger.debug("CORS", {
    origin,
    allowedOrigins,
    allowedHeaders,
    allowedMethods
  });
  if (RA.isNotNilOrEmpty(origin) && allowedOrigins.includes(origin)) {
    ctx.set("Access-Control-Allow-Origin", origin);
    ctx.set("Access-Control-Allow-Credentials", "true");
    ctx.set("Access-Control-Allow-Methods", allowedMethods.join(","));
    ctx.set("Access-Control-Allow-Headers", allowedHeaders.join(","));
    return next();
  }
  logger.debug("CORS validation failed, invalid origin", { origin });
  ctx.set("Access-Control-Allow-Origin", false);
  return next();
};

export default cors;
