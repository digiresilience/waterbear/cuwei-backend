import { start as startServer } from "./server.mjs";
import { startWorker } from "./worker.mjs";

if (process.argv.length <= 2 || process.argv[2] === "server") startServer();
else if (process.argv[2] === "worker") startWorker();
