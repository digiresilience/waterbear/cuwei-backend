--
-- PostgreSQL database dump
--

-- Dumped from database version 12.4 (Debian 12.4-1.pgdg100+1)
-- Dumped by pg_dump version 13.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: wb_hidden; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA wb_hidden;


--
-- Name: wb_private; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA wb_private;


--
-- Name: wb_public; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA wb_public;


--
-- Name: citext; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS citext WITH SCHEMA public;


--
-- Name: EXTENSION citext; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION citext IS 'data type for case-insensitive character strings';


--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


--
-- Name: tablefunc; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS tablefunc WITH SCHEMA public;


--
-- Name: EXTENSION tablefunc; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION tablefunc IS 'functions that manipulate whole tables, including crosstab';


--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


--
-- Name: edited_attribute; Type: TYPE; Schema: wb_public; Owner: -
--

CREATE TYPE wb_public.edited_attribute AS (
	id uuid,
	value_edited text,
	is_held boolean
);


--
-- Name: TYPE edited_attribute; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON TYPE wb_public.edited_attribute IS 'The subset of fields that an analyst can edit on an attribute.';


--
-- Name: COLUMN edited_attribute.id; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.edited_attribute.id IS 'The id of the attribute being edited';


--
-- Name: COLUMN edited_attribute.value_edited; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.edited_attribute.value_edited IS 'The edited value of the attribute';


--
-- Name: COLUMN edited_attribute.is_held; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.edited_attribute.is_held IS 'Whether this attribute should be held or not';


--
-- Name: edited_tag; Type: TYPE; Schema: wb_public; Owner: -
--

CREATE TYPE wb_public.edited_tag AS (
	id uuid,
	is_held boolean
);


--
-- Name: TYPE edited_tag; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON TYPE wb_public.edited_tag IS 'The subset of tags that an analyst can edit on an event.';


--
-- Name: COLUMN edited_tag.id; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.edited_tag.id IS 'The id of the tag being edited';


--
-- Name: COLUMN edited_tag.is_held; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.edited_tag.is_held IS 'Whether this tag should be held or not';


--
-- Name: export_request_status; Type: TYPE; Schema: wb_public; Owner: -
--

CREATE TYPE wb_public.export_request_status AS ENUM (
    'started',
    'inprogress',
    'completed',
    'error'
);


--
-- Name: extended_event; Type: TYPE; Schema: wb_public; Owner: -
--

CREATE TYPE wb_public.extended_event AS (
	id uuid,
	reported_at timestamp with time zone,
	event_number integer,
	is_archived boolean,
	is_held boolean,
	is_approved boolean,
	created_at timestamp with time zone,
	description text,
	description_edited text,
	reason text
);


--
-- Name: field_types; Type: TYPE; Schema: wb_public; Owner: -
--

CREATE TYPE wb_public.field_types AS ENUM (
    'option',
    'binary',
    'text'
);


--
-- Name: review_actions; Type: TYPE; Schema: wb_public; Owner: -
--

CREATE TYPE wb_public.review_actions AS ENUM (
    'approve',
    'hold',
    'archive'
);


--
-- Name: role_type; Type: TYPE; Schema: wb_public; Owner: -
--

CREATE TYPE wb_public.role_type AS ENUM (
    'none',
    'admin',
    'core_analyst',
    'senior_core_analyst',
    'investigator'
);


--
-- Name: tg__add_job(); Type: FUNCTION; Schema: wb_private; Owner: -
--

CREATE FUNCTION wb_private.tg__add_job() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    SET search_path TO 'pg_catalog', 'public', 'pg_temp'
    AS $$
begin
    perform graphile_worker.add_job(tg_argv[0], json_build_object('id', NEW.id),
                                    coalesce(tg_argv[1], public.gen_random_uuid()::text));
    return NEW;
end;
$$;


--
-- Name: FUNCTION tg__add_job(); Type: COMMENT; Schema: wb_private; Owner: -
--

COMMENT ON FUNCTION wb_private.tg__add_job() IS 'Useful shortcut to create a job on insert/update. Pass the task name as the first trigger argument, and optionally the queue name as the second argument. The record id will automatically be available on the JSON payload.';


--
-- Name: tg__timestamps(); Type: FUNCTION; Schema: wb_private; Owner: -
--

CREATE FUNCTION wb_private.tg__timestamps() RETURNS trigger
    LANGUAGE plpgsql
    SET search_path TO 'pg_catalog', 'public', 'pg_temp'
    AS $$
begin
    NEW.created_at = (case when TG_OP = 'INSERT' then NOW() else OLD.created_at end);
    NEW.updated_at = (case
                          when TG_OP = 'UPDATE' and OLD.updated_at >= NOW()
                              then OLD.updated_at + interval '1 millisecond'
                          else NOW() end);
    return NEW;
end;
$$;


--
-- Name: FUNCTION tg__timestamps(); Type: COMMENT; Schema: wb_private; Owner: -
--

COMMENT ON FUNCTION wb_private.tg__timestamps() IS 'This trigger should be called on all tables with created_at, updated_at - it ensures that they cannot be manipulated and that updated_at will always be larger than the previous updated_at.';


--
-- Name: approved_events_count(); Type: FUNCTION; Schema: wb_public; Owner: -
--

CREATE FUNCTION wb_public.approved_events_count() RETURNS integer
    LANGUAGE plpgsql STABLE SECURITY DEFINER
    AS $$
begin
    return (select count(*) from wb_public.events where is_approved = true);
end;
$$;


--
-- Name: FUNCTION approved_events_count(); Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON FUNCTION wb_public.approved_events_count() IS 'Gets the count of all approved events.';


--
-- Name: events_event_number_seq; Type: SEQUENCE; Schema: wb_public; Owner: -
--

CREATE SEQUENCE wb_public.events_event_number_seq
    START WITH 1000
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: events; Type: TABLE; Schema: wb_public; Owner: -
--

CREATE TABLE wb_public.events (
    id uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    reported_at timestamp with time zone NOT NULL,
    event_number integer DEFAULT nextval('wb_public.events_event_number_seq'::regclass) NOT NULL,
    is_locked boolean DEFAULT false NOT NULL,
    is_archived boolean DEFAULT false NOT NULL,
    is_held boolean DEFAULT false NOT NULL,
    is_approved boolean DEFAULT false NOT NULL,
    batch_id uuid,
    checked_out_at timestamp with time zone,
    checked_out_user_id uuid,
    import_id text NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: TABLE events; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON TABLE wb_public.events IS 'An event that was sighted in the wild';


--
-- Name: COLUMN events.reported_at; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.events.reported_at IS 'The time at which the event was reported. always in utc.';


--
-- Name: COLUMN events.event_number; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.events.event_number IS 'An auto incrementing number to identify the event';


--
-- Name: COLUMN events.is_locked; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.events.is_locked IS 'Whether the event is locked from being edited further.';


--
-- Name: COLUMN events.is_archived; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.events.is_archived IS 'Whether the event is soft-deleted.';


--
-- Name: COLUMN events.is_held; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.events.is_held IS 'whether the event is held for review.';


--
-- Name: COLUMN events.is_approved; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.events.is_approved IS 'Whether the event is has been approved';


--
-- Name: COLUMN events.checked_out_at; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.events.checked_out_at IS 'The timestamp at which the event was checked out. if this is null then the event is not checked out. always in utc.';


--
-- Name: COLUMN events.checked_out_user_id; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.events.checked_out_user_id IS 'The id of the user who has this event checked out. if this is null then the event is not checked out';


--
-- Name: COLUMN events.import_id; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.events.import_id IS 'The id used by the upstream system to identify this event. Used to prevent duplicate imports.';


--
-- Name: COLUMN events.created_at; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.events.created_at IS 'The datetime at which the event was asserted to the database. always in utc.';


--
-- Name: check_in_event(uuid, wb_public.review_actions, wb_public.edited_attribute[], wb_public.edited_tag[], text); Type: FUNCTION; Schema: wb_public; Owner: -
--

CREATE FUNCTION wb_public.check_in_event(event_id uuid, action wb_public.review_actions, attrs wb_public.edited_attribute[], tags wb_public.edited_tag[], reason text) RETURNS wb_public.events
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
declare
    attr                        wb_public.edited_attribute;
    tag                         wb_public.edited_tag;
    current_checked_out_user_id uuid;
    v_action alias for action;
    v_event_id alias for event_id;
    v_reason alias for reason;
    v_is_approved               boolean;
    v_is_held                   boolean;
    v_is_archived               boolean;
    e                           wb_public.events;
begin
    if ((select wb_public.current_user_id()) is null) then
        raise exception 'no logged in user %', wb_public.current_user_id();
    end if;
    current_checked_out_user_id := (select checked_out_user_id from wb_public.events where id = event_id);

    if (current_checked_out_user_id is null or current_checked_out_user_id != (wb_public.current_user_id())) then
        raise 'this event is not checked out by you. you can only check in events that you have checked out.';
    end if;

    if (v_action is null) then
        raise exception 'action is required';
    end if;

    if (v_event_id is null) then
        raise exception 'an event id is required';
    end if;


    v_is_approved := (v_action is not distinct from 'approve');
    v_is_held := (v_action is not distinct from 'hold');
    v_is_archived := (v_action is not distinct from 'archive');

    foreach attr in array attrs
        loop
            perform wb_public.update_attribute(attr);
        end loop;

    foreach tag in array tags
        loop
            perform wb_public.update_event_tag(v_event_id, tag.id, tag.is_held);
        end loop;

    insert into wb_public.reviews (user_id, event_id, action, reason)
    values (wb_public.current_user_id(), v_event_id, v_action, v_reason);

    update wb_public.events
    set checked_out_at      = null,
        checked_out_user_id = null,
        is_approved         = v_is_approved,
        is_held             = v_is_held,
        is_archived         = v_is_archived
    where id = event_id
    returning * into e;
    return e;
end ;
$$;


--
-- Name: FUNCTION check_in_event(event_id uuid, action wb_public.review_actions, attrs wb_public.edited_attribute[], tags wb_public.edited_tag[], reason text); Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON FUNCTION wb_public.check_in_event(event_id uuid, action wb_public.review_actions, attrs wb_public.edited_attribute[], tags wb_public.edited_tag[], reason text) IS 'Checks an event in along with its edited attributes and tags';


--
-- Name: check_out_event(); Type: FUNCTION; Schema: wb_public; Owner: -
--

CREATE FUNCTION wb_public.check_out_event() RETURNS wb_public.events
    LANGUAGE sql SECURITY DEFINER
    AS $$
update wb_public.events
set checked_out_at      = now(),
    checked_out_user_id = wb_public.current_user_id()

where id = (
    select e.id
    from wb_public.events e
    where e.is_approved = false
      and e.is_locked = false
      and e.is_held = false
      and e.is_archived = false
      and (e.checked_out_at is null
      or  e.checked_out_at + interval '1 hour' < now()
      or  checked_out_user_id = wb_public.current_user_id())
    order by created_at
    limit 1
    for update skip locked)
returning *;
$$;


--
-- Name: FUNCTION check_out_event(); Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON FUNCTION wb_public.check_out_event() IS 'Checks out an event for an analyst to work on. Sets checkedOutAt and checkedOutUserId automatically. Returns null if there are no valid events to check out.';


--
-- Name: check_out_event_by_id(uuid); Type: FUNCTION; Schema: wb_public; Owner: -
--

CREATE FUNCTION wb_public.check_out_event_by_id(event_id uuid) RETURNS wb_public.events
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
declare    
    v_event_id alias for event_id;
    e wb_public.events;
begin
    if ((select wb_public.current_user_id()) is null) then
        raise exception 'no logged in user %', wb_public.current_user_id();
    end if;
    
    if (v_event_id is null) then
        raise exception 'an event id is required';
    end if;
    
    update wb_public.events
    set checked_out_at      = now(),
    checked_out_user_id     = wb_public.current_user_id()
    where id = event_id
        and (checked_out_at is null
	or  checked_out_at + interval '1 hour' < now()
	or  checked_out_user_id = wb_public.current_user_id())
    returning * into e;
    return e;
end;
$$;


--
-- Name: FUNCTION check_out_event_by_id(event_id uuid); Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON FUNCTION wb_public.check_out_event_by_id(event_id uuid) IS 'Checks out an event by ID for an analyst to work on. Sets checkedOutAt and checkedOutUserId automatically.';


--
-- Name: users; Type: TABLE; Schema: wb_public; Owner: -
--

CREATE TABLE wb_public.users (
    id uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    email public.citext NOT NULL,
    name text NOT NULL,
    avatar_url text,
    user_role wb_public.role_type DEFAULT 'none'::wb_public.role_type NOT NULL,
    is_active boolean DEFAULT false NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    created_by text NOT NULL,
    filter_tags text,
    CONSTRAINT users_avatar_validity CHECK ((avatar_url ~ '^https?://[^/]+'::text)),
    CONSTRAINT users_email_validity CHECK ((email OPERATOR(public.~*) '^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+[.][A-Za-z]+$'::public.citext))
);


--
-- Name: TABLE users; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON TABLE wb_public.users IS 'A user who can log in to the application.';


--
-- Name: COLUMN users.id; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.users.id IS 'Unique identifier for the user.';


--
-- Name: COLUMN users.email; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.users.email IS 'The email address of the user.';


--
-- Name: COLUMN users.name; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.users.name IS 'Public-facing name (or pseudonym) of the user.';


--
-- Name: COLUMN users.avatar_url; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.users.avatar_url IS 'Optional avatar URL.';


--
-- Name: COLUMN users.user_role; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.users.user_role IS 'The role that defines the user''s privileges.';


--
-- Name: COLUMN users.is_active; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.users.is_active IS 'If false, the user is not allowed to login or access the application';


--
-- Name: create_first_user(text, text); Type: FUNCTION; Schema: wb_public; Owner: -
--

CREATE FUNCTION wb_public.create_first_user(user_email text, user_name text) RETURNS SETOF wb_public.users
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
declare
    user_count int;
begin

    user_count := (select count(id) from wb_public.users);

    if (user_count != 0) then
        raise exception 'Admin user already created';
    end if;

    return query insert into wb_public.users (email, name, user_role, is_active, created_by)
                        values (user_email, user_name, 'admin', true, 'system') returning *;
end ;
$$;


--
-- Name: FUNCTION create_first_user(user_email text, user_name text); Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON FUNCTION wb_public.create_first_user(user_email text, user_name text) IS 'Creates the first user with an admin role. Only possible when there are no other users in the database.';


--
-- Name: current_session_id(); Type: FUNCTION; Schema: wb_public; Owner: -
--

CREATE FUNCTION wb_public.current_session_id() RETURNS uuid
    LANGUAGE sql STABLE
    AS $$
select nullif(pg_catalog.current_setting('jwt.claims.session_id', true), '')::uuid;
$$;


--
-- Name: FUNCTION current_session_id(); Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON FUNCTION wb_public.current_session_id() IS 'Handy method to get the current session ID.';


--
-- Name: current_user(); Type: FUNCTION; Schema: wb_public; Owner: -
--

CREATE FUNCTION wb_public."current_user"() RETURNS wb_public.users
    LANGUAGE sql STABLE
    AS $$
select users.*
from wb_public.users
where id = wb_public.current_user_id();
$$;


--
-- Name: FUNCTION "current_user"(); Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON FUNCTION wb_public."current_user"() IS 'The currently logged in user (or null if not logged in).';


--
-- Name: current_user_id(); Type: FUNCTION; Schema: wb_public; Owner: -
--

CREATE FUNCTION wb_public.current_user_id() RETURNS uuid
    LANGUAGE sql STABLE SECURITY DEFINER
    SET search_path TO 'pg_catalog', 'public', 'pg_temp'
    AS $$
select user_id
from wb_private.sessions
where uuid = wb_public.current_session_id();
$$;


--
-- Name: FUNCTION current_user_id(); Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON FUNCTION wb_public.current_user_id() IS 'Handy method to get the current user ID for use in RLS policies, etc; in GraphQL, use `currentUser{id}` instead.';


--
-- Name: events_with_description(); Type: FUNCTION; Schema: wb_public; Owner: -
--

CREATE FUNCTION wb_public.events_with_description() RETURNS SETOF wb_public.extended_event
    LANGUAGE sql STABLE
    AS $$
    select
    e.id,
    e.reported_at,
    e.event_number,
    e.is_archived,
    e.is_held,
    e.is_approved,
    e.created_at,
    a.value as description,
    a.value_edited as description_edited,
    rl.reason
    from wb_public.events e
    left join wb_public.attributes a on e.id = a.event_id
    left join wb_public.fields f on a.field_id = f.id
    left join lateral (select reason from wb_public.reviews r where r.event_id = e.id order by r.created_at desc limit 1) rl on true
    where f.name = 'description'
    order by e.created_at desc;
$$;


--
-- Name: FUNCTION events_with_description(); Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON FUNCTION wb_public.events_with_description() IS 'Returns events with their description attribute.';


--
-- Name: logout(); Type: FUNCTION; Schema: wb_public; Owner: -
--

CREATE FUNCTION wb_public.logout() RETURNS void
    LANGUAGE plpgsql SECURITY DEFINER
    SET search_path TO 'pg_catalog', 'public', 'pg_temp'
    AS $$
begin
    -- Delete the session
    delete from wb_private.sessions where uuid = wb_public.current_session_id();
    -- Clear the identifier from the transaction
    perform set_config('jwt.claims.session_id', '', true);
end;
$$;


--
-- Name: recent_approved_events_count(); Type: FUNCTION; Schema: wb_public; Owner: -
--

CREATE FUNCTION wb_public.recent_approved_events_count() RETURNS integer
    LANGUAGE plpgsql STABLE SECURITY DEFINER
    AS $$
begin
    if ((select wb_public.current_user_id()) is null) then
       raise exception 'no logged in user %', wb_public.current_user_id();
    end if;

    return (select count(*) from wb_public.events where is_approved = true and created_at > (select requested_at from wb_public.export_requests where requesting_user_id = wb_public.current_user_id() order by requested_at desc limit 1));
end;
$$;


--
-- Name: FUNCTION recent_approved_events_count(); Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON FUNCTION wb_public.recent_approved_events_count() IS 'Gets the count of approved events since the user''s last export.';


--
-- Name: export_requests; Type: TABLE; Schema: wb_public; Owner: -
--

CREATE TABLE wb_public.export_requests (
    id uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    requested_at timestamp with time zone DEFAULT now() NOT NULL,
    completed_at timestamp with time zone,
    export_request_id uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    requesting_user_id uuid NOT NULL,
    download_url text,
    status wb_public.export_request_status DEFAULT 'started'::wb_public.export_request_status NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: TABLE export_requests; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON TABLE wb_public.export_requests IS 'A user request to download the dataset';


--
-- Name: COLUMN export_requests.requested_at; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.export_requests.requested_at IS 'The datetime at which the the export was requested. always in utc.';


--
-- Name: COLUMN export_requests.completed_at; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.export_requests.completed_at IS 'The datetime at which the the export was completed. always in utc.';


--
-- Name: COLUMN export_requests.export_request_id; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.export_requests.export_request_id IS 'A uuid to identify the export request.';


--
-- Name: COLUMN export_requests.requesting_user_id; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.export_requests.requesting_user_id IS 'The id of the user who requested the export.';


--
-- Name: COLUMN export_requests.download_url; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.export_requests.download_url IS 'The url where the user can download their exported data.';


--
-- Name: COLUMN export_requests.status; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.export_requests.status IS 'The current status of the export request.';


--
-- Name: COLUMN export_requests.created_at; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.export_requests.created_at IS 'The datetime at which the export request was inserted into the database. always in utc.';


--
-- Name: COLUMN export_requests.updated_at; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.export_requests.updated_at IS 'The datetime at which the export request was last updated in the database. always in utc.';


--
-- Name: request_export(); Type: FUNCTION; Schema: wb_public; Owner: -
--

CREATE FUNCTION wb_public.request_export() RETURNS wb_public.export_requests
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
declare
    e wb_public.export_requests;
begin
    if ((select wb_public.current_user_id()) is null) then
        raise exception 'no logged in user %', wb_public.current_user_id();
    end if;
    
    insert into wb_public.export_requests
    (requesting_user_id) values (wb_public.current_user_id())
    returning * into e;
    return e; 
end;
$$;


--
-- Name: FUNCTION request_export(); Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON FUNCTION wb_public.request_export() IS 'Creates an export request. Sets requestedAt and requestingUserId automatically.';


--
-- Name: tg__graphql_subscription(); Type: FUNCTION; Schema: wb_public; Owner: -
--

CREATE FUNCTION wb_public.tg__graphql_subscription() RETURNS trigger
    LANGUAGE plpgsql
    AS $_$
declare
    v_process_new    bool = (TG_OP = 'INSERT' OR TG_OP = 'UPDATE');
    v_process_old    bool = (TG_OP = 'UPDATE' OR TG_OP = 'DELETE');
    v_event          text = TG_ARGV[0];
    v_topic_template text = TG_ARGV[1];
    v_attribute      text = TG_ARGV[2];
    v_record         record;
    v_sub            text;
    v_topic          text;
    v_i              int  = 0;
    v_last_topic     text;
begin
    for v_i in 0..1
        loop
            if (v_i = 0) and v_process_new is true then
                v_record = new;
            elsif (v_i = 1) and v_process_old is true then
                v_record = old;
            else
                continue;
            end if;
            if v_attribute is not null then
                execute 'select $1.' || quote_ident(v_attribute)
                    using v_record
                    into v_sub;
            end if;
            if v_sub is not null then
                v_topic = replace(v_topic_template, '$1', v_sub);
            else
                v_topic = v_topic_template;
            end if;
            if v_topic is distinct from v_last_topic then
                -- This if statement prevents us from triggering the same notification twice
                v_last_topic = v_topic;
                perform pg_notify(v_topic, json_build_object(
                        'event', v_event,
                        'subject', v_sub
                    )::text);
            end if;
        end loop;
    return v_record;
end;
$_$;


--
-- Name: FUNCTION tg__graphql_subscription(); Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON FUNCTION wb_public.tg__graphql_subscription() IS 'This function enables the creation of simple focused GraphQL subscriptions using database triggers. Read more here: https://www.graphile.org/postgraphile/subscriptions/#custom-subscriptions';


--
-- Name: update_attribute(wb_public.edited_attribute); Type: FUNCTION; Schema: wb_public; Owner: -
--

CREATE FUNCTION wb_public.update_attribute(attr wb_public.edited_attribute) RETURNS uuid
    LANGUAGE plpgsql
    AS $$
declare
    otype            wb_public.field_types;
    nfield_option_id uuid;
    ofield_id        uuid;
begin

    ofield_id := (select field_id from wb_public.attributes a where a.id = attr.id);
    otype := (select f.field_type from wb_public.fields f where f.id = ofield_id);

    if (otype = 'text'::wb_public.field_types) then
        update wb_public.attributes set value_edited = attr.value_edited, is_held = attr.is_held where id = attr.id;
    elsif (otype = 'binary'::wb_public.field_types) then
        update wb_public.attributes
        set value_binary_edited = decode(attr.value_edited, 'base64'),
            is_held             = attr.is_held
        where id = attr.id;
    elsif (otype = 'option'::wb_public.field_types) then
        nfield_option_id = attr.value_edited::uuid;
        update wb_public.attributes
        set field_option_id_edited = nfield_option_id,
            is_held                = attr.is_held
        where id = attr.id;
    end if;
    return attr.id;
end ;
$$;


--
-- Name: FUNCTION update_attribute(attr wb_public.edited_attribute); Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON FUNCTION wb_public.update_attribute(attr wb_public.edited_attribute) IS 'Edit an attribute''s value.';


--
-- Name: update_event_tag(uuid, uuid, boolean); Type: FUNCTION; Schema: wb_public; Owner: -
--

CREATE FUNCTION wb_public.update_event_tag(v_event_id uuid, v_tag_id uuid, v_is_held boolean) RETURNS uuid
    LANGUAGE plpgsql
    AS $$
begin   
    update wb_public.events_tags set is_held = v_is_held where event_id = v_event_id and tag_id = v_tag_id;
    
    return v_tag_id;
end ;
$$;


--
-- Name: FUNCTION update_event_tag(v_event_id uuid, v_tag_id uuid, v_is_held boolean); Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON FUNCTION wb_public.update_event_tag(v_event_id uuid, v_tag_id uuid, v_is_held boolean) IS 'Edit a tag''s held value.';


--
-- Name: validate_attribute_field(); Type: FUNCTION; Schema: wb_public; Owner: -
--

CREATE FUNCTION wb_public.validate_attribute_field() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
    actual_field_type            wb_public.field_types;
    field_option_field_id        uuid;
    field_option_field_id_edited uuid;
begin
    actual_field_type := (select field_type from wb_public.fields where id = NEW.field_id);
    if (actual_field_type = 'option') then
        field_option_field_id := (select field_id from wb_public.field_options where id = NEW.field_option_id);
        field_option_field_id_edited :=
                (select field_id from wb_public.field_options where id = NEW.field_option_id_edited);

        IF (NEW.field_id != field_option_field_id or NEW.field_id != field_option_field_id_edited) then
            raise exception 'Attribute''s field_option_id and field_option_id_edited must belong to the attribute''s field';
        end if;
    end if;
    return new;
end;
$$;


--
-- Name: validate_events_checked_out(); Type: FUNCTION; Schema: wb_public; Owner: -
--

CREATE FUNCTION wb_public.validate_events_checked_out() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
    if (
            (NEW.checked_out_at is null and NEW.checked_out_user_id is not null)
            or (NEW.checked_out_at is not null and NEW.checked_out_user_id is null))
    then
        raise exception 'When an event is checked out the checked_out_at and checked_out_user_id fields both must be set.';
    end if;
    return new;
end;
$$;


--
-- Name: sessions; Type: TABLE; Schema: wb_private; Owner: -
--

CREATE TABLE wb_private.sessions (
    uuid uuid DEFAULT public.gen_random_uuid() NOT NULL,
    user_id uuid NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    last_active_at timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: attributes; Type: TABLE; Schema: wb_public; Owner: -
--

CREATE TABLE wb_public.attributes (
    id uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    metadata jsonb,
    value text,
    value_edited text,
    value_binary bytea,
    value_binary_edited bytea,
    is_binary boolean DEFAULT false NOT NULL,
    is_held boolean DEFAULT false NOT NULL,
    event_id uuid NOT NULL,
    field_id uuid NOT NULL,
    field_option_id uuid,
    field_option_id_edited uuid,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: TABLE attributes; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON TABLE wb_public.attributes IS '@name attributes_full
@omit read,update,create,delete,all,many
Attributes that an event has. For example, title, description, or a set of fields.
The value could be an attachment via the binary field.';


--
-- Name: COLUMN attributes.metadata; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.attributes.metadata IS 'Metadata about the attribute, cannot be edited by analysts';


--
-- Name: COLUMN attributes.value; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.attributes.value IS 'The optional value of the attribute.';


--
-- Name: COLUMN attributes.value_edited; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.attributes.value_edited IS 'The value of the attribute after being edited by an analyst.';


--
-- Name: COLUMN attributes.value_binary; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.attributes.value_binary IS 'Additional, and optional, binary data of the attribute. Could be an image or other media attachment.';


--
-- Name: COLUMN attributes.value_binary_edited; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.attributes.value_binary_edited IS 'The binary value of the attribute after being edited by an analyst.';


--
-- Name: COLUMN attributes.is_binary; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.attributes.is_binary IS 'Whether the attribute is a binary attachment';


--
-- Name: COLUMN attributes.is_held; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.attributes.is_held IS 'Whether the attribute should be held and not forwarded or distributed';


--
-- Name: COLUMN attributes.event_id; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.attributes.event_id IS 'The event this attribute is associated to.';


--
-- Name: COLUMN attributes.field_id; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.attributes.field_id IS 'The field that this attribute is associated to';


--
-- Name: COLUMN attributes.field_option_id; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.attributes.field_option_id IS 'The field option that this attribute is associated to';


--
-- Name: COLUMN attributes.field_option_id_edited; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.attributes.field_option_id_edited IS 'The field option that this attribute was changed to be after editing by an analyst';


--
-- Name: attributes_view; Type: VIEW; Schema: wb_public; Owner: -
--

CREATE VIEW wb_public.attributes_view AS
 SELECT a.id,
    a.is_held,
    a.is_binary,
    a.metadata,
    a.field_id,
    a.event_id,
    COALESCE(a.value, encode(a.value_binary, 'base64'::text), (a.field_option_id)::text) AS value,
    COALESCE(a.value_edited, encode(a.value_binary_edited, 'base64'::text), (a.field_option_id_edited)::text) AS value_edited,
    a.created_at,
    a.updated_at
   FROM wb_public.attributes a;


--
-- Name: VIEW attributes_view; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON VIEW wb_public.attributes_view IS '@name attributes
@primaryKey id
@foreignKey (event_id) references events (id)
@foreignKey (field_id) references fields (id)
Attributes that an event has. For example, title, description, or a set of fields.';


--
-- Name: batches; Type: TABLE; Schema: wb_public; Owner: -
--

CREATE TABLE wb_public.batches (
    id uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    origin text,
    total_processed integer,
    total_new integer,
    total_duplicates integer,
    total_errors integer,
    started_at timestamp with time zone,
    ended_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: TABLE batches; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON TABLE wb_public.batches IS 'Events are imported in batches';


--
-- Name: events_tags; Type: TABLE; Schema: wb_public; Owner: -
--

CREATE TABLE wb_public.events_tags (
    tag_id uuid NOT NULL,
    event_id uuid NOT NULL,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    is_held boolean DEFAULT false NOT NULL
);


--
-- Name: TABLE events_tags; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON TABLE wb_public.events_tags IS '@omit all,many
Tag to Event associations';


--
-- Name: fields; Type: TABLE; Schema: wb_public; Owner: -
--

CREATE TABLE wb_public.fields (
    id uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    name text NOT NULL,
    field_type wb_public.field_types NOT NULL,
    display_name text NOT NULL,
    description text,
    is_archived boolean DEFAULT false NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT field_name_lowercase CHECK ((name = lower(name))),
    CONSTRAINT field_name_validity CHECK ((name !~ '[\s\b\f\u0020\u00a0\u1680\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u202f\u205f\u3000/\\]'::text))
);


--
-- Name: TABLE fields; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON TABLE wb_public.fields IS 'Fields can be used to associate a static and specific type to an event. Examples are: states, countries, etc.';


--
-- Name: COLUMN fields.name; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.fields.name IS 'The type of the field, must not contain spaces nor slashes';


--
-- Name: COLUMN fields.display_name; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.fields.display_name IS 'The human friendly name of the field';


--
-- Name: COLUMN fields.description; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.fields.description IS 'An optional description of the field';


--
-- Name: tags; Type: TABLE; Schema: wb_public; Owner: -
--

CREATE TABLE wb_public.tags (
    id uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    name public.citext NOT NULL,
    display_name public.citext NOT NULL,
    description text,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT tags_name_validity CHECK ((name OPERATOR(public.!~) '[\s\b\f\u0020\u00A0\u1680\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u202f\u205f\u3000/\\]'::public.citext))
);


--
-- Name: COLUMN tags.name; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.tags.name IS 'The name of the tag, must not contain spaces nor slashes.';


--
-- Name: COLUMN tags.display_name; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.tags.display_name IS 'The human friendly name of the tag';


--
-- Name: COLUMN tags.description; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.tags.description IS 'A description of what exactly this tag represents';


--
-- Name: export_binary_view; Type: VIEW; Schema: wb_public; Owner: -
--

CREATE VIEW wb_public.export_binary_view AS
 SELECT e.event_number,
    f.name,
    COALESCE(a.value_binary_edited, a.value_binary) AS value,
    a.id,
    a.metadata,
    e.updated_at,
    string_agg((t.name)::text, ', '::text) AS tags
   FROM ((((wb_public.attributes a
     LEFT JOIN wb_public.events e ON ((a.event_id = e.id)))
     LEFT JOIN wb_public.fields f ON ((a.field_id = f.id)))
     LEFT JOIN wb_public.events_tags et ON ((e.id = et.event_id)))
     LEFT JOIN wb_public.tags t ON ((et.tag_id = t.id)))
  WHERE ((f.name = 'attachments'::text) AND (e.is_approved = true) AND (a.is_held = false))
  GROUP BY e.event_number, f.name, COALESCE(a.value_binary_edited, a.value_binary), a.id, a.metadata, e.updated_at
  ORDER BY e.event_number, f.name, COALESCE(a.value_binary_edited, a.value_binary), a.id, a.metadata, e.updated_at;


--
-- Name: export_view; Type: VIEW; Schema: wb_public; Owner: -
--

CREATE VIEW wb_public.export_view AS
 SELECT ct.event_number,
    ct.tags,
    ct.actor,
    ct.additional_info,
    ct.candidate,
    ct.comments,
    ct.communities,
    ct.description,
    ct.disinfo_links,
    ct.geography,
    ct.medium,
    ct.medium_other,
    ct.party,
    ct.platform,
    ct.reason,
    ct.sighted_on,
    ct.tactic
   FROM public.crosstab('SELECT
           	e.event_number,
           	STRING_AGG(DISTINCT(t.name), '', '') FILTER (WHERE et.is_held = false) as tags,
           	f.name,           	
           	STRING_AGG(COALESCE(foe.name, fo.name, a.value_edited, a.value), '', '')
           FROM
           	wb_public.attributes a
           	LEFT JOIN wb_public.events e ON a.event_id = e.id
           	LEFT JOIN wb_public.events_tags et ON e.id = et.event_id
           	LEFT JOIN wb_public.tags t ON et.tag_id = t.id
           	LEFT JOIN wb_public.fields f ON a.field_id = f.id
           	LEFT JOIN wb_public.field_options fo ON a.field_option_id = fo.id
           	LEFT JOIN wb_public.field_options foe ON a.field_option_id_edited = foe.id	   
           WHERE
           	e.is_approved = TRUE
           	AND a.is_held = FALSE
		        GROUP BY 1,3
           	ORDER BY 1,3'::text, 'SELECT name FROM (VALUES (''actor''), (''additional_info''), (''candidate''), (''comments''), (''communities''),(''description''), (''disinfo_links''), (''geography''), (''medium''), (''medium_other''), (''party''), (''platform''), (''reason''), (''sighted_on''), (''tactic'')) AS results(name)'::text) ct(event_number integer, tags text, actor text, additional_info text, candidate text, comments text, communities text, description text, disinfo_links text, geography text, medium text, medium_other text, party text, platform text, reason text, sighted_on text, tactic text);


--
-- Name: field_options; Type: TABLE; Schema: wb_public; Owner: -
--

CREATE TABLE wb_public.field_options (
    id uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    name text NOT NULL,
    display_name text NOT NULL,
    description text,
    is_archived boolean DEFAULT false NOT NULL,
    field_id uuid NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT field_option_name_validity CHECK ((name !~ '[\s\b\f\u0020\u00a0\u1680\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u202f\u205f\u3000/\\]'::text))
);


--
-- Name: TABLE field_options; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON TABLE wb_public.field_options IS 'An option that belongs to a specific field. Example: Field Option "AL"/Alaska for the field "State"';


--
-- Name: COLUMN field_options.name; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.field_options.name IS 'The name of the option, must not contain spaces nor slashes';


--
-- Name: COLUMN field_options.display_name; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.field_options.display_name IS 'The human friendly name of the option';


--
-- Name: COLUMN field_options.description; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.field_options.description IS 'An optional description of the field';


--
-- Name: reviews; Type: TABLE; Schema: wb_public; Owner: -
--

CREATE TABLE wb_public.reviews (
    id uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    user_id uuid NOT NULL,
    event_id uuid NOT NULL,
    action wb_public.review_actions NOT NULL,
    reason text,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: TABLE reviews; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON TABLE wb_public.reviews IS 'an event that was sighted in the wild';


--
-- Name: COLUMN reviews.user_id; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.reviews.user_id IS 'the user that did the review';


--
-- Name: COLUMN reviews.event_id; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.reviews.event_id IS 'the event that was reviewed';


--
-- Name: COLUMN reviews.action; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.reviews.action IS 'the concluding action as a result of the review';


--
-- Name: COLUMN reviews.reason; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.reviews.reason IS 'reasoning supporting the action taken';


--
-- Name: COLUMN reviews.created_at; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON COLUMN wb_public.reviews.created_at IS 'the review was asserted to the database at this time';


--
-- Name: sessions sessions_pkey; Type: CONSTRAINT; Schema: wb_private; Owner: -
--

ALTER TABLE ONLY wb_private.sessions
    ADD CONSTRAINT sessions_pkey PRIMARY KEY (uuid);


--
-- Name: attributes attributes_pkey; Type: CONSTRAINT; Schema: wb_public; Owner: -
--

ALTER TABLE ONLY wb_public.attributes
    ADD CONSTRAINT attributes_pkey PRIMARY KEY (id);


--
-- Name: batches batches_pkey; Type: CONSTRAINT; Schema: wb_public; Owner: -
--

ALTER TABLE ONLY wb_public.batches
    ADD CONSTRAINT batches_pkey PRIMARY KEY (id);


--
-- Name: events events_event_number_unique; Type: CONSTRAINT; Schema: wb_public; Owner: -
--

ALTER TABLE ONLY wb_public.events
    ADD CONSTRAINT events_event_number_unique UNIQUE (event_number);


--
-- Name: events events_import_id_unique; Type: CONSTRAINT; Schema: wb_public; Owner: -
--

ALTER TABLE ONLY wb_public.events
    ADD CONSTRAINT events_import_id_unique UNIQUE (import_id);


--
-- Name: events events_pkey; Type: CONSTRAINT; Schema: wb_public; Owner: -
--

ALTER TABLE ONLY wb_public.events
    ADD CONSTRAINT events_pkey PRIMARY KEY (id);


--
-- Name: events_tags events_tags_pk; Type: CONSTRAINT; Schema: wb_public; Owner: -
--

ALTER TABLE ONLY wb_public.events_tags
    ADD CONSTRAINT events_tags_pk PRIMARY KEY (tag_id, event_id);


--
-- Name: export_requests export_request_id_unique; Type: CONSTRAINT; Schema: wb_public; Owner: -
--

ALTER TABLE ONLY wb_public.export_requests
    ADD CONSTRAINT export_request_id_unique UNIQUE (export_request_id);


--
-- Name: export_requests export_requests_pkey; Type: CONSTRAINT; Schema: wb_public; Owner: -
--

ALTER TABLE ONLY wb_public.export_requests
    ADD CONSTRAINT export_requests_pkey PRIMARY KEY (id);


--
-- Name: fields field_name_unique; Type: CONSTRAINT; Schema: wb_public; Owner: -
--

ALTER TABLE ONLY wb_public.fields
    ADD CONSTRAINT field_name_unique UNIQUE (name);


--
-- Name: field_options field_option_name_field_id_unique; Type: CONSTRAINT; Schema: wb_public; Owner: -
--

ALTER TABLE ONLY wb_public.field_options
    ADD CONSTRAINT field_option_name_field_id_unique UNIQUE (name, field_id);


--
-- Name: field_options field_options_pkey; Type: CONSTRAINT; Schema: wb_public; Owner: -
--

ALTER TABLE ONLY wb_public.field_options
    ADD CONSTRAINT field_options_pkey PRIMARY KEY (id);


--
-- Name: fields fields_pkey; Type: CONSTRAINT; Schema: wb_public; Owner: -
--

ALTER TABLE ONLY wb_public.fields
    ADD CONSTRAINT fields_pkey PRIMARY KEY (id);


--
-- Name: reviews reviews_pkey; Type: CONSTRAINT; Schema: wb_public; Owner: -
--

ALTER TABLE ONLY wb_public.reviews
    ADD CONSTRAINT reviews_pkey PRIMARY KEY (id);


--
-- Name: tags tags_name_unique; Type: CONSTRAINT; Schema: wb_public; Owner: -
--

ALTER TABLE ONLY wb_public.tags
    ADD CONSTRAINT tags_name_unique UNIQUE (name);


--
-- Name: tags tags_pkey; Type: CONSTRAINT; Schema: wb_public; Owner: -
--

ALTER TABLE ONLY wb_public.tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (id);


--
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: wb_public; Owner: -
--

ALTER TABLE ONLY wb_public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: wb_public; Owner: -
--

ALTER TABLE ONLY wb_public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: sessions_user_id_idx; Type: INDEX; Schema: wb_private; Owner: -
--

CREATE INDEX sessions_user_id_idx ON wb_private.sessions USING btree (user_id);


--
-- Name: attributes_event_id_idx; Type: INDEX; Schema: wb_public; Owner: -
--

CREATE INDEX attributes_event_id_idx ON wb_public.attributes USING btree (event_id);


--
-- Name: attributes_field_id_idx; Type: INDEX; Schema: wb_public; Owner: -
--

CREATE INDEX attributes_field_id_idx ON wb_public.attributes USING btree (field_id);


--
-- Name: attributes_field_option_id_edited_idx; Type: INDEX; Schema: wb_public; Owner: -
--

CREATE INDEX attributes_field_option_id_edited_idx ON wb_public.attributes USING btree (field_option_id_edited);


--
-- Name: attributes_field_option_id_idx; Type: INDEX; Schema: wb_public; Owner: -
--

CREATE INDEX attributes_field_option_id_idx ON wb_public.attributes USING btree (field_option_id);


--
-- Name: events_batch_id_idx; Type: INDEX; Schema: wb_public; Owner: -
--

CREATE INDEX events_batch_id_idx ON wb_public.events USING btree (batch_id);


--
-- Name: events_checked_out_user_id_idx; Type: INDEX; Schema: wb_public; Owner: -
--

CREATE INDEX events_checked_out_user_id_idx ON wb_public.events USING btree (checked_out_user_id);


--
-- Name: events_created_at_idx; Type: INDEX; Schema: wb_public; Owner: -
--

CREATE INDEX events_created_at_idx ON wb_public.events USING btree (created_at);


--
-- Name: events_is_approved_idx; Type: INDEX; Schema: wb_public; Owner: -
--

CREATE INDEX events_is_approved_idx ON wb_public.events USING btree (is_approved);


--
-- Name: events_tags_event_id_idx; Type: INDEX; Schema: wb_public; Owner: -
--

CREATE INDEX events_tags_event_id_idx ON wb_public.events_tags USING btree (event_id);


--
-- Name: events_tags_tag_id_idx; Type: INDEX; Schema: wb_public; Owner: -
--

CREATE INDEX events_tags_tag_id_idx ON wb_public.events_tags USING btree (tag_id);


--
-- Name: export_requests_requested_at_idx; Type: INDEX; Schema: wb_public; Owner: -
--

CREATE INDEX export_requests_requested_at_idx ON wb_public.export_requests USING btree (requested_at);


--
-- Name: export_requests_requesting_user_id_idx; Type: INDEX; Schema: wb_public; Owner: -
--

CREATE INDEX export_requests_requesting_user_id_idx ON wb_public.export_requests USING btree (requesting_user_id);


--
-- Name: export_requests_requesting_user_id_idx1; Type: INDEX; Schema: wb_public; Owner: -
--

CREATE INDEX export_requests_requesting_user_id_idx1 ON wb_public.export_requests USING btree (requesting_user_id);


--
-- Name: field_options_field_id_idx; Type: INDEX; Schema: wb_public; Owner: -
--

CREATE INDEX field_options_field_id_idx ON wb_public.field_options USING btree (field_id);


--
-- Name: field_options_name_idx; Type: INDEX; Schema: wb_public; Owner: -
--

CREATE INDEX field_options_name_idx ON wb_public.field_options USING btree (name);


--
-- Name: fields_name_idx; Type: INDEX; Schema: wb_public; Owner: -
--

CREATE INDEX fields_name_idx ON wb_public.fields USING btree (name);


--
-- Name: reviews_event_id_idx; Type: INDEX; Schema: wb_public; Owner: -
--

CREATE INDEX reviews_event_id_idx ON wb_public.reviews USING btree (event_id);


--
-- Name: reviews_user_id_idx; Type: INDEX; Schema: wb_public; Owner: -
--

CREATE INDEX reviews_user_id_idx ON wb_public.reviews USING btree (user_id);


--
-- Name: attributes _100_timestamps; Type: TRIGGER; Schema: wb_public; Owner: -
--

CREATE TRIGGER _100_timestamps BEFORE INSERT OR UPDATE ON wb_public.attributes FOR EACH ROW EXECUTE FUNCTION wb_private.tg__timestamps();


--
-- Name: batches _100_timestamps; Type: TRIGGER; Schema: wb_public; Owner: -
--

CREATE TRIGGER _100_timestamps BEFORE INSERT OR UPDATE ON wb_public.batches FOR EACH ROW EXECUTE FUNCTION wb_private.tg__timestamps();


--
-- Name: events _100_timestamps; Type: TRIGGER; Schema: wb_public; Owner: -
--

CREATE TRIGGER _100_timestamps BEFORE INSERT OR UPDATE ON wb_public.events FOR EACH ROW EXECUTE FUNCTION wb_private.tg__timestamps();


--
-- Name: events_tags _100_timestamps; Type: TRIGGER; Schema: wb_public; Owner: -
--

CREATE TRIGGER _100_timestamps BEFORE INSERT OR UPDATE ON wb_public.events_tags FOR EACH ROW EXECUTE FUNCTION wb_private.tg__timestamps();


--
-- Name: export_requests _100_timestamps; Type: TRIGGER; Schema: wb_public; Owner: -
--

CREATE TRIGGER _100_timestamps BEFORE INSERT OR UPDATE ON wb_public.export_requests FOR EACH ROW EXECUTE FUNCTION wb_private.tg__timestamps();


--
-- Name: field_options _100_timestamps; Type: TRIGGER; Schema: wb_public; Owner: -
--

CREATE TRIGGER _100_timestamps BEFORE INSERT OR UPDATE ON wb_public.field_options FOR EACH ROW EXECUTE FUNCTION wb_private.tg__timestamps();


--
-- Name: fields _100_timestamps; Type: TRIGGER; Schema: wb_public; Owner: -
--

CREATE TRIGGER _100_timestamps BEFORE INSERT OR UPDATE ON wb_public.fields FOR EACH ROW EXECUTE FUNCTION wb_private.tg__timestamps();


--
-- Name: reviews _100_timestamps; Type: TRIGGER; Schema: wb_public; Owner: -
--

CREATE TRIGGER _100_timestamps BEFORE INSERT OR UPDATE ON wb_public.reviews FOR EACH ROW EXECUTE FUNCTION wb_private.tg__timestamps();


--
-- Name: tags _100_timestamps; Type: TRIGGER; Schema: wb_public; Owner: -
--

CREATE TRIGGER _100_timestamps BEFORE INSERT OR UPDATE ON wb_public.tags FOR EACH ROW EXECUTE FUNCTION wb_private.tg__timestamps();


--
-- Name: users _100_timestamps; Type: TRIGGER; Schema: wb_public; Owner: -
--

CREATE TRIGGER _100_timestamps BEFORE INSERT OR UPDATE ON wb_public.users FOR EACH ROW EXECUTE FUNCTION wb_private.tg__timestamps();


--
-- Name: attributes _200_attributes_field_matches_option; Type: TRIGGER; Schema: wb_public; Owner: -
--

CREATE TRIGGER _200_attributes_field_matches_option BEFORE INSERT OR UPDATE ON wb_public.attributes FOR EACH ROW EXECUTE FUNCTION wb_public.validate_attribute_field();


--
-- Name: events _200_events_checked_out; Type: TRIGGER; Schema: wb_public; Owner: -
--

CREATE TRIGGER _200_events_checked_out BEFORE INSERT OR UPDATE ON wb_public.events FOR EACH ROW EXECUTE FUNCTION wb_public.validate_events_checked_out();


--
-- Name: export_requests _200_export_data; Type: TRIGGER; Schema: wb_public; Owner: -
--

CREATE TRIGGER _200_export_data AFTER INSERT ON wb_public.export_requests FOR EACH ROW EXECUTE FUNCTION wb_private.tg__add_job('exportTicketDataTask');


--
-- Name: users _500_gql_update; Type: TRIGGER; Schema: wb_public; Owner: -
--

CREATE TRIGGER _500_gql_update AFTER UPDATE ON wb_public.users FOR EACH ROW EXECUTE FUNCTION wb_public.tg__graphql_subscription('userChanged', 'graphql:user:$1', 'id');


--
-- Name: sessions sessions_user_id_fkey; Type: FK CONSTRAINT; Schema: wb_private; Owner: -
--

ALTER TABLE ONLY wb_private.sessions
    ADD CONSTRAINT sessions_user_id_fkey FOREIGN KEY (user_id) REFERENCES wb_public.users(id) ON DELETE CASCADE;


--
-- Name: events events_batch_id_fkey; Type: FK CONSTRAINT; Schema: wb_public; Owner: -
--

ALTER TABLE ONLY wb_public.events
    ADD CONSTRAINT events_batch_id_fkey FOREIGN KEY (batch_id) REFERENCES wb_public.batches(id) ON DELETE RESTRICT;


--
-- Name: events events_checked_out_user_id_fkey; Type: FK CONSTRAINT; Schema: wb_public; Owner: -
--

ALTER TABLE ONLY wb_public.events
    ADD CONSTRAINT events_checked_out_user_id_fkey FOREIGN KEY (checked_out_user_id) REFERENCES wb_public.users(id) ON DELETE RESTRICT;


--
-- Name: attributes events_fk; Type: FK CONSTRAINT; Schema: wb_public; Owner: -
--

ALTER TABLE ONLY wb_public.attributes
    ADD CONSTRAINT events_fk FOREIGN KEY (event_id) REFERENCES wb_public.events(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: reviews events_fk; Type: FK CONSTRAINT; Schema: wb_public; Owner: -
--

ALTER TABLE ONLY wb_public.reviews
    ADD CONSTRAINT events_fk FOREIGN KEY (event_id) REFERENCES wb_public.events(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: events_tags events_fk; Type: FK CONSTRAINT; Schema: wb_public; Owner: -
--

ALTER TABLE ONLY wb_public.events_tags
    ADD CONSTRAINT events_fk FOREIGN KEY (event_id) REFERENCES wb_public.events(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: CONSTRAINT events_fk ON events_tags; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON CONSTRAINT events_fk ON wb_public.events_tags IS '@manyToManyFieldName events';


--
-- Name: attributes field_fk; Type: FK CONSTRAINT; Schema: wb_public; Owner: -
--

ALTER TABLE ONLY wb_public.attributes
    ADD CONSTRAINT field_fk FOREIGN KEY (field_id) REFERENCES wb_public.fields(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: attributes field_options_edited_fk; Type: FK CONSTRAINT; Schema: wb_public; Owner: -
--

ALTER TABLE ONLY wb_public.attributes
    ADD CONSTRAINT field_options_edited_fk FOREIGN KEY (field_option_id_edited) REFERENCES wb_public.field_options(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: field_options field_options_field_id_fkey; Type: FK CONSTRAINT; Schema: wb_public; Owner: -
--

ALTER TABLE ONLY wb_public.field_options
    ADD CONSTRAINT field_options_field_id_fkey FOREIGN KEY (field_id) REFERENCES wb_public.fields(id) ON DELETE RESTRICT;


--
-- Name: attributes field_options_fk; Type: FK CONSTRAINT; Schema: wb_public; Owner: -
--

ALTER TABLE ONLY wb_public.attributes
    ADD CONSTRAINT field_options_fk FOREIGN KEY (field_option_id) REFERENCES wb_public.field_options(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: export_requests requesting_user_id_fkey; Type: FK CONSTRAINT; Schema: wb_public; Owner: -
--

ALTER TABLE ONLY wb_public.export_requests
    ADD CONSTRAINT requesting_user_id_fkey FOREIGN KEY (requesting_user_id) REFERENCES wb_public.users(id) ON DELETE RESTRICT;


--
-- Name: events_tags tags_fk; Type: FK CONSTRAINT; Schema: wb_public; Owner: -
--

ALTER TABLE ONLY wb_public.events_tags
    ADD CONSTRAINT tags_fk FOREIGN KEY (tag_id) REFERENCES wb_public.tags(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: CONSTRAINT tags_fk ON events_tags; Type: COMMENT; Schema: wb_public; Owner: -
--

COMMENT ON CONSTRAINT tags_fk ON wb_public.events_tags IS '@manyToManyFieldName tags';


--
-- Name: reviews users_fk; Type: FK CONSTRAINT; Schema: wb_public; Owner: -
--

ALTER TABLE ONLY wb_public.reviews
    ADD CONSTRAINT users_fk FOREIGN KEY (user_id) REFERENCES wb_public.users(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: export_requests users_fk; Type: FK CONSTRAINT; Schema: wb_public; Owner: -
--

ALTER TABLE ONLY wb_public.export_requests
    ADD CONSTRAINT users_fk FOREIGN KEY (requesting_user_id) REFERENCES wb_public.users(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: sessions; Type: ROW SECURITY; Schema: wb_private; Owner: -
--

ALTER TABLE wb_private.sessions ENABLE ROW LEVEL SECURITY;

--
-- Name: attributes access_all; Type: POLICY; Schema: wb_public; Owner: -
--

CREATE POLICY access_all ON wb_public.attributes TO wb_analyst USING (true);


--
-- Name: batches access_all; Type: POLICY; Schema: wb_public; Owner: -
--

CREATE POLICY access_all ON wb_public.batches TO wb_analyst USING (true);


--
-- Name: events access_all; Type: POLICY; Schema: wb_public; Owner: -
--

CREATE POLICY access_all ON wb_public.events TO wb_analyst USING (true);


--
-- Name: events_tags access_all; Type: POLICY; Schema: wb_public; Owner: -
--

CREATE POLICY access_all ON wb_public.events_tags TO wb_analyst USING (true);


--
-- Name: export_requests access_all; Type: POLICY; Schema: wb_public; Owner: -
--

CREATE POLICY access_all ON wb_public.export_requests TO wb_admin USING (true);


--
-- Name: field_options access_all; Type: POLICY; Schema: wb_public; Owner: -
--

CREATE POLICY access_all ON wb_public.field_options TO wb_analyst USING (true);


--
-- Name: fields access_all; Type: POLICY; Schema: wb_public; Owner: -
--

CREATE POLICY access_all ON wb_public.fields TO wb_analyst USING (true);


--
-- Name: reviews access_all; Type: POLICY; Schema: wb_public; Owner: -
--

CREATE POLICY access_all ON wb_public.reviews TO wb_senioranalyst USING (true);


--
-- Name: tags access_all; Type: POLICY; Schema: wb_public; Owner: -
--

CREATE POLICY access_all ON wb_public.tags TO wb_analyst USING (true);


--
-- Name: users access_all; Type: POLICY; Schema: wb_public; Owner: -
--

CREATE POLICY access_all ON wb_public.users TO wb_admin USING (true);


--
-- Name: export_requests access_self; Type: POLICY; Schema: wb_public; Owner: -
--

CREATE POLICY access_self ON wb_public.export_requests TO wb_analyst, wb_investigator, wb_senioranalyst USING ((requesting_user_id = wb_public.current_user_id()));


--
-- Name: reviews access_self; Type: POLICY; Schema: wb_public; Owner: -
--

CREATE POLICY access_self ON wb_public.reviews TO wb_analyst USING ((user_id = wb_public.current_user_id()));


--
-- Name: users access_self; Type: POLICY; Schema: wb_public; Owner: -
--

CREATE POLICY access_self ON wb_public.users TO wb_anonymous USING ((id = wb_public.current_user_id()));


--
-- Name: attributes; Type: ROW SECURITY; Schema: wb_public; Owner: -
--

ALTER TABLE wb_public.attributes ENABLE ROW LEVEL SECURITY;

--
-- Name: batches; Type: ROW SECURITY; Schema: wb_public; Owner: -
--

ALTER TABLE wb_public.batches ENABLE ROW LEVEL SECURITY;

--
-- Name: events; Type: ROW SECURITY; Schema: wb_public; Owner: -
--

ALTER TABLE wb_public.events ENABLE ROW LEVEL SECURITY;

--
-- Name: export_requests; Type: ROW SECURITY; Schema: wb_public; Owner: -
--

ALTER TABLE wb_public.export_requests ENABLE ROW LEVEL SECURITY;

--
-- Name: field_options; Type: ROW SECURITY; Schema: wb_public; Owner: -
--

ALTER TABLE wb_public.field_options ENABLE ROW LEVEL SECURITY;

--
-- Name: fields; Type: ROW SECURITY; Schema: wb_public; Owner: -
--

ALTER TABLE wb_public.fields ENABLE ROW LEVEL SECURITY;

--
-- Name: reviews; Type: ROW SECURITY; Schema: wb_public; Owner: -
--

ALTER TABLE wb_public.reviews ENABLE ROW LEVEL SECURITY;

--
-- Name: tags; Type: ROW SECURITY; Schema: wb_public; Owner: -
--

ALTER TABLE wb_public.tags ENABLE ROW LEVEL SECURITY;

--
-- Name: users; Type: ROW SECURITY; Schema: wb_public; Owner: -
--

ALTER TABLE wb_public.users ENABLE ROW LEVEL SECURITY;

--
-- Name: SCHEMA wb_hidden; Type: ACL; Schema: -; Owner: -
--

GRANT USAGE ON SCHEMA wb_hidden TO wb_postgraphile;


--
-- Name: SCHEMA wb_public; Type: ACL; Schema: -; Owner: -
--

GRANT USAGE ON SCHEMA wb_public TO wb_postgraphile;
GRANT USAGE ON SCHEMA wb_public TO wb_admin;
GRANT USAGE ON SCHEMA wb_public TO wb_anonymous;
GRANT USAGE ON SCHEMA wb_public TO wb_analyst;
GRANT USAGE ON SCHEMA wb_public TO wb_senioranalyst;
GRANT USAGE ON SCHEMA wb_public TO wb_investigator;


--
-- Name: FUNCTION tg__add_job(); Type: ACL; Schema: wb_private; Owner: -
--

REVOKE ALL ON FUNCTION wb_private.tg__add_job() FROM PUBLIC;


--
-- Name: FUNCTION tg__timestamps(); Type: ACL; Schema: wb_private; Owner: -
--

REVOKE ALL ON FUNCTION wb_private.tg__timestamps() FROM PUBLIC;


--
-- Name: FUNCTION approved_events_count(); Type: ACL; Schema: wb_public; Owner: -
--

REVOKE ALL ON FUNCTION wb_public.approved_events_count() FROM PUBLIC;
GRANT ALL ON FUNCTION wb_public.approved_events_count() TO wb_postgraphile;
GRANT ALL ON FUNCTION wb_public.approved_events_count() TO wb_analyst;
GRANT ALL ON FUNCTION wb_public.approved_events_count() TO wb_investigator;
GRANT ALL ON FUNCTION wb_public.approved_events_count() TO wb_senioranalyst;
GRANT ALL ON FUNCTION wb_public.approved_events_count() TO wb_admin;


--
-- Name: SEQUENCE events_event_number_seq; Type: ACL; Schema: wb_public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE wb_public.events_event_number_seq TO wb_postgraphile;


--
-- Name: TABLE events; Type: ACL; Schema: wb_public; Owner: -
--

GRANT SELECT ON TABLE wb_public.events TO wb_analyst;


--
-- Name: COLUMN events.reported_at; Type: ACL; Schema: wb_public; Owner: -
--

GRANT UPDATE(reported_at) ON TABLE wb_public.events TO wb_analyst;
GRANT INSERT(reported_at) ON TABLE wb_public.events TO wb_admin;


--
-- Name: COLUMN events.is_locked; Type: ACL; Schema: wb_public; Owner: -
--

GRANT UPDATE(is_locked) ON TABLE wb_public.events TO wb_analyst;
GRANT INSERT(is_locked) ON TABLE wb_public.events TO wb_admin;


--
-- Name: COLUMN events.is_archived; Type: ACL; Schema: wb_public; Owner: -
--

GRANT UPDATE(is_archived) ON TABLE wb_public.events TO wb_analyst;
GRANT INSERT(is_archived) ON TABLE wb_public.events TO wb_admin;


--
-- Name: COLUMN events.is_held; Type: ACL; Schema: wb_public; Owner: -
--

GRANT UPDATE(is_held) ON TABLE wb_public.events TO wb_analyst;
GRANT INSERT(is_held) ON TABLE wb_public.events TO wb_admin;


--
-- Name: COLUMN events.is_approved; Type: ACL; Schema: wb_public; Owner: -
--

GRANT UPDATE(is_approved) ON TABLE wb_public.events TO wb_analyst;
GRANT INSERT(is_approved) ON TABLE wb_public.events TO wb_admin;


--
-- Name: FUNCTION check_in_event(event_id uuid, action wb_public.review_actions, attrs wb_public.edited_attribute[], tags wb_public.edited_tag[], reason text); Type: ACL; Schema: wb_public; Owner: -
--

REVOKE ALL ON FUNCTION wb_public.check_in_event(event_id uuid, action wb_public.review_actions, attrs wb_public.edited_attribute[], tags wb_public.edited_tag[], reason text) FROM PUBLIC;
GRANT ALL ON FUNCTION wb_public.check_in_event(event_id uuid, action wb_public.review_actions, attrs wb_public.edited_attribute[], tags wb_public.edited_tag[], reason text) TO wb_postgraphile;
GRANT ALL ON FUNCTION wb_public.check_in_event(event_id uuid, action wb_public.review_actions, attrs wb_public.edited_attribute[], tags wb_public.edited_tag[], reason text) TO wb_analyst;
GRANT ALL ON FUNCTION wb_public.check_in_event(event_id uuid, action wb_public.review_actions, attrs wb_public.edited_attribute[], tags wb_public.edited_tag[], reason text) TO wb_senioranalyst;
GRANT ALL ON FUNCTION wb_public.check_in_event(event_id uuid, action wb_public.review_actions, attrs wb_public.edited_attribute[], tags wb_public.edited_tag[], reason text) TO wb_admin;


--
-- Name: FUNCTION check_out_event(); Type: ACL; Schema: wb_public; Owner: -
--

REVOKE ALL ON FUNCTION wb_public.check_out_event() FROM PUBLIC;
GRANT ALL ON FUNCTION wb_public.check_out_event() TO wb_postgraphile;
GRANT ALL ON FUNCTION wb_public.check_out_event() TO wb_analyst;
GRANT ALL ON FUNCTION wb_public.check_out_event() TO wb_senioranalyst;
GRANT ALL ON FUNCTION wb_public.check_out_event() TO wb_admin;


--
-- Name: FUNCTION check_out_event_by_id(event_id uuid); Type: ACL; Schema: wb_public; Owner: -
--

REVOKE ALL ON FUNCTION wb_public.check_out_event_by_id(event_id uuid) FROM PUBLIC;
GRANT ALL ON FUNCTION wb_public.check_out_event_by_id(event_id uuid) TO wb_postgraphile;
GRANT ALL ON FUNCTION wb_public.check_out_event_by_id(event_id uuid) TO wb_analyst;
GRANT ALL ON FUNCTION wb_public.check_out_event_by_id(event_id uuid) TO wb_senioranalyst;
GRANT ALL ON FUNCTION wb_public.check_out_event_by_id(event_id uuid) TO wb_admin;


--
-- Name: TABLE users; Type: ACL; Schema: wb_public; Owner: -
--

GRANT SELECT ON TABLE wb_public.users TO wb_anonymous;
GRANT SELECT ON TABLE wb_public.users TO wb_admin;


--
-- Name: COLUMN users.email; Type: ACL; Schema: wb_public; Owner: -
--

GRANT INSERT(email),UPDATE(email) ON TABLE wb_public.users TO wb_admin;


--
-- Name: COLUMN users.name; Type: ACL; Schema: wb_public; Owner: -
--

GRANT UPDATE(name) ON TABLE wb_public.users TO wb_postgraphile;
GRANT UPDATE(name) ON TABLE wb_public.users TO wb_analyst;
GRANT UPDATE(name) ON TABLE wb_public.users TO wb_investigator;
GRANT INSERT(name),UPDATE(name) ON TABLE wb_public.users TO wb_admin;


--
-- Name: COLUMN users.avatar_url; Type: ACL; Schema: wb_public; Owner: -
--

GRANT UPDATE(avatar_url) ON TABLE wb_public.users TO wb_postgraphile;
GRANT UPDATE(avatar_url) ON TABLE wb_public.users TO wb_analyst;
GRANT UPDATE(avatar_url) ON TABLE wb_public.users TO wb_investigator;
GRANT INSERT(avatar_url),UPDATE(avatar_url) ON TABLE wb_public.users TO wb_admin;


--
-- Name: COLUMN users.user_role; Type: ACL; Schema: wb_public; Owner: -
--

GRANT INSERT(user_role),UPDATE(user_role) ON TABLE wb_public.users TO wb_admin;


--
-- Name: COLUMN users.is_active; Type: ACL; Schema: wb_public; Owner: -
--

GRANT INSERT(is_active),UPDATE(is_active) ON TABLE wb_public.users TO wb_admin;


--
-- Name: COLUMN users.created_by; Type: ACL; Schema: wb_public; Owner: -
--

GRANT INSERT(created_by),UPDATE(created_by) ON TABLE wb_public.users TO wb_admin;


--
-- Name: COLUMN users.filter_tags; Type: ACL; Schema: wb_public; Owner: -
--

GRANT INSERT(filter_tags),UPDATE(filter_tags) ON TABLE wb_public.users TO wb_admin;


--
-- Name: FUNCTION create_first_user(user_email text, user_name text); Type: ACL; Schema: wb_public; Owner: -
--

REVOKE ALL ON FUNCTION wb_public.create_first_user(user_email text, user_name text) FROM PUBLIC;
GRANT ALL ON FUNCTION wb_public.create_first_user(user_email text, user_name text) TO wb_postgraphile;
GRANT ALL ON FUNCTION wb_public.create_first_user(user_email text, user_name text) TO wb_anonymous;
GRANT ALL ON FUNCTION wb_public.create_first_user(user_email text, user_name text) TO wb_analyst;
GRANT ALL ON FUNCTION wb_public.create_first_user(user_email text, user_name text) TO wb_investigator;
GRANT ALL ON FUNCTION wb_public.create_first_user(user_email text, user_name text) TO wb_senioranalyst;
GRANT ALL ON FUNCTION wb_public.create_first_user(user_email text, user_name text) TO wb_admin;


--
-- Name: FUNCTION current_session_id(); Type: ACL; Schema: wb_public; Owner: -
--

REVOKE ALL ON FUNCTION wb_public.current_session_id() FROM PUBLIC;
GRANT ALL ON FUNCTION wb_public.current_session_id() TO wb_postgraphile;
GRANT ALL ON FUNCTION wb_public.current_session_id() TO wb_anonymous;
GRANT ALL ON FUNCTION wb_public.current_session_id() TO wb_analyst;
GRANT ALL ON FUNCTION wb_public.current_session_id() TO wb_investigator;
GRANT ALL ON FUNCTION wb_public.current_session_id() TO wb_senioranalyst;
GRANT ALL ON FUNCTION wb_public.current_session_id() TO wb_admin;


--
-- Name: FUNCTION "current_user"(); Type: ACL; Schema: wb_public; Owner: -
--

REVOKE ALL ON FUNCTION wb_public."current_user"() FROM PUBLIC;
GRANT ALL ON FUNCTION wb_public."current_user"() TO wb_postgraphile;
GRANT ALL ON FUNCTION wb_public."current_user"() TO wb_anonymous;
GRANT ALL ON FUNCTION wb_public."current_user"() TO wb_analyst;
GRANT ALL ON FUNCTION wb_public."current_user"() TO wb_investigator;
GRANT ALL ON FUNCTION wb_public."current_user"() TO wb_senioranalyst;
GRANT ALL ON FUNCTION wb_public."current_user"() TO wb_admin;


--
-- Name: FUNCTION current_user_id(); Type: ACL; Schema: wb_public; Owner: -
--

REVOKE ALL ON FUNCTION wb_public.current_user_id() FROM PUBLIC;
GRANT ALL ON FUNCTION wb_public.current_user_id() TO wb_postgraphile;
GRANT ALL ON FUNCTION wb_public.current_user_id() TO wb_anonymous;
GRANT ALL ON FUNCTION wb_public.current_user_id() TO wb_analyst;
GRANT ALL ON FUNCTION wb_public.current_user_id() TO wb_investigator;
GRANT ALL ON FUNCTION wb_public.current_user_id() TO wb_senioranalyst;
GRANT ALL ON FUNCTION wb_public.current_user_id() TO wb_admin;


--
-- Name: FUNCTION events_with_description(); Type: ACL; Schema: wb_public; Owner: -
--

REVOKE ALL ON FUNCTION wb_public.events_with_description() FROM PUBLIC;
GRANT ALL ON FUNCTION wb_public.events_with_description() TO wb_postgraphile;
GRANT ALL ON FUNCTION wb_public.events_with_description() TO wb_analyst;
GRANT ALL ON FUNCTION wb_public.events_with_description() TO wb_senioranalyst;
GRANT ALL ON FUNCTION wb_public.events_with_description() TO wb_admin;


--
-- Name: FUNCTION logout(); Type: ACL; Schema: wb_public; Owner: -
--

REVOKE ALL ON FUNCTION wb_public.logout() FROM PUBLIC;
GRANT ALL ON FUNCTION wb_public.logout() TO wb_postgraphile;
GRANT ALL ON FUNCTION wb_public.logout() TO wb_anonymous;
GRANT ALL ON FUNCTION wb_public.logout() TO wb_analyst;
GRANT ALL ON FUNCTION wb_public.logout() TO wb_investigator;
GRANT ALL ON FUNCTION wb_public.logout() TO wb_senioranalyst;
GRANT ALL ON FUNCTION wb_public.logout() TO wb_admin;


--
-- Name: FUNCTION recent_approved_events_count(); Type: ACL; Schema: wb_public; Owner: -
--

REVOKE ALL ON FUNCTION wb_public.recent_approved_events_count() FROM PUBLIC;
GRANT ALL ON FUNCTION wb_public.recent_approved_events_count() TO wb_postgraphile;
GRANT ALL ON FUNCTION wb_public.recent_approved_events_count() TO wb_analyst;
GRANT ALL ON FUNCTION wb_public.recent_approved_events_count() TO wb_investigator;
GRANT ALL ON FUNCTION wb_public.recent_approved_events_count() TO wb_senioranalyst;
GRANT ALL ON FUNCTION wb_public.recent_approved_events_count() TO wb_admin;


--
-- Name: TABLE export_requests; Type: ACL; Schema: wb_public; Owner: -
--

GRANT SELECT ON TABLE wb_public.export_requests TO wb_admin;
GRANT SELECT ON TABLE wb_public.export_requests TO wb_analyst;
GRANT SELECT ON TABLE wb_public.export_requests TO wb_senioranalyst;
GRANT SELECT ON TABLE wb_public.export_requests TO wb_investigator;


--
-- Name: COLUMN export_requests.completed_at; Type: ACL; Schema: wb_public; Owner: -
--

GRANT UPDATE(completed_at) ON TABLE wb_public.export_requests TO wb_admin;


--
-- Name: COLUMN export_requests.download_url; Type: ACL; Schema: wb_public; Owner: -
--

GRANT UPDATE(download_url) ON TABLE wb_public.export_requests TO wb_admin;


--
-- Name: COLUMN export_requests.status; Type: ACL; Schema: wb_public; Owner: -
--

GRANT UPDATE(status) ON TABLE wb_public.export_requests TO wb_admin;


--
-- Name: FUNCTION request_export(); Type: ACL; Schema: wb_public; Owner: -
--

REVOKE ALL ON FUNCTION wb_public.request_export() FROM PUBLIC;
GRANT ALL ON FUNCTION wb_public.request_export() TO wb_postgraphile;
GRANT ALL ON FUNCTION wb_public.request_export() TO wb_analyst;
GRANT ALL ON FUNCTION wb_public.request_export() TO wb_investigator;
GRANT ALL ON FUNCTION wb_public.request_export() TO wb_senioranalyst;
GRANT ALL ON FUNCTION wb_public.request_export() TO wb_admin;


--
-- Name: FUNCTION tg__graphql_subscription(); Type: ACL; Schema: wb_public; Owner: -
--

REVOKE ALL ON FUNCTION wb_public.tg__graphql_subscription() FROM PUBLIC;
GRANT ALL ON FUNCTION wb_public.tg__graphql_subscription() TO wb_postgraphile;
GRANT ALL ON FUNCTION wb_public.tg__graphql_subscription() TO wb_anonymous;
GRANT ALL ON FUNCTION wb_public.tg__graphql_subscription() TO wb_analyst;
GRANT ALL ON FUNCTION wb_public.tg__graphql_subscription() TO wb_investigator;
GRANT ALL ON FUNCTION wb_public.tg__graphql_subscription() TO wb_senioranalyst;
GRANT ALL ON FUNCTION wb_public.tg__graphql_subscription() TO wb_admin;


--
-- Name: FUNCTION update_attribute(attr wb_public.edited_attribute); Type: ACL; Schema: wb_public; Owner: -
--

REVOKE ALL ON FUNCTION wb_public.update_attribute(attr wb_public.edited_attribute) FROM PUBLIC;
GRANT ALL ON FUNCTION wb_public.update_attribute(attr wb_public.edited_attribute) TO wb_postgraphile;
GRANT ALL ON FUNCTION wb_public.update_attribute(attr wb_public.edited_attribute) TO wb_anonymous;
GRANT ALL ON FUNCTION wb_public.update_attribute(attr wb_public.edited_attribute) TO wb_analyst;
GRANT ALL ON FUNCTION wb_public.update_attribute(attr wb_public.edited_attribute) TO wb_investigator;
GRANT ALL ON FUNCTION wb_public.update_attribute(attr wb_public.edited_attribute) TO wb_senioranalyst;
GRANT ALL ON FUNCTION wb_public.update_attribute(attr wb_public.edited_attribute) TO wb_admin;


--
-- Name: FUNCTION update_event_tag(v_event_id uuid, v_tag_id uuid, v_is_held boolean); Type: ACL; Schema: wb_public; Owner: -
--

REVOKE ALL ON FUNCTION wb_public.update_event_tag(v_event_id uuid, v_tag_id uuid, v_is_held boolean) FROM PUBLIC;
GRANT ALL ON FUNCTION wb_public.update_event_tag(v_event_id uuid, v_tag_id uuid, v_is_held boolean) TO wb_postgraphile;
GRANT ALL ON FUNCTION wb_public.update_event_tag(v_event_id uuid, v_tag_id uuid, v_is_held boolean) TO wb_anonymous;
GRANT ALL ON FUNCTION wb_public.update_event_tag(v_event_id uuid, v_tag_id uuid, v_is_held boolean) TO wb_analyst;
GRANT ALL ON FUNCTION wb_public.update_event_tag(v_event_id uuid, v_tag_id uuid, v_is_held boolean) TO wb_investigator;
GRANT ALL ON FUNCTION wb_public.update_event_tag(v_event_id uuid, v_tag_id uuid, v_is_held boolean) TO wb_senioranalyst;
GRANT ALL ON FUNCTION wb_public.update_event_tag(v_event_id uuid, v_tag_id uuid, v_is_held boolean) TO wb_admin;


--
-- Name: FUNCTION validate_attribute_field(); Type: ACL; Schema: wb_public; Owner: -
--

REVOKE ALL ON FUNCTION wb_public.validate_attribute_field() FROM PUBLIC;
GRANT ALL ON FUNCTION wb_public.validate_attribute_field() TO wb_postgraphile;
GRANT ALL ON FUNCTION wb_public.validate_attribute_field() TO wb_anonymous;
GRANT ALL ON FUNCTION wb_public.validate_attribute_field() TO wb_analyst;
GRANT ALL ON FUNCTION wb_public.validate_attribute_field() TO wb_investigator;
GRANT ALL ON FUNCTION wb_public.validate_attribute_field() TO wb_senioranalyst;
GRANT ALL ON FUNCTION wb_public.validate_attribute_field() TO wb_admin;


--
-- Name: FUNCTION validate_events_checked_out(); Type: ACL; Schema: wb_public; Owner: -
--

REVOKE ALL ON FUNCTION wb_public.validate_events_checked_out() FROM PUBLIC;
GRANT ALL ON FUNCTION wb_public.validate_events_checked_out() TO wb_postgraphile;
GRANT ALL ON FUNCTION wb_public.validate_events_checked_out() TO wb_anonymous;
GRANT ALL ON FUNCTION wb_public.validate_events_checked_out() TO wb_analyst;
GRANT ALL ON FUNCTION wb_public.validate_events_checked_out() TO wb_investigator;
GRANT ALL ON FUNCTION wb_public.validate_events_checked_out() TO wb_senioranalyst;
GRANT ALL ON FUNCTION wb_public.validate_events_checked_out() TO wb_admin;


--
-- Name: TABLE attributes; Type: ACL; Schema: wb_public; Owner: -
--

GRANT SELECT ON TABLE wb_public.attributes TO wb_analyst;


--
-- Name: COLUMN attributes.metadata; Type: ACL; Schema: wb_public; Owner: -
--

GRANT INSERT(metadata) ON TABLE wb_public.attributes TO wb_admin;


--
-- Name: COLUMN attributes.value; Type: ACL; Schema: wb_public; Owner: -
--

GRANT INSERT(value) ON TABLE wb_public.attributes TO wb_admin;


--
-- Name: COLUMN attributes.value_edited; Type: ACL; Schema: wb_public; Owner: -
--

GRANT UPDATE(value_edited) ON TABLE wb_public.attributes TO wb_analyst;
GRANT INSERT(value_edited) ON TABLE wb_public.attributes TO wb_admin;


--
-- Name: COLUMN attributes.value_binary; Type: ACL; Schema: wb_public; Owner: -
--

GRANT INSERT(value_binary) ON TABLE wb_public.attributes TO wb_admin;


--
-- Name: COLUMN attributes.value_binary_edited; Type: ACL; Schema: wb_public; Owner: -
--

GRANT UPDATE(value_binary_edited) ON TABLE wb_public.attributes TO wb_analyst;
GRANT INSERT(value_binary_edited) ON TABLE wb_public.attributes TO wb_admin;


--
-- Name: COLUMN attributes.is_held; Type: ACL; Schema: wb_public; Owner: -
--

GRANT UPDATE(is_held) ON TABLE wb_public.attributes TO wb_analyst;
GRANT INSERT(is_held) ON TABLE wb_public.attributes TO wb_admin;


--
-- Name: COLUMN attributes.event_id; Type: ACL; Schema: wb_public; Owner: -
--

GRANT INSERT(event_id) ON TABLE wb_public.attributes TO wb_admin;


--
-- Name: COLUMN attributes.field_id; Type: ACL; Schema: wb_public; Owner: -
--

GRANT INSERT(field_id) ON TABLE wb_public.attributes TO wb_admin;


--
-- Name: COLUMN attributes.field_option_id; Type: ACL; Schema: wb_public; Owner: -
--

GRANT INSERT(field_option_id) ON TABLE wb_public.attributes TO wb_admin;


--
-- Name: COLUMN attributes.field_option_id_edited; Type: ACL; Schema: wb_public; Owner: -
--

GRANT UPDATE(field_option_id_edited) ON TABLE wb_public.attributes TO wb_analyst;
GRANT INSERT(field_option_id_edited) ON TABLE wb_public.attributes TO wb_admin;


--
-- Name: TABLE attributes_view; Type: ACL; Schema: wb_public; Owner: -
--

GRANT SELECT ON TABLE wb_public.attributes_view TO wb_analyst;


--
-- Name: TABLE batches; Type: ACL; Schema: wb_public; Owner: -
--

GRANT SELECT ON TABLE wb_public.batches TO wb_analyst;


--
-- Name: COLUMN batches.origin; Type: ACL; Schema: wb_public; Owner: -
--

GRANT INSERT(origin),UPDATE(origin) ON TABLE wb_public.batches TO wb_admin;


--
-- Name: COLUMN batches.total_processed; Type: ACL; Schema: wb_public; Owner: -
--

GRANT INSERT(total_processed),UPDATE(total_processed) ON TABLE wb_public.batches TO wb_admin;


--
-- Name: COLUMN batches.total_new; Type: ACL; Schema: wb_public; Owner: -
--

GRANT INSERT(total_new),UPDATE(total_new) ON TABLE wb_public.batches TO wb_admin;


--
-- Name: COLUMN batches.total_duplicates; Type: ACL; Schema: wb_public; Owner: -
--

GRANT INSERT(total_duplicates),UPDATE(total_duplicates) ON TABLE wb_public.batches TO wb_admin;


--
-- Name: COLUMN batches.total_errors; Type: ACL; Schema: wb_public; Owner: -
--

GRANT INSERT(total_errors),UPDATE(total_errors) ON TABLE wb_public.batches TO wb_admin;


--
-- Name: COLUMN batches.started_at; Type: ACL; Schema: wb_public; Owner: -
--

GRANT INSERT(started_at),UPDATE(started_at) ON TABLE wb_public.batches TO wb_admin;


--
-- Name: COLUMN batches.ended_at; Type: ACL; Schema: wb_public; Owner: -
--

GRANT INSERT(ended_at),UPDATE(ended_at) ON TABLE wb_public.batches TO wb_admin;


--
-- Name: TABLE events_tags; Type: ACL; Schema: wb_public; Owner: -
--

GRANT SELECT,DELETE ON TABLE wb_public.events_tags TO wb_analyst;


--
-- Name: COLUMN events_tags.tag_id; Type: ACL; Schema: wb_public; Owner: -
--

GRANT INSERT(tag_id) ON TABLE wb_public.events_tags TO wb_analyst;


--
-- Name: COLUMN events_tags.event_id; Type: ACL; Schema: wb_public; Owner: -
--

GRANT INSERT(event_id) ON TABLE wb_public.events_tags TO wb_analyst;


--
-- Name: TABLE fields; Type: ACL; Schema: wb_public; Owner: -
--

GRANT SELECT ON TABLE wb_public.fields TO wb_analyst;


--
-- Name: COLUMN fields.name; Type: ACL; Schema: wb_public; Owner: -
--

GRANT UPDATE(name) ON TABLE wb_public.fields TO wb_analyst;
GRANT INSERT(name) ON TABLE wb_public.fields TO wb_admin;


--
-- Name: COLUMN fields.display_name; Type: ACL; Schema: wb_public; Owner: -
--

GRANT UPDATE(display_name) ON TABLE wb_public.fields TO wb_analyst;
GRANT INSERT(display_name) ON TABLE wb_public.fields TO wb_admin;


--
-- Name: COLUMN fields.description; Type: ACL; Schema: wb_public; Owner: -
--

GRANT UPDATE(description) ON TABLE wb_public.fields TO wb_analyst;
GRANT INSERT(description) ON TABLE wb_public.fields TO wb_admin;


--
-- Name: COLUMN fields.is_archived; Type: ACL; Schema: wb_public; Owner: -
--

GRANT UPDATE(is_archived) ON TABLE wb_public.fields TO wb_analyst;
GRANT INSERT(is_archived) ON TABLE wb_public.fields TO wb_admin;


--
-- Name: TABLE tags; Type: ACL; Schema: wb_public; Owner: -
--

GRANT SELECT ON TABLE wb_public.tags TO wb_analyst;


--
-- Name: COLUMN tags.name; Type: ACL; Schema: wb_public; Owner: -
--

GRANT UPDATE(name) ON TABLE wb_public.tags TO wb_analyst;
GRANT INSERT(name) ON TABLE wb_public.tags TO wb_admin;


--
-- Name: COLUMN tags.display_name; Type: ACL; Schema: wb_public; Owner: -
--

GRANT UPDATE(display_name) ON TABLE wb_public.tags TO wb_analyst;
GRANT INSERT(display_name) ON TABLE wb_public.tags TO wb_admin;


--
-- Name: COLUMN tags.description; Type: ACL; Schema: wb_public; Owner: -
--

GRANT UPDATE(description) ON TABLE wb_public.tags TO wb_analyst;
GRANT INSERT(description) ON TABLE wb_public.tags TO wb_admin;


--
-- Name: TABLE field_options; Type: ACL; Schema: wb_public; Owner: -
--

GRANT SELECT ON TABLE wb_public.field_options TO wb_analyst;


--
-- Name: COLUMN field_options.name; Type: ACL; Schema: wb_public; Owner: -
--

GRANT UPDATE(name) ON TABLE wb_public.field_options TO wb_analyst;
GRANT INSERT(name),UPDATE(name) ON TABLE wb_public.field_options TO wb_admin;


--
-- Name: COLUMN field_options.display_name; Type: ACL; Schema: wb_public; Owner: -
--

GRANT UPDATE(display_name) ON TABLE wb_public.field_options TO wb_analyst;
GRANT INSERT(display_name),UPDATE(display_name) ON TABLE wb_public.field_options TO wb_admin;


--
-- Name: COLUMN field_options.description; Type: ACL; Schema: wb_public; Owner: -
--

GRANT UPDATE(description) ON TABLE wb_public.field_options TO wb_analyst;
GRANT INSERT(description),UPDATE(description) ON TABLE wb_public.field_options TO wb_admin;


--
-- Name: COLUMN field_options.is_archived; Type: ACL; Schema: wb_public; Owner: -
--

GRANT UPDATE(is_archived) ON TABLE wb_public.field_options TO wb_analyst;
GRANT INSERT(is_archived),UPDATE(is_archived) ON TABLE wb_public.field_options TO wb_admin;


--
-- Name: COLUMN field_options.field_id; Type: ACL; Schema: wb_public; Owner: -
--

GRANT INSERT(field_id),UPDATE(field_id) ON TABLE wb_public.field_options TO wb_admin;


--
-- Name: TABLE reviews; Type: ACL; Schema: wb_public; Owner: -
--

GRANT SELECT ON TABLE wb_public.reviews TO wb_analyst;


--
-- Name: COLUMN reviews.user_id; Type: ACL; Schema: wb_public; Owner: -
--

GRANT INSERT(user_id) ON TABLE wb_public.reviews TO wb_analyst;
GRANT INSERT(user_id) ON TABLE wb_public.reviews TO wb_senioranalyst;


--
-- Name: COLUMN reviews.event_id; Type: ACL; Schema: wb_public; Owner: -
--

GRANT INSERT(event_id) ON TABLE wb_public.reviews TO wb_analyst;
GRANT INSERT(event_id) ON TABLE wb_public.reviews TO wb_senioranalyst;


--
-- Name: COLUMN reviews.action; Type: ACL; Schema: wb_public; Owner: -
--

GRANT INSERT(action),UPDATE(action) ON TABLE wb_public.reviews TO wb_analyst;
GRANT INSERT(action) ON TABLE wb_public.reviews TO wb_senioranalyst;


--
-- Name: COLUMN reviews.reason; Type: ACL; Schema: wb_public; Owner: -
--

GRANT INSERT(reason),UPDATE(reason) ON TABLE wb_public.reviews TO wb_analyst;
GRANT INSERT(reason) ON TABLE wb_public.reviews TO wb_senioranalyst;


--
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: public; Owner: -
--

ALTER DEFAULT PRIVILEGES FOR ROLE waterbear IN SCHEMA public REVOKE ALL ON SEQUENCES  FROM waterbear;
ALTER DEFAULT PRIVILEGES FOR ROLE waterbear IN SCHEMA public GRANT SELECT,USAGE ON SEQUENCES  TO wb_postgraphile;


--
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: public; Owner: -
--

ALTER DEFAULT PRIVILEGES FOR ROLE waterbear IN SCHEMA public REVOKE ALL ON FUNCTIONS  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE waterbear IN SCHEMA public REVOKE ALL ON FUNCTIONS  FROM waterbear;
ALTER DEFAULT PRIVILEGES FOR ROLE waterbear IN SCHEMA public GRANT ALL ON FUNCTIONS  TO wb_postgraphile;
ALTER DEFAULT PRIVILEGES FOR ROLE waterbear IN SCHEMA public GRANT ALL ON FUNCTIONS  TO wb_anonymous;
ALTER DEFAULT PRIVILEGES FOR ROLE waterbear IN SCHEMA public GRANT ALL ON FUNCTIONS  TO wb_analyst;
ALTER DEFAULT PRIVILEGES FOR ROLE waterbear IN SCHEMA public GRANT ALL ON FUNCTIONS  TO wb_investigator;
ALTER DEFAULT PRIVILEGES FOR ROLE waterbear IN SCHEMA public GRANT ALL ON FUNCTIONS  TO wb_senioranalyst;
ALTER DEFAULT PRIVILEGES FOR ROLE waterbear IN SCHEMA public GRANT ALL ON FUNCTIONS  TO wb_admin;


--
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: wb_hidden; Owner: -
--

ALTER DEFAULT PRIVILEGES FOR ROLE waterbear IN SCHEMA wb_hidden REVOKE ALL ON SEQUENCES  FROM waterbear;
ALTER DEFAULT PRIVILEGES FOR ROLE waterbear IN SCHEMA wb_hidden GRANT SELECT,USAGE ON SEQUENCES  TO wb_postgraphile;


--
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: wb_hidden; Owner: -
--

ALTER DEFAULT PRIVILEGES FOR ROLE waterbear IN SCHEMA wb_hidden REVOKE ALL ON FUNCTIONS  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE waterbear IN SCHEMA wb_hidden REVOKE ALL ON FUNCTIONS  FROM waterbear;
ALTER DEFAULT PRIVILEGES FOR ROLE waterbear IN SCHEMA wb_hidden GRANT ALL ON FUNCTIONS  TO wb_postgraphile;
ALTER DEFAULT PRIVILEGES FOR ROLE waterbear IN SCHEMA wb_hidden GRANT ALL ON FUNCTIONS  TO wb_anonymous;
ALTER DEFAULT PRIVILEGES FOR ROLE waterbear IN SCHEMA wb_hidden GRANT ALL ON FUNCTIONS  TO wb_analyst;
ALTER DEFAULT PRIVILEGES FOR ROLE waterbear IN SCHEMA wb_hidden GRANT ALL ON FUNCTIONS  TO wb_investigator;
ALTER DEFAULT PRIVILEGES FOR ROLE waterbear IN SCHEMA wb_hidden GRANT ALL ON FUNCTIONS  TO wb_senioranalyst;
ALTER DEFAULT PRIVILEGES FOR ROLE waterbear IN SCHEMA wb_hidden GRANT ALL ON FUNCTIONS  TO wb_admin;


--
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: wb_public; Owner: -
--

ALTER DEFAULT PRIVILEGES FOR ROLE waterbear IN SCHEMA wb_public REVOKE ALL ON SEQUENCES  FROM waterbear;
ALTER DEFAULT PRIVILEGES FOR ROLE waterbear IN SCHEMA wb_public GRANT SELECT,USAGE ON SEQUENCES  TO wb_postgraphile;


--
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: wb_public; Owner: -
--

ALTER DEFAULT PRIVILEGES FOR ROLE waterbear IN SCHEMA wb_public REVOKE ALL ON FUNCTIONS  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE waterbear IN SCHEMA wb_public REVOKE ALL ON FUNCTIONS  FROM waterbear;
ALTER DEFAULT PRIVILEGES FOR ROLE waterbear IN SCHEMA wb_public GRANT ALL ON FUNCTIONS  TO wb_postgraphile;
ALTER DEFAULT PRIVILEGES FOR ROLE waterbear IN SCHEMA wb_public GRANT ALL ON FUNCTIONS  TO wb_anonymous;
ALTER DEFAULT PRIVILEGES FOR ROLE waterbear IN SCHEMA wb_public GRANT ALL ON FUNCTIONS  TO wb_analyst;
ALTER DEFAULT PRIVILEGES FOR ROLE waterbear IN SCHEMA wb_public GRANT ALL ON FUNCTIONS  TO wb_investigator;
ALTER DEFAULT PRIVILEGES FOR ROLE waterbear IN SCHEMA wb_public GRANT ALL ON FUNCTIONS  TO wb_senioranalyst;
ALTER DEFAULT PRIVILEGES FOR ROLE waterbear IN SCHEMA wb_public GRANT ALL ON FUNCTIONS  TO wb_admin;


--
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: -; Owner: -
--

ALTER DEFAULT PRIVILEGES FOR ROLE waterbear REVOKE ALL ON FUNCTIONS  FROM PUBLIC;


--
-- PostgreSQL database dump complete
--

