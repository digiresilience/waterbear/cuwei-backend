FROM node:lts-buster as builder
ARG APP_DIR=/app
RUN mkdir -p ${APP_DIR}
COPY package.json ${APP_DIR}/
COPY .npmrc ${APP_DIR}/
RUN chown -R node:node ${APP_DIR}
USER node
WORKDIR ${APP_DIR}
RUN yarn install
RUN rm -f .npmrc

FROM node:lts-buster
RUN DEBIAN_FRONTEND=noninteractive apt-get update && \
    apt-get install -y --no-install-recommends \
    dumb-init

ARG APP_DIR=/app
RUN mkdir -p ${APP_DIR}
RUN chown -R node:node ${APP_DIR}
COPY docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh

USER node
WORKDIR ${APP_DIR}
COPY src src/
COPY db db/
COPY test test/
COPY package.json package.json
COPY --from=builder ${APP_DIR}/node_modules node_modules

LABEL maintainer="Abel Luck <abel@guardianproject.info>"
ARG VCS_URL="https://gitlab.com/digiresilience/waterbear/cuwei-backend.git"
ARG BUILD_DATE
ARG VCS_REF
ARG VERSION
LABEL org.label-schema.build-date="${BUILD_DATE}" \
      org.label-schema.name="cuwei-backend" \
      org.label-schema.license="AGPL-3.0" \
      org.label-schema.description="A backend for the Waterbear analyst QA app" \
      org.label-schema.url="https://digiresilience.org" \
      org.label-schema.vcs-url="${VCS_URL}" \
      org.label-schema.vcs-url="${VCS_REF}" \
      org.label-schema.vcs-type="git" \
      org.label-schema.vendor="CDR" \
      org.label-schema.schema-version="${VERSION}"

ENV PORT 3000
ENV HOST 0.0.0.0
ENV NODE_ENV production
ENV TZ UTC
ENV APP_DIR ${APP_DIR}
ENTRYPOINT ["/docker-entrypoint.sh"]
