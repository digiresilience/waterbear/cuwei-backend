# analyst-backend

> the backend for the analysts

the counterpart to [analyst-frontend](https://gitlab.com/digiresilience/waterbear/analyst-frontend)

### What? Why?

TODO

## Usage

### Prerequisites

For local development:

- node.js >= 13.x **!!!**
- yarn
- postgresql
- docker
- docker-compose

### Run the thing

Setup your env file

```bash
cp env-sample .env
# edit .env
```

```bash

# install deps
yarn

# bootstrap database
docker-compose build && docker-compose down -v && docker-compose up  -d

# setup db schema
yarn db:reset

# populate with default data
yarn db:current

# start the server
yarn start

# checkout http://localhost:3000/graphiql
```

### Run tests

```bash
# install deps
yarn

# bootstrap database
docker-compose build && docker-compose down -v && docker-compose up  -d

# run the unit tests
yarn test:unit
# or
yarn test:watch:unit

# run the integration tests
yarn test:integration
# or
yarn test:watch:integration
```

## Development

Much of this project is implemented in postgres sql, so you need to be familiar
with how to manage migrations and create new migrations.

We use [graphile migrate](https://github.com/graphile/migrate) to manage sql
migrations. It is a little different than what you're used to.

Final comitted migrations live in `db/migrations/committed/`. These are
immutable, and automatically generated. Do not put anything in this directory
manually.

Migrations in progress live in `db/migrations/current.sql`. This file is in
version control. Changes can be made slowly overtime before being "comitted" by
graphile migrate to the committed directory.

Working on migrations goes like so:

1. Edit `db/migrations/current.sql`
2. Apply the current migrations with `yarn db:current`
3. Repeat

You can use `yarn db:watch` and any edits to `db/migrations/current.sql` will
be instantly applied.

Due to the create and replace nature of SQL you will likely have to copy code
from existing committed migrations into current and tweak them.

The current migration **must be idempotent** (this is your responsibility); i.e.
it should be able to be ran multiple times and have the same result.

Once you are happy with the state of the current migration, you are ready to
commit it (to the migrations folder, not to git. it should have been in git all
along).

To commit a current migration:

1. `yarn db:commit`

This will move the current migration into `db/migrations/committed/` and create
a new empty current migration.

Running `yarn db:migrate` on existing deployments will apply un-applied
committed migrations. Current migrations should only ever be used for development.

## License

[![License GNU AGPL v3.0](https://img.shields.io/badge/License-AGPL%203.0-lightgrey.svg)](https://gitlab.com/digiresilience/waterbear/analyst-backend/blob/master/LICENSE.md)

analyst-backend is a free software project licensed under the GNU Affero
General Public License v3.0 (GNU AGPLv3) by [The Center for Digital
Resilience](https://digiresilience.org) and [Guardian
Project](https://guardianproject.info).
