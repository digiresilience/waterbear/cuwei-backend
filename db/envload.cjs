#!/usr/bin/env node
/**
 * This is a wrapper file for our test suite
 *
 * When invoked it will load the env-test file using dotenv
 *
 * Except when the CI variable is truthy, then env-i-test is loaded
 */
/* eslint-disable import/no-extraneous-dependencies */
const spawn = require('cross-spawn')
const path = require('path')
const dotenv = require('dotenv')
const dotenvExpand = require('dotenv-expand')


let paths;
if (process.env.CI) {
  paths = [path.resolve(path.join(__dirname, './env-ci-test'))]
} else {
  paths = [path.resolve(path.join(__dirname, './env-test'))]
}
paths.forEach(function (env) {
  dotenvExpand(dotenv.config({ path: path.resolve(env) }))
})


const args = process.argv.slice(2);
const command = "npx"
console.log(command, args)
spawn(command, args, { stdio: 'inherit' })
  .on('exit', function (exitCode) {
    process.exit(exitCode)
  })
