# Conventions used in this database schema

## Schemas

We use these schema namesapces:

1. `wb_public` - tables and functions to be exposed to GraphQL
2. `wb_hidden` - same privileges as wb_public, but simply not exposed to GraphQL
3. `wb_private` - secrets that require elevated privileges to access. nothing in this table is exposed via graphql

## Roles

There are five primary postgres roles used to access the database. Here they
are in order of access from MOST to LEAST access.

1. `wb_admin` - the role used by administrator users
2. `wb_senioranalyst` - the role used by senior core analysts
3. `wb_coreanalyst` - the role used by core analysts
4. `wb_investigator` - the role used by investigators
5. `wb_anonymous` - the role used by unknown users, the default and least privileged role

In addition to these user roles there is an additional service role used by
postgraphile:

- `wb_postgraphile` - used by postgraphile to introspect and setup the graphql schema

Postgraphile uses this service role initially, but then for every request made
by a user "sudos" into the appropriate user role from the list above.

## Style Guide

### Naming

- snake_case for tables, functions, columns (avoids having to put them in quotes
  in most cases)
- plural table names (avoids conflicts with e.g. `user` built ins, is better
  depluralized by PostGraphile)
- trigger functions valid for one table only are named
  tg\_[table_name]\_\_[task_name]
- trigger functions valid for many tables are named tg\_\_[task_name]
- trigger names should be prefixed with `_NNN_` where NNN is a three digit
  number that defines the priority of the trigger (use _500_ if unsure)
- prefer lowercase over UPPERCASE

### Security

- all `security definer` functions should define `set search_path from current`
  due to `CVE-2018-1058`
- [`@omit` smart
  comments](https://www.graphile.org/postgraphile/smart-comments/) should not
  be used for permissions, instead deferring to PostGraphile's RBAC support
- all tables (public or not) should enable RLS
- relevant RLS policy should be defined before granting a permission
- `grant select` should never specify a column list; instead use one-to-one
  relations as permission boundaries
- `grant insert` and `grant update` must ALWAYS specify a column list

### Explicitness

- all functions should explicitly state immutable/stable/volatile
- do not override `search_path` during migrations or in server code - prefer to
  explicitly state schemas

### Functions

- if a function can be expressed as a single SQL statement it should use the
  `sql` language if possible. Other functions should use `plpgsql`.
- be aware of the function inlining rules:
  https://wiki.postgresql.org/wiki/Inlining_of_SQL_functions

### Relations

- all foreign key `references` statements should have `on delete` clauses. Some
  may also want `on update` clauses, but that's optional
- all comments should be defined using '"escape" string constants' - e.g.
  `E'...'` - because this more easily allows adding special characters such as
  newlines
- defining things (primary key, checks, unique constraints, etc) within the
  `create table` statement is preferable to adding them after

### General conventions (e.g. for PostGraphile compatibility)

- @omit smart comments should be used heavily to remove fields we don't
  currently need in GraphQL - we can always remove them later

### Definitions

Please adhere to the following templates (respecting newlines):

Tables:

```sql
create table <schema_name>.<table_name> (
  ...
);
```

SQL functions:

```sql
create function <fn_name>(<args...>) returns <return_value> as $$
  select ...
  from ...
  inner join ...
  on ...
  where ...
  and ...
  order by ...
  limit ...;
$$ language sql <strict?> <immutable|stable|volatile> <security definer?> set search_path from current;
```

PL/pgSQL functions:

```sql
create function <fn_name>(<args...>) returns <return_value> as $$
declare
  v_[varname] <type>[ = <default>];
  ...
begin
  if ... then
    ...
  end if;
  return <value>;
end;
$$ language plpgsql <strict?> <immutable|stable|volatile> <security definer?> set search_path from current;
```

Triggers:

```sql
create trigger _NNN_trigger_name
  <before|after> <insert|update|delete> on <schema_name>.<table_name>
  for each row [when (<condition>)]
  execute procedure <schema_name.function_name>(...);
```

Comments:

```sql
comment on <table|column|function|...> <fully.qualified.name> is
  E'...';
```

# License

This CONVENTIONS file was based on the conventions of the Graphile Starter
project (which, at time of writing (2020-02-03), is located at
https://github.com/graphile/starter) and is originally copyright Graphile Ltd.
