--! Previous: -
--! Hash: sha1:ce86d899c9dfb3d0871894a8536a8dc8aae1f610

-- region Bootstrap
drop schema if exists wb_public cascade;
alter default privileges revoke all on sequences from public;
alter default privileges revoke all on functions from public;

-- By default the public schema is owned by `postgres`; we need superuser privileges to change this :(
-- alter schema public owner to waterbear;
revoke all on schema public from public;
grant all on schema public to :DATABASE_OWNER;

create schema wb_public;
grant usage on schema
    public,
    wb_public
    to
    :DATABASE_VISITOR,
    wb_admin,
    wb_anonymous,
    wb_analyst,
    wb_senioranalyst,
    wb_investigator;


/**********/

drop schema if exists wb_hidden cascade;
create schema wb_hidden;
grant usage on schema wb_hidden to :DATABASE_VISITOR;
alter default privileges in schema wb_hidden grant usage, select on sequences to :DATABASE_VISITOR;

/**********/

alter default privileges in schema public, wb_public, wb_hidden grant usage, select on sequences to :DATABASE_VISITOR;
alter default privileges in schema public, wb_public, wb_hidden
    grant execute on functions to
    :DATABASE_VISITOR,
    wb_admin,
    wb_anonymous,
    wb_analyst,
    wb_senioranalyst,
    wb_investigator;

/**********/

drop schema if exists wb_private cascade;
create schema wb_private;
-- endregion

-- region UtilFunctions
/* ------------------------------------------------------------------ */
create function wb_private.tg__add_job() returns trigger as
$$
begin
    perform graphile_worker.add_job(tg_argv[0], json_build_object('id', NEW.id),
                                    coalesce(tg_argv[1], public.gen_random_uuid()::text));
    return NEW;
end;
$$ language plpgsql volatile
                    security definer
                    set search_path to pg_catalog, public, pg_temp;
comment on function wb_private.tg__add_job() is
    E'Useful shortcut to create a job on insert/update. Pass the task name as the first trigger argument, and optionally the queue name as the second argument. The record id will automatically be available on the JSON payload.';

/* ------------------------------------------------------------------ */
create function wb_private.tg__timestamps() returns trigger as
$$
begin
    NEW.created_at = (case when TG_OP = 'INSERT' then NOW() else OLD.created_at end);
    NEW.updated_at = (case
                          when TG_OP = 'UPDATE' and OLD.updated_at >= NOW()
                              then OLD.updated_at + interval '1 millisecond'
                          else NOW() end);
    return NEW;
end;
$$ language plpgsql volatile
                    set search_path to pg_catalog, public, pg_temp;
comment on function wb_private.tg__timestamps() is
    E'This trigger should be called on all tables with created_at, updated_at - it ensures that they cannot be manipulated and that updated_at will always be larger than the previous updated_at.';

-- endregion


-- region Users and Sessions
/* ------------------------------------------------------------------ */
create table wb_private.sessions
(
    uuid           uuid        not null default gen_random_uuid() primary key,
    user_id        uuid        not null,
    -- You could add access restriction columns here if you want, e.g. for OAuth scopes.
    created_at     timestamptz not null default now(),
    last_active_at timestamptz not null default now()
);
alter table wb_private.sessions
    enable row level security;

/* ------------------------------------------------------------------ */

create function wb_public.current_session_id() returns uuid as
$$
select nullif(pg_catalog.current_setting('jwt.claims.session_id', true), '')::uuid;
$$ language sql stable;
comment on function wb_public.current_session_id() is
    E'Handy method to get the current session ID.';
-- We've put this in public, but omitted it, because it's often useful for debugging auth issues.

/*
 * A less secure but more performant version of this function would be just:
 *
 *  select nullif(pg_catalog.current_setting('jwt.claims.user_id', true), '')::int;
 *
 * The increased security of this implementation is because even if someone gets
 * the ability to run SQL within this transaction they cannot impersonate
 * another user without knowing their session_id (which should be closely
 * guarded).
 */
create function wb_public.current_user_id() returns uuid as
$$
select user_id
from wb_private.sessions
where uuid = wb_public.current_session_id();
$$ language sql stable
                security definer
                set search_path to pg_catalog, public, pg_temp;
comment on function wb_public.current_user_id() is
    E'Handy method to get the current user ID for use in RLS policies, etc; in GraphQL, use `currentUser{id}` instead.';
-- We've put this in public, but omitted it, because it's often useful for debugging auth issues.

/* ------------------------------------------------------------------ */

create type wb_public.role_type as
    ENUM ('none','admin','core_analyst','senior_core_analyst','investigator');

/* ------------------------------------------------------------------ */

create table wb_public.users
(
    id         uuid                not null default uuid_generate_v1mc() primary key,
    email      citext              not null,
    name       text                not null,
    avatar_url text,
    user_role  wb_public.role_type not null default 'none',
    is_active  boolean             not null default false,
    created_at timestamptz         not null default now(),
    updated_at timestamptz         not null default now(),
    created_by text                not null,
    constraint users_email_validity check (email ~* '^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+[.][A-Za-z]+$'),
    constraint users_avatar_validity check (avatar_url ~ '^https?://[^/]+'),
    constraint users_email_unique unique (email)
);
comment on table wb_public.users is
    E'A user who can log in to the application.';
comment on column wb_public.users.id is
    E'Unique identifier for the user.';
comment on column wb_public.users.email is
    E'The email address of the user.';
comment on column wb_public.users.name is
    E'Public-facing name (or pseudonym) of the user.';
comment on column wb_public.users.avatar_url is
    E'Optional avatar URL.';
comment on column wb_public.users.user_role is
    E'The role that defines the user''s privileges.';
comment on column wb_public.users.is_active is
    E'If false, the user is not allowed to login or access the application';

alter table wb_public.users
    enable row level security;

alter table wb_private.sessions
    add constraint sessions_user_id_fkey foreign key ("user_id") references wb_public.users on delete cascade;
create index on wb_private.sessions (user_id);

-- wb_public perms default
create policy access_self on wb_public.users to wb_anonymous using (id = wb_public.current_user_id());
--create policy update_self on wb_public.users for update using (id = wb_public.current_user_id());
grant select on wb_public.users to wb_anonymous;
grant update (name, avatar_url) on wb_public.users to :DATABASE_VISITOR, wb_analyst, wb_investigator;

-- wb_public perms for wb_admin
create policy access_all on wb_public.users to wb_admin using (true);
grant update (email, name, avatar_url, is_active, user_role) on wb_public.users to wb_admin;
grant select on wb_public.users to wb_admin;
grant insert (email, name, avatar_url, user_role, is_active, created_by) on wb_public.users to wb_admin;
grant update (email, name, avatar_url, user_role, is_active, created_by) on wb_public.users to wb_admin;

create trigger _100_timestamps
    before insert or update
    on wb_public.users
    for each row
execute procedure wb_private.tg__timestamps();

/*
create function wb_private.tg_users__make_first_user_admin() returns trigger as $$
begin
  NEW.user_role = 'admin';
  return NEW;
end;
$$ language plpgsql volatile set search_path to pg_catalog, public, pg_temp;

create trigger _200_make_first_user_admin
  before insert on wb_public.users
  for each row
  when (NEW.id = 1)
  execute procedure wb_private.tg_users__make_first_user_admin();
*/

/* ------------------------------------------------------------------ */

create function wb_public.current_user() returns wb_public.users as
$$
select users.*
from wb_public.users
where id = wb_public.current_user_id();
$$ language sql stable;
comment on function wb_public.current_user() is
    E'The currently logged in user (or null if not logged in).';

/* ------------------------------------------------------------------ */

create function wb_public.logout() returns void as
$$
begin
    -- Delete the session
    delete from wb_private.sessions where uuid = wb_public.current_session_id();
    -- Clear the identifier from the transaction
    perform set_config('jwt.claims.session_id', '', true);
end;
$$ language plpgsql security definer
                    volatile
                    set search_path to pg_catalog, public, pg_temp;
-- endregion


-- region Graphql Subscriptions
/* ------------------------------------------------------------------ */
create function wb_public.tg__graphql_subscription() returns trigger as
$$
declare
    v_process_new    bool = (TG_OP = 'INSERT' OR TG_OP = 'UPDATE');
    v_process_old    bool = (TG_OP = 'UPDATE' OR TG_OP = 'DELETE');
    v_event          text = TG_ARGV[0];
    v_topic_template text = TG_ARGV[1];
    v_attribute      text = TG_ARGV[2];
    v_record         record;
    v_sub            text;
    v_topic          text;
    v_i              int  = 0;
    v_last_topic     text;
begin
    for v_i in 0..1
        loop
            if (v_i = 0) and v_process_new is true then
                v_record = new;
            elsif (v_i = 1) and v_process_old is true then
                v_record = old;
            else
                continue;
            end if;
            if v_attribute is not null then
                execute 'select $1.' || quote_ident(v_attribute)
                    using v_record
                    into v_sub;
            end if;
            if v_sub is not null then
                v_topic = replace(v_topic_template, '$1', v_sub);
            else
                v_topic = v_topic_template;
            end if;
            if v_topic is distinct from v_last_topic then
                -- This if statement prevents us from triggering the same notification twice
                v_last_topic = v_topic;
                perform pg_notify(v_topic, json_build_object(
                        'event', v_event,
                        'subject', v_sub
                    )::text);
            end if;
        end loop;
    return v_record;
end;
$$ language plpgsql volatile;
comment on function wb_public.tg__graphql_subscription() is
    E'This function enables the creation of simple focused GraphQL subscriptions using database triggers. Read more here: https://www.graphile.org/postgraphile/subscriptions/#custom-subscriptions';

create trigger _500_gql_update
    after update
    on wb_public.users
    for each row
execute procedure wb_public.tg__graphql_subscription(
        'userChanged', -- the "event" string, useful for the client to know what happened
        'graphql:user:$1', -- the "topic" the event will be published to, as a template
        'id' -- If specified, `$1` above will be replaced with NEW.id or OLD.id from the trigger.
    );

-- endregion

/* ------------------------------------------------------------------ */

-- region EventTags
create table wb_public.tags
(
    id           uuid        not null default uuid_generate_v1mc() primary key,
    name         citext      not null,
    display_name citext      not null,
    description  text,
    created_at   timestamptz          default now(),
    updated_at   timestamptz not null default now(),
    constraint tags_name_validity check (name !~
                                         '[\s\b\f\u0020\u00A0\u1680\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u202f\u205f\u3000/\\]'),
    constraint tags_name_unique unique (name)
);
comment on column wb_public.tags.name is 'The name of the tag, must not contain spaces nor slashes.';
comment on column wb_public.tags.display_name is 'The human friendly name of the tag';
comment on column wb_public.tags.description is 'A description of what exactly this tag represents';
alter table wb_public.tags
    enable row level security;

-- wb_public perms default
create policy access_all on wb_public.tags to wb_analyst using (true);
grant select on wb_public.tags to wb_analyst;
grant update (name, display_name, description) on wb_public.tags to wb_analyst;

-- wb_public perms for wb_admin
grant insert (name, display_name, description) on wb_public.tags to wb_admin;

create trigger _100_timestamps
    before insert or update
    on wb_public.tags
    for each row
execute procedure wb_private.tg__timestamps();
-- endregion

/* ------------------------------------------------------------------ */

-- region Field and Field Options

create type wb_public.field_types as enum ('option', 'binary', 'text');

create table wb_public.fields
(
    id           uuid                  not null default uuid_generate_v1mc() primary key,
    name         text                  not null,
    field_type   wb_public.field_types not null,
    display_name text                  not null,
    description  text,
    is_archived  boolean               not null default false,
    created_at   timestamptz           not null default now(),
    updated_at   timestamptz           not null default now(),
    constraint field_name_validity check (name !~
                                          '[\s\b\f\u0020\u00a0\u1680\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u202f\u205f\u3000/\\]'),
    constraint field_name_unique unique (name),
    constraint field_name_lowercase check (name = lower(name))
);
comment on table wb_public.fields is e'Fields can be used to associate a static and specific type to an event. Examples are: states, countries, etc.';
comment on column wb_public.fields.name is 'The type of the field, must not contain spaces nor slashes';
comment on column wb_public.fields.display_name is 'The human friendly name of the field';
comment on column wb_public.fields.description is 'An optional description of the field';

alter table wb_public.fields
    enable row level security;

create index on wb_public.fields (name);

-- wb_public perms default
create policy access_all on wb_public.fields to wb_analyst using (true);
grant select on wb_public.fields to wb_analyst;
grant update (name, display_name, description, is_archived) on wb_public.fields to wb_analyst;

-- wb_public perms for wb_admin
grant insert (name, display_name, description, is_archived) on wb_public.fields to wb_admin;

create trigger _100_timestamps
    before insert or update
    on wb_public.fields
    for each row
execute procedure wb_private.tg__timestamps();

/* ------------------------------------------------------------------ */

create table wb_public.field_options
(
    id           uuid        not null default uuid_generate_v1mc() primary key,
    name         text        not null,
    display_name text        not null,
    description  text,
    is_archived  boolean     not null default false,
    field_id     uuid        not null,
    created_at   timestamptz not null default now(),
    updated_at   timestamptz not null default now(),
    constraint field_option_name_validity check (name !~
                                                 '[\s\b\f\u0020\u00a0\u1680\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u202f\u205f\u3000/\\]'),
    constraint field_option_name_field_id_unique unique (name, field_id)
);

comment on table wb_public.field_options is e'An option that belongs to a specific field. Example: Field Option "AL"/Alaska for the field "State"';
comment on column wb_public.field_options.name is 'The name of the option, must not contain spaces nor slashes';
comment on column wb_public.field_options.display_name is 'The human friendly name of the option';
comment on column wb_public.field_options.description is 'An optional description of the field';
alter table wb_public.field_options
    enable row level security;

alter table wb_public.field_options
    add constraint field_options_field_id_fkey foreign key (field_id) references wb_public.fields on delete restrict;

create index on wb_public.field_options (name);
create index on wb_public.field_options (field_id);

-- wb_public perms default
create policy access_all on wb_public.field_options to wb_analyst using (true);
grant select on wb_public.field_options to wb_analyst;
grant update (name, display_name, description, is_archived) on wb_public.field_options to wb_analyst;

-- wb_public perms for wb_admin
grant insert (name, display_name, description, is_archived, field_id) on wb_public.field_options to wb_admin;
grant update (name, display_name, description, is_archived, field_id) on wb_public.field_options to wb_admin;

create trigger _100_timestamps
    before insert or update
    on wb_public.field_options
    for each row
execute procedure wb_private.tg__timestamps();

-- endregion

/* ------------------------------------------------------------------ */

-- region Events

create table wb_public.batches
(
    id                  uuid        not null default uuid_generate_v1mc() primary key,
    origin              text,
    total_processed     int,
    total_new           int,
    total_duplicates    int,
    total_errors        int,

    started_at          timestamptz,
    ended_at            timestamptz,

    created_at          timestamptz not null default now(),
    updated_at          timestamptz not null default now()
);
comment on table wb_public.batches is e'Events are imported in batches';

alter table wb_public.batches
enable row level security;

-- wb_public perms default
create policy access_all on wb_public.batches to wb_analyst using (true);
grant select on wb_public.batches to wb_analyst;

-- wb_public perms for wb_admin
grant update (origin, total_processed, total_new, total_duplicates, total_errors, started_at, ended_at) on wb_public.batches to wb_admin;
grant insert (origin, total_processed, total_new, total_duplicates, total_errors, started_at, ended_at) on wb_public.batches to wb_admin;

create trigger _100_timestamps
    before insert or update
    on wb_public.batches
    for each row
execute procedure wb_private.tg__timestamps();

-- endregion

/* ------------------------------------------------------------------ */

-- region Events

create sequence wb_public.events_event_number_seq start 1000;
create table wb_public.events
(
    id                  uuid         not null default uuid_generate_v1mc() primary key,
    reported_at         timestamptz  not null,
    event_number        int          not null default nextval('wb_public.events_event_number_seq'),
    is_locked           boolean      not null default false,
    is_archived         boolean      not null default false,
    is_held             boolean      not null default false,
    is_approved         boolean      not null default false,
    batch_id            uuid,
    checked_out_at      timestamptz,
    checked_out_user_id uuid,
    import_id           text         not null,
    created_at          timestamptz  not null default now(),
    updated_at          timestamptz  not null default now(),
    constraint events_import_id_unique unique (import_id),
    constraint events_event_number_unique unique (event_number)
);

comment on table  wb_public.events is 'An event that was sighted in the wild';
comment on column wb_public.events.reported_at is 'The time at which the event was reported. always in utc.';
comment on column wb_public.events.created_at is 'The datetime at which the event was asserted to the database. always in utc.';
comment on column wb_public.events.event_number is 'An auto incrementing number to identify the event';
comment on column wb_public.events.is_locked is 'Whether the event is locked from being edited further.';
comment on column wb_public.events.is_archived is 'Whether the event is soft-deleted.';
comment on column wb_public.events.is_held is 'whether the event is held for review.';
comment on column wb_public.events.is_approved is 'Whether the event is has been approved';
comment on column wb_public.events.import_id is e'The id used by the upstream system to identify this event. Used to prevent duplicate imports.';
comment on column wb_public.events.checked_out_at is 'The timestamp at which the event was checked out. if this is null then the event is not checked out. always in utc.';
comment on column wb_public.events.checked_out_user_id is 'The id of the user who has this event checked out. if this is null then the event is not checked out';

alter table wb_public.events
    enable row level security;

alter table wb_public.events
    add constraint events_checked_out_user_id_fkey foreign key (checked_out_user_id) references wb_public.users on delete restrict;
create index on wb_public.events (checked_out_user_id);

alter table wb_public.events
    add constraint events_batch_id_fkey foreign key (batch_id) references wb_public.batches on delete restrict;
create index on wb_public.events (batch_id);

-- wb_public perms default
create policy access_all on wb_public.events to wb_analyst using (true);
grant select on wb_public.events to wb_analyst;
grant update (reported_at, is_locked, is_archived, is_held,
              is_approved) on wb_public.events to wb_analyst;

-- wb_public perms for wb_admin
grant insert (reported_at, is_locked, is_archived, is_held, is_approved) on wb_public.events to wb_admin;

create trigger _100_timestamps
    before insert or update
    on wb_public.events
    for each row
execute procedure wb_private.tg__timestamps();

create function wb_public.validate_events_checked_out() returns trigger as
$$
begin
    if (
            (NEW.checked_out_at is null and NEW.checked_out_user_id is not null)
            or (NEW.checked_out_at is not null and NEW.checked_out_user_id is null))
    then
        raise exception 'When an event is checked out the checked_out_at and checked_out_user_id fields both must be set.';
    end if;
    return new;
end;
$$ language plpgsql;

create trigger _200_events_checked_out
    before insert or update
    on wb_public.events
    for each row
execute procedure wb_public.validate_events_checked_out();

-- endregion

/* ------------------------------------------------------------------ */

-- region Attributes
create table wb_public.attributes
(
    id                     uuid        not null default uuid_generate_v1mc() primary key,
    metadata               jsonb,
    value                  text,
    value_edited           text,
    value_binary           bytea,
    value_binary_edited    bytea,
    is_binary              boolean     not null default false,
    is_held                boolean     not null default false,
    event_id               uuid        not null,
    field_id               uuid        not null not null,
    field_option_id        uuid,
    field_option_id_edited uuid,
    created_at             timestamptz not null default now(),
    updated_at             timestamptz not null default now()
);

comment on table wb_public.attributes is e'@name attributes_full\n@omit read,update,create,delete,all,many\nAttributes that an event has. For example, title, description, or a set of fields.\nThe value could be an attachment via the binary field.';
comment on column wb_public.attributes.metadata is e'Metadata about the attribute, cannot be edited by analysts';
comment on column wb_public.attributes.value is e'The optional value of the attribute.';
comment on column wb_public.attributes.value_edited is e'The value of the attribute after being edited by an analyst.';
comment on column wb_public.attributes.value_binary is e'Additional, and optional, binary data of the attribute. Could be an image or other media attachment.';
comment on column wb_public.attributes.value_binary_edited is e'The binary value of the attribute after being edited by an analyst.';
comment on column wb_public.attributes.is_binary is e'Whether the attribute is a binary attachment';
comment on column wb_public.attributes.is_held is e'Whether the attribute should be held and not forwarded or distributed';
comment on column wb_public.attributes.event_id is e'The event this attribute is associated to.';
comment on column wb_public.attributes.field_id is e'The field that this attribute is associated to';
comment on column wb_public.attributes.field_option_id is e'The field option that this attribute is associated to';
comment on column wb_public.attributes.field_option_id_edited is e'The field option that this attribute was changed to be after editing by an analyst';

alter table wb_public.attributes
    enable row level security;
alter table wb_public.attributes
    add constraint events_fk foreign key (event_id)
        references wb_public.events (id) match full
        on delete cascade on update cascade;

create index on wb_public.attributes (event_id);
create index on wb_public.attributes (field_id);
create index on wb_public.attributes (field_option_id);
create index on wb_public.attributes (field_option_id_edited);

-- wb_public perms default
create policy access_all on wb_public.attributes to wb_analyst using (true);
grant select on wb_public.attributes to wb_analyst;
grant update (value_edited, value_binary_edited, is_held, field_option_id_edited) on wb_public.attributes to wb_analyst;

-- wb_public perms for wb_admin
grant insert (metadata, value, value_edited, value_binary, value_binary_edited, is_held, event_id, field_id,
              field_option_id, field_option_id_edited) on wb_public.attributes to wb_admin;

create trigger _100_timestamps
    before insert or update
    on wb_public.attributes
    for each row
execute procedure wb_private.tg__timestamps();

alter table wb_public.attributes
    add constraint field_fk foreign key (field_id)
        references wb_public.fields (id) match full on delete restrict on update cascade;

alter table wb_public.attributes
    add constraint field_options_fk foreign key (field_option_id)
        references wb_public.field_options (id) match full on delete restrict on update cascade;

alter table wb_public.attributes
    add constraint field_options_edited_fk foreign key (field_option_id_edited)
        references wb_public.field_options (id) match full on delete restrict on update cascade;

create function wb_public.validate_attribute_field() returns trigger as
$$
declare
    actual_field_type            wb_public.field_types;
    field_option_field_id        uuid;
    field_option_field_id_edited uuid;
begin
    actual_field_type := (select field_type from wb_public.fields where id = NEW.field_id);
    if (actual_field_type = 'option') then
        field_option_field_id := (select field_id from wb_public.field_options where id = NEW.field_option_id);
        field_option_field_id_edited :=
                (select field_id from wb_public.field_options where id = NEW.field_option_id_edited);

        IF (NEW.field_id != field_option_field_id or NEW.field_id != field_option_field_id_edited) then
            raise exception 'Attribute''s field_option_id and field_option_id_edited must belong to the attribute''s field';
        end if;
    end if;
    return new;
end;
$$ language plpgsql;

create trigger _200_attributes_field_matches_option
    before insert or update
    on wb_public.attributes
    for each row
execute procedure wb_public.validate_attribute_field();

create type wb_public.edited_attribute as
(
    id           uuid,
    value_edited text,
    is_held      boolean
);
comment on type wb_public.edited_attribute is e'The subset of fields that an analyst can edit on an attribute.';
comment on column wb_public.edited_attribute.id is e'The id of the attribute being edited';
comment on column wb_public.edited_attribute.value_edited is e'The edited value of the attribute';
comment on column wb_public.edited_attribute.is_held is e'Whether this attribute should be held or not';

-- endregion

/* ------------------------------------------------------------------ */

-- region Reviews
create type wb_public.review_actions as enum ('approve', 'hold', 'archive');

create table wb_public.reviews
(
    id         uuid                     not null default uuid_generate_v1mc() primary key,
    user_id    uuid                     not null,
    event_id   uuid                     not null,
    action     wb_public.review_actions not null,
    reason     text,
    created_at timestamptz                       default now(),
    updated_at timestamptz              not null default now()
);
comment on table wb_public.reviews is 'an event that was sighted in the wild';
comment on column wb_public.reviews.user_id is 'the user that did the review';
comment on column wb_public.reviews.event_id is 'the event that was reviewed';
comment on column wb_public.reviews.action is 'the concluding action as a result of the review';
comment on column wb_public.reviews.reason is 'reasoning supporting the action taken';
comment on column wb_public.reviews.created_at is 'the review was asserted to the database at this time';

alter table wb_public.reviews
    enable row level security;
alter table wb_public.reviews
    add constraint events_fk foreign key (event_id) references wb_public.events (id) on delete restrict on update cascade;
alter table wb_public.reviews
    add constraint users_fk foreign key (user_id) references wb_public.users (id) on delete restrict on update cascade;


create index on wb_public.reviews (event_id);
create index on wb_public.reviews (user_id);

-- wb_public perms default
create policy access_self on wb_public.reviews to wb_analyst using (user_id = wb_public.current_user_id());
grant select on wb_public.reviews to wb_analyst;
grant update (action, reason) on wb_public.reviews to wb_analyst;
grant insert (user_id, event_id, action, reason) on wb_public.reviews to wb_analyst;

-- wb_public perms for wb_senioranalyst
create policy access_all on wb_public.reviews to wb_senioranalyst using (true);
grant insert (user_id, event_id, action, reason) on wb_public.reviews to wb_senioranalyst;

create trigger _100_timestamps
    before insert or update
    on wb_public.reviews
    for each row
execute procedure wb_private.tg__timestamps();
-- endregion
/* ------------------------------------------------------------------ */

-- region Event, Attribute, Reviews Functions

create or replace function wb_public.update_attribute(attr wb_public.edited_attribute)
    returns uuid
as
$$
declare
    otype            wb_public.field_types;
    nfield_option_id uuid;
    ofield_id        uuid;
begin

    ofield_id := (select field_id from wb_public.attributes a where a.id = attr.id);
    otype := (select f.field_type from wb_public.fields f where f.id = ofield_id);

    if (otype = 'text'::wb_public.field_types) then
        update wb_public.attributes set value_edited = attr.value_edited, is_held = attr.is_held where id = attr.id;
    elsif (otype = 'binary'::wb_public.field_types) then
        update wb_public.attributes
        set value_binary_edited = decode(attr.value_edited, 'base64'),
            is_held             = attr.is_held
        where id = attr.id;
    elsif (otype = 'option'::wb_public.field_types) then
        nfield_option_id = attr.value_edited::uuid;
        update wb_public.attributes
        set field_option_id_edited = nfield_option_id,
            is_held                = attr.is_held
        where id = attr.id;
    end if;
    return attr.id;
end ;
$$ LANGUAGE plpgsql VOLATILE
                    SECURITY INVOKER;
comment on function wb_public.update_attribute(attr wb_public.edited_attribute) is e'Edit an attribute''s value.';

/* ------------------------------------------------------------------ */
create or replace view wb_public.attributes_view as
select a.id                                     as id,
       a.is_held,
       a.is_binary,
       a.metadata,
       a.field_id                               as field_id,
       a.event_id                               as event_id,
       coalesce(a.value::text,
                encode(a.value_binary, 'base64'),
                a.field_option_id::text)        as value,
       coalesce(a.value_edited::text,
                encode(a.value_binary_edited, 'base64'),
                a.field_option_id_edited::text) as value_edited,
       a.created_at,
       a.updated_at
from wb_public.attributes a;

grant select on wb_public.attributes_view to wb_analyst;
comment on view wb_public.attributes_view is '@name attributes
@primaryKey id
@foreignKey (event_id) references events (id)
@foreignKey (field_id) references fields (id)
Attributes that an event has. For example, title, description, or a set of fields.';

/* ------------------------------------------------------------------ */

create or replace function wb_public.check_out_event()
    returns wb_public.events
as
$$
update wb_public.events
set checked_out_at      = now(),
    checked_out_user_id = wb_public.current_user_id()

where id = (
    select e.id
    from wb_public.events e
    where e.is_approved = false
      and e.is_locked = false
      and e.is_held = false
      and e.is_archived = false
      and e.checked_out_at is null
    order by created_at
    limit 1
    for update skip locked)
returning *;
$$ language sql volatile
                security definer;

revoke execute on function wb_public.check_out_event() from wb_anonymous, wb_investigator;
grant execute on function wb_public.check_out_event() to wb_analyst;
comment on function wb_public.check_out_event() is
    E'Checks out an event for an analyst to work on. Sets checkedOutAt and checkedOutUserId automatically. Returns null if there are no valid events to check out.';


/* ------------------------------------------------------------------ */

create or replace function wb_public.check_in_event(event_id uuid, action wb_public.review_actions,
                                                    attrs wb_public.edited_attribute[],
                                                    reason text)
    returns wb_public.events
as
$$
declare
    attr                        wb_public.edited_attribute;
    current_checked_out_user_id uuid;
    v_action alias for action;
    v_event_id alias for event_id;
    v_reason alias for reason;
    v_is_approved               boolean;
    v_is_held                   boolean;
    v_is_archived               boolean;
    e                           wb_public.events;
begin
    if ((select wb_public.current_user_id()) is null) then
        raise exception 'no logged in user %', wb_public.current_user_id();
    end if;
    current_checked_out_user_id := (select checked_out_user_id from wb_public.events where id = event_id);

    if (current_checked_out_user_id is null or current_checked_out_user_id != (wb_public.current_user_id())) then
        raise 'this event is not checked out by you. you can only check in events that you have checked out.';
    end if;

    if (v_action is null) then
        raise exception 'action is required';
    end if;

    if (v_event_id is null) then
        raise exception 'an event id is required';
    end if;


    v_is_approved := (v_action is not distinct from 'approve');
    v_is_held := (v_action is not distinct from 'hold');
    v_is_archived := (v_action is not distinct from 'archive');

    foreach attr in array attrs
        loop
            perform wb_public.update_attribute(attr);
        end loop;

    insert into wb_public.reviews (user_id, event_id, action, reason)
    values (wb_public.current_user_id(), v_event_id, v_action, v_reason);

    update wb_public.events
    set checked_out_at      = null,
        checked_out_user_id = null,
        is_approved         = v_is_approved,
        is_held             = v_is_held,
        is_archived         = v_is_archived
    where id = event_id
    returning * into e;
    return e;
end ;
$$ language plpgsql volatile
                    security definer;
revoke execute on function wb_public.check_in_event(uuid, wb_public.review_actions, wb_public.edited_attribute[], text) from wb_anonymous, wb_investigator;
grant execute on function wb_public.check_in_event(uuid, wb_public.review_actions, wb_public.edited_attribute[], text) to wb_analyst;
comment on function wb_public.check_in_event(uuid, wb_public.review_actions, wb_public.edited_attribute[], text) is
    E'Checks an event in along with its edited attributes.';


-- endregion

/* ------------------------------------------------------------------ */

-- region EventTags
create table wb_public.events_tags
(
    tag_id     uuid        not null,
    event_id   uuid        not null,
    created_at timestamptz          default now(),
    updated_at timestamptz not null default now(),
    constraint events_tags_pk primary key (tag_id, event_id)
);
alter table wb_public.tags
    enable row level security;

alter table wb_public.events_tags
    add constraint tags_fk foreign key (tag_id)
        references wb_public.tags (id) match full
        on delete restrict on update cascade;

alter table wb_public.events_tags
    add constraint events_fk foreign key (event_id)
        references wb_public.events (id) match full
        on delete restrict on update cascade;

create index on wb_public.events_tags (tag_id);
create index on wb_public.events_tags (event_id);

comment on constraint events_fk on wb_public.events_tags is E'@manyToManyFieldName events';
comment on constraint tags_fk on wb_public.events_tags is E'@manyToManyFieldName tags';
comment on table wb_public.events_tags is e'@omit all,many\nTag to Event associations';

-- wb_public perms default
create policy access_all on wb_public.events_tags to wb_analyst using (true);
grant select on wb_public.events_tags to wb_analyst;
grant delete on wb_public.events_tags to wb_analyst;
grant insert (tag_id, event_id) on wb_public.events_tags to wb_analyst;


create trigger _100_timestamps
    before insert or update
    on wb_public.events_tags
    for each row
execute procedure wb_private.tg__timestamps();
-- endregion


/**********/


/**********/
/**********/
/**********/
/**********/
/**********/
