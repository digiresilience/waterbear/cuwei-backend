--! Previous: sha1:f4b396de5edd212671e9737dac45dc7404d5f460
--! Hash: sha1:450d06b7cb53016ca128eb7daf9629572b931eab

-- Enter migration here

-- Update export binary view

CREATE OR REPLACE VIEW wb_public.export_binary_view AS
SELECT
  e.event_number,
  f.name,
  COALESCE(
    a.value_binary_edited, a.value_binary) AS value,
  a.id,
  a.metadata,
  e.updated_at,
  STRING_AGG(t.name, ', ') AS tags
FROM
  wb_public.attributes a
  LEFT JOIN wb_public.events e ON a.event_id = e.id
  LEFT JOIN wb_public.fields f ON a.field_id = f.id
  LEFT JOIN wb_public.events_tags et ON e.id = et.event_id
  LEFT JOIN wb_public.tags t ON et.tag_id = t.id
WHERE
  f.name = 'attachments'
  AND e.is_approved = TRUE
  AND a.is_held = FALSE
  GROUP BY 1, 2, 3, 4, 5, 6
ORDER BY
  1, 2, 3, 4, 5, 6;

-- endregion
