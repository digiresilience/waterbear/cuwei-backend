--! Previous: sha1:770c21648c87a8e59e753a98e4bce5ad662bfb32
--! Hash: sha1:8f945b1f5b2b740867a37d74737f3b6f3b757451

-- region Create update event tag function

create or replace function wb_public.update_event_tag(event_id uuid, tag_id uuid, is_held boolean)
    returns uuid
as
$$
declare
  v_event_id alias for event_id;
  v_tag_id alias for tag_id;
  v_is_held alias for is_held;
begin   
    update wb_public.events_tags as et set et.is_held = v_is_held where et.event_id = v_event_id and et.tag_id = v_tag_id;
    
    return v_tag_id;
end ;
$$ LANGUAGE plpgsql VOLATILE
                    SECURITY INVOKER;
comment on function wb_public.update_event_tag(event_id uuid, tag_id uuid, is_held boolean) is e'Edit a tag''s held value.';
