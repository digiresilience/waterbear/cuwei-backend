--! Previous: sha1:8f945b1f5b2b740867a37d74737f3b6f3b757451
--! Hash: sha1:a3a358051eb5102188ed1db98d8baa56017a8460

-- region Create update event tag function

drop function if exists wb_public.update_event_tag(event_id uuid, tag_id uuid, is_held boolean);

create or replace function wb_public.update_event_tag(v_event_id uuid, v_tag_id uuid, v_is_held boolean)
    returns uuid
as
$$
begin   
    update wb_public.events_tags set is_held = v_is_held where event_id = v_event_id and tag_id = v_tag_id;
    
    return v_tag_id;
end ;
$$ LANGUAGE plpgsql VOLATILE
                    SECURITY INVOKER;
comment on function wb_public.update_event_tag(v_event_id uuid, v_tag_id uuid, v_is_held boolean) is e'Edit a tag''s held value.';
