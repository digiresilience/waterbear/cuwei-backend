--! Previous: sha1:450d06b7cb53016ca128eb7daf9629572b931eab
--! Hash: sha1:04f2e547ef19fbfc151617b6148639481cb40d74

-- Enter migration here

-- Count all events

create or replace function wb_public.approved_events_count()
    returns integer
as
$$
begin
    return (select count(*) from wb_public.events where is_approved = true);
end;
$$ language plpgsql stable security definer;

revoke execute on function wb_public.approved_events_count() from wb_anonymous;
grant execute on function wb_public.approved_events_count() to wb_admin, wb_analyst, wb_investigator;
comment on function wb_public.approved_events_count() is
    E'Gets the count of all approved events.';


-- Count recent events

create or replace function wb_public.recent_approved_events_count()
    returns integer
as
$$
begin
    if ((select wb_public.current_user_id()) is null) then
       raise exception 'no logged in user %', wb_public.current_user_id();
    end if;

    return (select count(*) from wb_public.events where is_approved = true and created_at > (select requested_at from wb_public.export_requests where requesting_user_id = wb_public.current_user_id() order by requested_at desc limit 1));
end;
$$ language plpgsql stable security definer;

revoke execute on function wb_public.recent_approved_events_count() from wb_anonymous;
grant execute on function wb_public.recent_approved_events_count() to wb_admin, wb_analyst, wb_investigator;
comment on function wb_public.recent_approved_events_count() is
    E'Gets the count of approved events since the user''s last export.';
