--! Previous: sha1:c1c4d936932891a11e5706003d30ffacaff494fd
--! Hash: sha1:76924e66b36ea239449eff09b0d6edbac48c6d1c

-- Enter migration here

alter table wb_public.export_requests
    enable row level security;
alter table wb_public.export_requests
    add constraint users_fk foreign key (requesting_user_id) references wb_public.users (id) on delete restrict on update cascade;

create index on wb_public.export_requests (requesting_user_id);

create policy access_self on wb_public.export_requests to wb_senioranalyst, wb_analyst, wb_investigator using (requesting_user_id = wb_public.current_user_id());
grant select on wb_public.export_requests to wb_senioranalyst, wb_analyst, wb_investigator;
