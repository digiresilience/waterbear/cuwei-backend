--! Previous: sha1:4f05a8c004cce21da6a1c193ddaf4a3e951ea890
--! Hash: sha1:d8775fb289295c924b0aae55b38897d3e60b5015

-- Enter migration here

create or replace function wb_public.check_out_event_by_id(event_id uuid)
    returns wb_public.events
as
$$
declare    
    v_event_id alias for event_id;
    e wb_public.events;
begin
    if ((select wb_public.current_user_id()) is null) then
        raise exception 'no logged in user %', wb_public.current_user_id();
    end if;
    
    if (v_event_id is null) then
        raise exception 'an event id is required';
    end if;
    
    update wb_public.events
    set checked_out_at      = now(),
    checked_out_user_id     = wb_public.current_user_id()
    where id = event_id
        and checked_out_at is null
	or  checked_out_at + interval '1 hour' < now()
    returning * into e;
    return e;
end;
$$ language plpgsql volatile
                    security definer;

revoke execute on function wb_public.check_out_event_by_id(event_id uuid) from wb_anonymous, wb_investigator;
grant execute on function wb_public.check_out_event_by_id(event_id uuid) to wb_analyst;
comment on function wb_public.check_out_event_by_id(event_id uuid) is
    E'Checks out an event by ID for an analyst to work on. Sets checkedOutAt and checkedOutUserId automatically.';

    
/* ------------------------------------------------------------------ */

create or replace function wb_public.check_out_event()
    returns wb_public.events
as
$$
update wb_public.events
set checked_out_at      = now(),
    checked_out_user_id = wb_public.current_user_id()

where id = (
    select e.id
    from wb_public.events e
    where e.is_approved = false
      and e.is_locked = false
      and e.is_held = false
      and e.is_archived = false
      and e.checked_out_at is null
      or  e.checked_out_at + interval '1 hour' < now()
    order by created_at
    limit 1
    for update skip locked)
returning *;
$$ language sql volatile
                security definer;

revoke execute on function wb_public.check_out_event() from wb_anonymous, wb_investigator;
grant execute on function wb_public.check_out_event() to wb_analyst;
comment on function wb_public.check_out_event() is
    E'Checks out an event for an analyst to work on. Sets checkedOutAt and checkedOutUserId automatically. Returns null if there are no valid events to check out.';
