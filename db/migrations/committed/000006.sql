--! Previous: sha1:9e592f3f72af8a5c74d6616215c9483fd29a9351
--! Hash: sha1:b5d0a65742efcb2607c7cada1e775df633e771fd

-- Enter migration here

create type extended_event as (id uuid, reported_at timestamptz, event_number int, is_archived boolean, is_held boolean, is_approved boolean, created_at timestamptz, description text, description_edited text, reason text);

create or replace function wb_public.events_with_description()
    returns setof extended_event
as
$$
    select
    e.id,
    e.reported_at,
    e.event_number,
    e.is_archived,
    e.is_held,
    e.is_approved,
    e.created_at,
    a.value as description,
    a.value_edited as description_edited,
    rl.reason
    from wb_public.events e
    left join wb_public.attributes a on e.id = a.event_id
    left join wb_public.fields f on a.field_id = f.id
    left join lateral (select reason from wb_public.reviews r where r.event_id = e.id order by r.created_at desc limit 1) rl on true
    where f.name = 'description'
    order by e.created_at desc;
$$ language sql stable;

revoke execute on function wb_public.events_with_description() from wb_anonymous, wb_investigator;
grant execute on function wb_public.events_with_description() to wb_analyst;
comment on function wb_public.events_with_description() is
    E'Returns events with their description attribute.';


-- Check in / out

create or replace function wb_public.check_out_event_by_id(event_id uuid)
    returns wb_public.events
as
$$
declare    
    v_event_id alias for event_id;
    e wb_public.events;
begin
    if ((select wb_public.current_user_id()) is null) then
        raise exception 'no logged in user %', wb_public.current_user_id();
    end if;
    
    if (v_event_id is null) then
        raise exception 'an event id is required';
    end if;
    
    update wb_public.events
    set checked_out_at      = now(),
    checked_out_user_id     = wb_public.current_user_id()
    where id = event_id
        and (checked_out_at is null
	or  checked_out_at + interval '1 hour' < now()
	or  checked_out_user_id = wb_public.current_user_id())
    returning * into e;
    return e;
end;
$$ language plpgsql volatile
                    security definer;

revoke execute on function wb_public.check_out_event_by_id(event_id uuid) from wb_anonymous, wb_investigator;
grant execute on function wb_public.check_out_event_by_id(event_id uuid) to wb_analyst;
comment on function wb_public.check_out_event_by_id(event_id uuid) is
    E'Checks out an event by ID for an analyst to work on. Sets checkedOutAt and checkedOutUserId automatically.';

    
/* ------------------------------------------------------------------ */

create or replace function wb_public.check_out_event()
    returns wb_public.events
as
$$
update wb_public.events
set checked_out_at      = now(),
    checked_out_user_id = wb_public.current_user_id()

where id = (
    select e.id
    from wb_public.events e
    where e.is_approved = false
      and e.is_locked = false
      and e.is_held = false
      and e.is_archived = false
      and (e.checked_out_at is null
      or  e.checked_out_at + interval '1 hour' < now()
      or  checked_out_user_id = wb_public.current_user_id())
    order by created_at
    limit 1
    for update skip locked)
returning *;
$$ language sql volatile
                security definer;

revoke execute on function wb_public.check_out_event() from wb_anonymous, wb_investigator;
grant execute on function wb_public.check_out_event() to wb_analyst;
comment on function wb_public.check_out_event() is
    E'Checks out an event for an analyst to work on. Sets checkedOutAt and checkedOutUserId automatically. Returns null if there are no valid events to check out.';
