--! Previous: sha1:960f985df50fd6936de74100156fb835be9b6a05
--! Hash: sha1:9e592f3f72af8a5c74d6616215c9483fd29a9351

-- Enter migration here

-- region Export Requests

create type wb_public.export_request_status as
    ENUM ('started','inprogress','completed','error');

create table wb_public.export_requests
(
    id                  uuid				not null default uuid_generate_v1mc() primary key,
    requested_at 	timestamptz			not null default now(),
    completed_at 	timestamptz,
    export_request_id	uuid         			not null default uuid_generate_v1mc(),
    requesting_user_id 	uuid				not null,
    download_url	text,
    status		wb_public.export_request_status	not null default 'started',
    created_at          timestamptz  			not null default now(),
    updated_at          timestamptz  			not null default now(),
    constraint export_request_id_unique unique (export_request_id)
);

comment on table  wb_public.export_requests is 'A user request to download the dataset';
comment on column wb_public.export_requests.requested_at is 'The datetime at which the the export was requested. always in utc.';
comment on column wb_public.export_requests.completed_at is 'The datetime at which the the export was completed. always in utc.';
comment on column wb_public.export_requests.export_request_id is 'A uuid to identify the export request.';
comment on column wb_public.export_requests.requesting_user_id is 'The id of the user who requested the export.';
comment on column wb_public.export_requests.download_url is 'The url where the user can download their exported data.';
comment on column wb_public.export_requests.status is 'The current status of the export request.';
comment on column wb_public.export_requests.created_at is 'The datetime at which the export request was inserted into the database. always in utc.';
comment on column wb_public.export_requests.updated_at is 'The datetime at which the export request was last updated in the database. always in utc.';

alter table wb_public.export_requests
    enable row level security;

alter table wb_public.export_requests
    add constraint requesting_user_id_fkey foreign key (requesting_user_id) references wb_public.users on delete restrict;
create index on wb_public.export_requests (requesting_user_id);
create index on wb_public.export_requests (requested_at);

-- wb_public perms for wb_admin
create policy access_all on wb_public.export_requests to wb_admin using (true);
grant update (completed_at, download_url, status) on wb_public.export_requests to wb_admin;
grant select on wb_public.export_requests to wb_admin;

create trigger _100_timestamps
    before insert or update
    on wb_public.export_requests
    for each row
execute procedure wb_private.tg__timestamps();

create trigger _200_export_data
    after insert
    on wb_public.export_requests
    for each row
execute procedure wb_private.tg__add_job('exportTicketDataTask');

-- endregion

-- region Request Export function

create or replace function wb_public.request_export()
    returns wb_public.export_requests
as
$$
declare
    e wb_public.export_requests;
begin
    if ((select wb_public.current_user_id()) is null) then
        raise exception 'no logged in user %', wb_public.current_user_id();
    end if;
    
    insert into wb_public.export_requests
    (requesting_user_id) values (wb_public.current_user_id())
    returning * into e;
    return e; 
end;
$$ language plpgsql volatile
                    security definer;

revoke execute on function wb_public.request_export() from wb_anonymous;
grant execute on function wb_public.request_export() to wb_analyst, wb_investigator;
comment on function wb_public.request_export() is
    E'Creates an export request. Sets requestedAt and requestingUserId automatically.';

-- endregion

-- region Add Event indexes

create index on wb_public.events (is_approved);
create index on wb_public.events (created_at);

-- endregion

-- region Add Export views

CREATE OR REPLACE VIEW wb_public.export_view AS
SELECT
  *
FROM
  crosstab ('SELECT
           	e.event_number,
           	f.name,
           	COALESCE(foe.name, fo.name, a.value_edited, a.value)
           FROM
           	wb_public.attributes a
           	LEFT JOIN wb_public.events e ON a.event_id = e.id
           	LEFT JOIN wb_public.fields f ON a.field_id = f.id
           	LEFT JOIN wb_public.field_options fo ON a.field_option_id = fo.id
           	LEFT JOIN wb_public.field_options foe ON a.field_option_id_edited = foe.id
           WHERE
           	e.is_approved = TRUE
           	AND a.is_held = FALSE
           	ORDER BY 1,2',
    'select distinct name from wb_public.fields WHERE name <> ''submitter_email'' and name <> ''follow_up'' and name <> ''attachments'' and name <> ''sensitive'' ORDER BY NAME') AS ct ("event_number" int,
    "actor" text,
    "additional_info" text,
    "candidate" text,
    "comments" text,
    "communities" text,
    "description" text,
    "disinfo_links" text,
    "geography" text,
    "medium" text,
    "medium_other" text,
    "party" text,
    "platform" text,
    "reason" text,
    "sighted_on" text,
    "tactic" text);


CREATE OR REPLACE VIEW wb_public.export_binary_view AS
SELECT
  e.event_number,
  f.name,
  COALESCE(
    a.value_binary_edited, a.value_binary) AS value,
  a.id,
  a.metadata,
  e.updated_at
FROM
  wb_public.attributes a
  LEFT JOIN wb_public.events e ON a.event_id = e.id
  LEFT JOIN wb_public.fields f ON a.field_id = f.id
WHERE
  f.name = 'attachments'
  AND e.is_approved = TRUE
  AND a.is_held = FALSE
ORDER BY
  e.event_number;

-- endregion
