--! Previous: sha1:ce86d899c9dfb3d0871894a8536a8dc8aae1f610
--! Hash: sha1:4f05a8c004cce21da6a1c193ddaf4a3e951ea890

/* ------------------------------------------------------------------ */

-- region create first user function

create or replace function wb_public.create_first_user (user_email text, user_name text)
    returns setof wb_public.users
as
$$
declare
    user_count int;
begin

    user_count := (select count(id) from wb_public.users);

    if (user_count != 0) then
        raise exception 'Admin user already created';
    end if;

    return query insert into wb_public.users (email, name, user_role, is_active, created_by)
                        values (user_email, user_name, 'admin', true, 'system') returning *;
end ;
$$ LANGUAGE plpgsql VOLATILE
                    SECURITY DEFINER;


comment on function wb_public.create_first_user(user_email text, user_name text) is
    E'Creates the first user with an admin role. Only possible when there are no other users in the database.';

grant execute on function wb_public.create_first_user(user_email text, user_name text) to wb_anonymous;

-- endregion

/* ------------------------------------------------------------------ */
