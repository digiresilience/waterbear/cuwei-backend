--! Previous: sha1:b5d0a65742efcb2607c7cada1e775df633e771fd
--! Hash: sha1:c1c4d936932891a11e5706003d30ffacaff494fd

-- Enter migration here

grant select on wb_public.export_requests to wb_admin, wb_analyst, wb_senioranalyst, wb_investigator;
