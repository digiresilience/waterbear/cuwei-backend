--! Previous: sha1:76924e66b36ea239449eff09b0d6edbac48c6d1c
--! Hash: sha1:f4b396de5edd212671e9737dac45dc7404d5f460

-- Enter migration here

-- Reset

drop view if exists wb_public.export_view;
drop function if exists wb_public.update_event_tag(tag wb_public.edited_tag);
drop function if exists wb_public.check_in_event(event_id uuid, action wb_public.review_actions,
                                                    attrs wb_public.edited_attribute[],
                                                    reason text);
drop function if exists wb_public.check_in_event(event_id uuid, action wb_public.review_actions,
                                                    attrs wb_public.edited_attribute[],
						    tags wb_public.edited_tag[],
                                                    reason text);
drop type if exists wb_public.edited_tag;

-- region Create edited tag type

create type wb_public.edited_tag as
(
    id           uuid,
    is_held      boolean
);
comment on type wb_public.edited_tag is e'The subset of tags that an analyst can edit on an event.';
comment on column wb_public.edited_tag.id is e'The id of the tag being edited';
comment on column wb_public.edited_tag.is_held is e'Whether this tag should be held or not';

-- endregion

-- region Add filter tags to users
alter table wb_public.users add column if not exists filter_tags text;

grant insert (email, name, avatar_url, user_role, is_active, created_by, filter_tags) on wb_public.users to wb_admin;
grant update (email, name, avatar_url, user_role, is_active, created_by, filter_tags) on wb_public.users to wb_admin;

-- endregion


-- region Add held to event

alter table wb_public.events_tags add column if not exists is_held boolean not null default false;

-- endregion


-- region Create update event tag function

create or replace function wb_public.update_event_tag(tag wb_public.edited_tag)
    returns uuid
as
$$
begin   
    update wb_public.events_tags set is_held = tag.is_held where id = tag.id;
    
    return tag.id;
end ;
$$ LANGUAGE plpgsql VOLATILE
                    SECURITY INVOKER;
comment on function wb_public.update_event_tag(tag wb_public.edited_tag) is e'Edit a tag''s value.';

-- endregion


-- region Update export views

CREATE OR REPLACE VIEW wb_public.export_view AS
SELECT
  *
FROM
  crosstab ('SELECT
           	e.event_number,
           	STRING_AGG(t.name, '', '') as tags,
           	f.name,           	
           	STRING_AGG(COALESCE(foe.name, fo.name, a.value_edited, a.value), '', '')
           FROM
           	wb_public.attributes a
           	LEFT JOIN wb_public.events e ON a.event_id = e.id
           	LEFT JOIN wb_public.events_tags et ON e.id = et.event_id
           	LEFT JOIN wb_public.tags t ON et.tag_id = t.id
           	LEFT JOIN wb_public.fields f ON a.field_id = f.id
           	LEFT JOIN wb_public.field_options fo ON a.field_option_id = fo.id
           	LEFT JOIN wb_public.field_options foe ON a.field_option_id_edited = foe.id	   
           WHERE
           	e.is_approved = TRUE
           	AND a.is_held = FALSE
		        GROUP BY 1,3
           	ORDER BY 1,3', 'SELECT name FROM (VALUES (''actor''), (''additional_info''), (''candidate''), (''comments''), (''communities''),(''description''), (''disinfo_links''), (''geography''), (''medium''), (''medium_other''), (''party''), (''platform''), (''reason''), (''sighted_on''), (''tactic'')) AS results(name)') AS ct ("event_number" int,
           	"tags" text,
    "actor" text,
    "additional_info" text,
    "candidate" text,
    "comments" text,
    "communities" text,
    "description" text,
    "disinfo_links" text,
    "geography" text,
    "medium" text,
    "medium_other" text,
    "party" text,
    "platform" text,
    "reason" text,
    "sighted_on" text,
    "tactic" text);

-- endregion

-- region Update check in function

create or replace function wb_public.check_in_event(event_id uuid, action wb_public.review_actions,
                                                    attrs wb_public.edited_attribute[],
						    tags wb_public.edited_tag[],
                                                    reason text)
    returns wb_public.events
as
$$
declare
    attr                        wb_public.edited_attribute;
    tag                         wb_public.edited_tag;
    current_checked_out_user_id uuid;
    v_action alias for action;
    v_event_id alias for event_id;
    v_reason alias for reason;
    v_is_approved               boolean;
    v_is_held                   boolean;
    v_is_archived               boolean;
    e                           wb_public.events;
begin
    if ((select wb_public.current_user_id()) is null) then
        raise exception 'no logged in user %', wb_public.current_user_id();
    end if;
    current_checked_out_user_id := (select checked_out_user_id from wb_public.events where id = event_id);

    if (current_checked_out_user_id is null or current_checked_out_user_id != (wb_public.current_user_id())) then
        raise 'this event is not checked out by you. you can only check in events that you have checked out.';
    end if;

    if (v_action is null) then
        raise exception 'action is required';
    end if;

    if (v_event_id is null) then
        raise exception 'an event id is required';
    end if;


    v_is_approved := (v_action is not distinct from 'approve');
    v_is_held := (v_action is not distinct from 'hold');
    v_is_archived := (v_action is not distinct from 'archive');

    foreach attr in array attrs
        loop
            perform wb_public.update_attribute(attr);
        end loop;

    foreach tag in array tags
        loop
            perform wb_public.update_event_tag(tag);
        end loop;

    insert into wb_public.reviews (user_id, event_id, action, reason)
    values (wb_public.current_user_id(), v_event_id, v_action, v_reason);

    update wb_public.events
    set checked_out_at      = null,
        checked_out_user_id = null,
        is_approved         = v_is_approved,
        is_held             = v_is_held,
        is_archived         = v_is_archived
    where id = event_id
    returning * into e;
    return e;
end ;
$$ language plpgsql volatile
                    security definer;
revoke execute on function wb_public.check_in_event(uuid, wb_public.review_actions, wb_public.edited_attribute[], wb_public.edited_tag[], text) from wb_anonymous, wb_investigator;
grant execute on function wb_public.check_in_event(uuid, wb_public.review_actions, wb_public.edited_attribute[], wb_public.edited_tag[], text) to wb_analyst;
comment on function wb_public.check_in_event(uuid, wb_public.review_actions, wb_public.edited_attribute[], wb_public.edited_tag[], text) is
    E'Checks an event in along with its edited attributes and tags';


-- endregion
