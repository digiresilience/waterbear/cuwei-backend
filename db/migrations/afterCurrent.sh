#!/bin/bash
set -eu

# this is disabled for now

#psql -Xv ON_ERROR_STOP=1 "${GM_DBURL}" <<EOF
#EOF


#INSERT INTO wb_public.users(email, name, user_role, is_active, created_by)
#VALUES('abelxluck@gmail.com', 'Abel', 'admin'::wb_public.role_type, true, 'afterReset Hook')
#on conflict (email) do nothing;
#
#INSERT INTO wb_public.users(email, name, user_role, is_active, created_by)
#VALUES('darren@redaranj.com', 'Darren', 'admin'::wb_public.role_type, true, 'afterReset Hook')
#on conflict (email) do nothing;
#
#INSERT INTO wb_public.users(email, name, user_role, is_active, created_by)
#VALUES('analyst@example.com', 'Analyst Andy', 'core_analyst', true, 'afterReset Hook')
#on conflict (email) do nothing;
#
#INSERT INTO wb_public.users(email, name, user_role, is_active, created_by)
#VALUES('senioranalyst@example.com', 'Senior Analyst Sandy', 'senior_core_analyst', true, 'afterReset Hook')
#on conflict (email) do nothing;
#
#INSERT INTO wb_public.users(email, name, user_role, is_active, created_by)
#VALUES('investigator@example.com', 'Investigator Ingrid', 'investigator', true, 'afterReset Hook')
#on conflict (email) do nothing;
#
#INSERT INTO wb_public.users(email, name, user_role, is_active, created_by)
#VALUES('anon@example.com', 'Anonymous Anon', 'none', true, 'afterReset Hook')
#on conflict (email) do nothing;
