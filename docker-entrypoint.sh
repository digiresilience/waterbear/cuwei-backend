#!/bin/bash

set -e

cd "${APP_DIR}"

if [[ "$1" == "test" ]]; then
  bash ./test/wait-until-ready.sh
  npm run test
elif [[ "$1" == "server" ]]; then
  echo "starting cuwei-backend..."
  yarn db:migrate
  exec dumb-init yarn start
elif [[ "$1" == "worker" ]]; then
  echo "starting cuwei-worker..."
  exec dumb-init yarn worker
fi
