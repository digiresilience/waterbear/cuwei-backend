// eslint-disable-next-line import/no-extraneous-dependencies
import jwt from "jsonwebtoken";

export default function createToken(key, kid, payload) {
  return jwt.sign(payload, key, {
    noTimestamp: true,
    algorithm: "RS256",
    header: { alg: "RS256", kid }
  });
}
