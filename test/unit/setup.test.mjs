/* eslint-disable import/prefer-default-export */

class AbortTx extends Error {
  constructor(...args) {
    super(...args);
    this.abort = true;
  }
}
let unitDb;

// used for running unit tests when the app context is not running
export const withUnitDb = fn => async () => {
  try {
    await unitDb.tx(async tx => {
      await fn(tx);
      throw new AbortTx();
    });
  } catch (e) {
    if (!e.abort) throw e;
  }
};
// cause unhandled promise rejections to throw
process.on("unhandledRejection", up => {
  throw up;
});

// global before hook
before(async () => {
  // unitDb = initializePGP(config.get("db").rootConnection);
});

//  global after hook
after(async () => {
  // await unitDb.$pool.end();
});
