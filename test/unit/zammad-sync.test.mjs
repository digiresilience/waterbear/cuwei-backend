import R from "ramda";
import chai from "chai";
import sinon from "sinon";
import {
  xGenerateFieldToFieldMapping,
  fieldSpecsToGrahphqlFields,
  mapFieldOption,
  mapField,
  pluckOptionIdsFor,
  castArrays,
  ticketArticlesToEventAttrs
} from "../../src/tasks/zammad-sync.mjs";

const { assert } = chai;

const fixtures = {
  description: {
    id: "description",
    type: "longtext",
    required: true,
    for: "submitter",
    displayName: "What did you see or hear?",
    hint: "Describe it",
    name: "Description",
    hidden: false,
    default: null
  },
  geography: {
    id: "geography",
    displayName: "Where were you when it happened?",
    hint: "State",
    type: "select",
    options: [
      { id: "AL", name: "Alabama" },
      { id: "AK", name: "Alaska" }
    ],
    for: "submitter",
    required: false,
    name: "Geography",
    hidden: false,
    default: null
  },

  sensitive: {
    id: "sensitive",
    name: "Sensitive",
    type: "select",
    for: "analyst",
    options: [
      { id: "sensitive", name: "Yes" },
      { id: "not-sensitive", name: "No" }
    ],
    required: false,
    hidden: false,
    default: null
  }
};

describe("fieldSpecsToGrahphqlFields", () => {
  it("works in the simple case", () => {
    assert.deepEqual(
      fieldSpecsToGrahphqlFields(["foo", "bar"], ["foo", "bar"]),
      "foo\nbar"
    );
  });
  it("errors when a field is missing", () => {
    assert.throws(
      () =>
        fieldSpecsToGrahphqlFields(["foo", "bar"], ["foo", "bar", "not-there"]),
      /not-there/
    );
  });

  it("allows zammad to have extra fields", () => {
    assert.deepEqual(
      fieldSpecsToGrahphqlFields(["foo", "bar", "extra"], ["foo", "bar"]),
      "foo\nbar"
    );
  });
});
describe("pluckOptionsFor", () => {
  it("get the options for a field id", () => {
    const r = pluckOptionIdsFor([fixtures.sensitive], "sensitive");
    assert.deepEqual(r, ["sensitive", "not-sensitive"]);
  });
});

describe("mapField", () => {
  it("converts text field spec into field table row type", () => {
    const field = fixtures.description;
    const r = mapField(field);
    assert.deepEqual(r, {
      name: field.id,
      field_type: "text",
      display_name: field.name,
      description: field.displayName
    });
  });
  it("converts select field spec into field table row type", () => {
    const field = fixtures.sensitive;
    const r = mapField(field);
    assert.deepEqual(r, {
      name: field.id,
      field_type: "option",
      display_name: field.name,
      description: field.displayName
    });
  });
});

describe("mapFieldOption", () => {
  it("maps the options to field options", () => {
    const field = fixtures.sensitive;
    const r = mapFieldOption(field.id)(field.options[0]);
    assert.deepEqual(r, {
      name: field.options[0].id,
      display_name: field.options[0].name,
      description: null,
      field_id: field.id
    });
  });
});

describe("castArrays", () => {
  it("works", () => {
    const r = castArrays(R.values(fixtures))({
      description: "foo bar",
      geography: "AK",
      sensitive: null
    });
    assert.deepEqual(r, {
      description: "foo bar",
      geography: ["AK"],
      sensitive: null
    });
  });
});

describe("xGenerateFieldToFieldMapping", () => {
  it("works", async () => {
    const stub = sinon.stub();
    stub.onCall(0).returns(
      // this mocks the db query that looks up the fields
      R.curry(a =>
        Promise.resolve([
          {
            id: "d2a83e58-5f98-11ea-ae94-4b0e2c9a060c",
            name: "description",
            description: "What did you see or hear?",
            fieldType: "text",
            displayName: "Description",
            isArchived: false,
            createdAt: "2020-03-06T10:54:16.208Z",
            updatedAt: "2020-03-09T12:39:42.879Z"
          },
          {
            id: "d2a8d9f8-5f98-11ea-ae94-53ff406f4e99",
            name: "geography",
            description: "Where were you when it happened?",
            fieldType: "option",
            displayName: "Geography",
            isArchived: false,
            createdAt: "2020-03-06T10:54:16.208Z",
            updatedAt: "2020-03-09T12:39:42.879Z"
          },
          {
            id: "d2a8d9f8-5f98-11ea-ae94-53ff406f4e9A",
            name: "sensitive",
            description: "",
            fieldType: "option",
            displayName: "Sensitive",
            isArchived: false,
            createdAt: "2020-03-06T10:54:16.208Z",
            updatedAt: "2020-03-09T12:39:42.879Z"
          }
        ])
      )
    );
    stub.onCall(1).returns("curry call");
    stub.onCall(2).returns(
      // this mocks the db query that looks up the field_options
      R.curry(a =>
        Promise.resolve([
          {
            id: "d2b5b98e-5f98-11ea-ae94-afd086e1efb1",
            name: "AL",
            description: null,
            displayName: "Alabama",
            isArchived: false,
            createdAt: "2020-03-06T10:54:16.230Z",
            updatedAt: "2020-03-09T12:41:23.505Z"
          },
          {
            id: "d2b5cd5c-5f98-11ea-ae94-2f056d5c0acf",
            name: "AK",
            description: null,
            displayName: "Alaska",
            isArchived: false,
            createdAt: "2020-03-06T10:54:16.230Z",
            updatedAt: "2020-03-09T12:41:23.505Z"
          }
        ])
      )
    );
    stub.onCall(3).returns(
      // this mocks the db query that looks up the field_options
      R.curry(a =>
        Promise.resolve([
          {
            id: "d2b5cd5c-5f98-11ea-ae94-2f056d5c0acf",
            name: "sensitive",
            description: null,
            displayName: "Alaska",
            isArchived: false,
            createdAt: "2020-03-06T10:54:16.230Z",
            updatedAt: "2020-03-09T12:41:23.505Z"
          },
          {
            id: "d2b5b98e-5f98-11ea-ae94-afd086e1efb1",
            name: "not-sensitive",
            description: null,
            displayName: "Alabama",
            isArchived: false,
            createdAt: "2020-03-06T10:54:16.230Z",
            updatedAt: "2020-03-09T12:41:23.505Z"
          }
        ])
      )
    );
    const fieldSpecs = [
      fixtures.description,
      fixtures.geography,
      fixtures.sensitive
    ];
    const r = await xGenerateFieldToFieldMapping(fieldSpecs, stub);

    assert.deepEqual(r, {
      description: {
        id: "d2a83e58-5f98-11ea-ae94-4b0e2c9a060c",
        type: "text",
        specType: "longtext",
        options: undefined
      },
      geography: {
        id: "d2a8d9f8-5f98-11ea-ae94-53ff406f4e99",
        type: "option",
        specType: "select",
        options: {
          AL: "d2b5b98e-5f98-11ea-ae94-afd086e1efb1",
          AK: "d2b5cd5c-5f98-11ea-ae94-2f056d5c0acf"
        }
      },
      sensitive: {
        id: "d2a8d9f8-5f98-11ea-ae94-53ff406f4e9A",
        type: "option",
        specType: "select",
        options: {
          "not-sensitive": "d2b5b98e-5f98-11ea-ae94-afd086e1efb1",
          sensitive: "d2b5cd5c-5f98-11ea-ae94-2f056d5c0acf"
        }
      }
    });
  });
});

describe("ticketArticlesToEventAttrs", () => {
  it("works", () => {
    const ticket = {
      id: 3,
      created_at: "2020-02-21T20:28:56.195",
      description: "",
      disinfo_links: [],
      sighted_on: null,
      geography: ["AK"],
      reason: "",
      medium: [],
      medium_other: "",
      additional_info: "",
      follow_up: ["urgent"],
      submitter_email: "",
      sensitive: ["sensitive"],
      tactic: ["defamation", "inauthentic-identity"],
      platform: [],
      party: ["CP", "DNC", "GP"],
      candidate: [],
      actor: [],
      communities: [],
      state: { name: "open", id: 2 },
      ticket_articles: {
        nodes: [
          {
            id: 17,
            from: "Tiffany O",
            to: "",
            subject: "",
            body: "this should be skipped",
            contentType: "text/html",
            createdAt: "2020-02-24T16:03:05.404",
            createdById: 10,
            updatedAt: "2020-02-24T16:04:06.787",
            internal: false,
            sender: { id: 1, name: "Agent" }
          },
          {
            id: 38,
            from: "Fabby M",
            to: "",
            subject: "",
            body: "Who can see this?",
            contentType: "text/html",
            createdAt: "2020-02-25T19:48:05.361",
            createdById: 11,
            updatedAt: "2020-02-25T19:48:05.361",
            internal: false,
            sender: { id: 1, name: "Agent" }
          }
        ]
      }
    };
    const r = ticketArticlesToEventAttrs({
      comments: { id: "ABC" }
    })({ ticket, attributes: [] });
    assert.deepEqual(r, {
      ticket,
      attributes: [
        {
          event_id: null,
          field_id: "ABC",
          field_option_id: null,
          is_binary: false,
          metadata: {
            author: "Fabby M",
            commented_at: "2020-02-25T19:48:05.361"
          },
          value: "Who can see this?",
          value_binary: null
        }
      ]
    });
  });
});
