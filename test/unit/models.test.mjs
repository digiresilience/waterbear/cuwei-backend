import chai from "chai";
import User from "../../src/models/user.mjs";
import { selectPgRole } from "../../src/middleware/postgraphile.mjs";

const { assert } = chai;

describe("a thing", () => {
  it("should do a thing", async () => {
    assert.equal(1, 1);
  });
});

describe("Model::User", () => {
  it("validator", async () => {
    const appuser = User(null);
    const user = await appuser.validate({
      email: "notanemail",
      id: 1,
      created_by: "test"
    });
    assert.isAtLeast(user.error.details.length, 2);
  });
});

describe("select pg role", () => {
  it("returns an actual role in the happy case", () =>
    assert.equal("wb_admin", selectPgRole({ userRole: "admin" })));

  it("returns anon role when null", () =>
    assert.equal("wb_anonymous", selectPgRole()));

  it("returns anon role when empty", () =>
    assert.equal("wb_anonymous", selectPgRole({})));

  it("returns anon role when invalid", () =>
    assert.equal("wb_anonymous", selectPgRole({ userRole: "foobar" })));
});
