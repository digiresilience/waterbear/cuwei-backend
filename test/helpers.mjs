/* eslint-disable import/no-extraneous-dependencies */
import chai from "chai";
import cd from "chai-datetime";
// import pg from "pg";
// import gm from "graphile-migrate";
// import config from "../src/config.js";

const { assert: _assert } = chai;
chai.use(cd);

export const ass = (e, v, m) => {
  if (v instanceof Date) return _assert.equalDate(e, v, m);
  return _assert.equal(e, v, m);
};

export const assert = _assert;

/*
const getGmSettings = () => ({
  connectionString: config.get("db.connection"),
  rootConnectionString: config.get("db.rootConnection"),
  pgSettings: {
    search_path: "wb_public,wb_private,wb_hidden,public"
  },
  placeholders: {
    ":DATABASE_AUTHENTICATOR": config.get("postgraphile.auth"),
    ":DATABASE_VISITOR": config.get("postgraphile.visitor")
  },
  afterReset: [],
  afterAllMigrations: [],
  afterCurrent: []
});

export const doesDbExist = async () => {
  try {
    const client = new pg.Client({
      database: "postgres",
      connectionString: config.get("db").rootConnection
    });
    await client.connect();
    const res = await client.query(
      `SELECT FROM pg_database WHERE datname = 'waterbear_test'`
    );
    const result = res.rowCount === 1;
    await client.end();
    return result;
  } catch (e) {
    throw new Error(
      "Could not connect to test database. Is postgres running?",
      e
    );
  }
};

export const createTestDb = async () => {
  const gmConf = getGmSettings();
  process.chdir("./db");
  await gm.reset(gmConf, false);
  await gm.migrate(gmConf, false);
  await gm.watch(gmConf, true);
  process.chdir("..");
};
*/
export const createUsers = db =>
  db.tx(t => {
    const queries = [
      ["admin@test.com", "Admin", "admin", true],
      ["coreanalyst@test.com", "Core Analyst", "core_analyst", true],
      [
        "senioranalyst@test.com",
        "Senior Core Analyst",
        "senior_core_analyst",
        true
      ],
      ["investigator@test.com", "Investigator", "investigator", true],
      ["anonymous@test.com", "Anon", "none", true]
    ].map(u =>
      t.one(
        "INSERT INTO wb_public.users(email, name, user_role, is_active, created_by) VALUES($1, $2, $3, $4, 'test') RETURNING id",
        u
      )
    );

    return t.batch(queries);
  });
