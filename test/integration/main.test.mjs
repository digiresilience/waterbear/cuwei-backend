import supertest from "supertest";
import chai from "chai";
import {
  withDb,
  adminAuth,
  coreAnalystAuth,
  start,
  teardown,
  server,
  token
} from "./setup.test.mjs";

const { assert } = chai;
const cfHeader = "Cf-Access-Authenticated-User-Email";

describe("authorization tests", () => {
  before(start);
  after(teardown);

  it("should throw 401 if no valid CF JWT found in", async () =>
    supertest(server())
      .get("/")
      .expect(401));

  it("should throw 401 when header is missing even if cloudflare cookie is set", async () =>
    supertest(server())
      .get("/")
      .set("Cookie", [`CF_Authorization=${token}`])
      .expect(401));

  it("should throw 401 when cookie is missing even if cloudflare header is set", async () =>
    supertest(server())
      .get("/")
      .set(cfHeader, "alice@test.com")
      .expect(401));

  it("should should throw a 401 when cloudflare header is empty", async () => {
    return supertest(server())
      .get("/")
      .set(cfHeader, "")
      .set("Cookie", [`CF_Authorization=${token}`])
      .expect(401);
  });

  it("should should throw a 401 with valid token, valid header but no-such user", async () => {
    return supertest(server())
      .get("/")
      .set(cfHeader, "nobody@test.com")
      .set("Cookie", [`CF_Authorization=${token}`])
      .expect(401);
  });

  it(
    "should work with valid CF JWT",
    withDb(async db => {
      return supertest(server())
        .get("/")
        .set(adminAuth)
        .set("Cookie", [`CF_Authorization=${token}`])
        .expect(200);
    })
  );
});

describe("root endpoint", () => {
  before(start);
  after(teardown);

  it("should output hello world", async () =>
    withDb(async db => {
      assert.equal(1, 1);
      return supertest(server())
        .get("/")
        .set(coreAnalystAuth)
        .expect(200, { hello: "world" })
        .expect("Content-Type", /json/);
    }));
});
