import jwksEndpoint from "../mocks/jwks.mjs";
import { publicKey, privateKey } from "../mocks/keys.mjs";
import createToken from "../mocks/tokens.mjs";
import { setupApp } from "../../src/server.mjs";
import config from "../../src/config.mjs";
import { createUsers } from "../helpers.mjs";

// cause unhandled promise rejections to throw
process.on("unhandledRejection", up => {
  throw up;
});

const app = setupApp();
let serverInstance;

export const getDb = () => app.context.pgp;
export const getOwnerPgPool = () => app.context.ownerPgPool;
export const getPostGraphilePool = () => app.context.graphqlPgPool;

// used when running integration tests, the actual db handle the app uses
export const withDb = fn => async () => fn(getDb());

before(async () => {
  await createUsers(getDb());
});

//  global after hook
after(async () => {
  // await getDb().$pool.end();
  await getPostGraphilePool().end();
  await getOwnerPgPool().end();
});

export const start = async () => {
  serverInstance = app.listen();
};
export const teardown = async () => {
  app.terminate();
};

export const server = () => serverInstance;

export const token = createToken(privateKey, "123", {
  sub: "john",
  email: "admin@test.com",
  aud: config.get("cfaccess.audience"),
  iss: config.get("cfaccess.url")
});
jwksEndpoint("http://localhost", [{ pub: publicKey, kid: "123" }]);

export const [adminAuth, coreAnalystAuth, investigatorAuth, anonAuth] = [
  "admin@test.com",
  "coreanalyst@test.com",
  "investigator@test.com",
  "anonymous@test.com"
].map(email => ({
  "Cf-Access-Authenticated-User-Email": email,
  Cookie: [`CF_Authorization=${token}`]
}));
