import joi from "@hapi/joi";
import nassert from "assert";
import { withDb, start, teardown } from "./setup.test.mjs";
import { ass, assert } from "../helpers.mjs";
import User from "../../src/models/user.mjs";
import Sessions from "../../src/models/session.mjs";

describe(
  "Model::User",
  withDb(async db => {
    const AppUser = User(db);
    before(start);
    after(teardown);

    it("fail on invalid email address", async () => {
      const user = await AppUser.create({ email: "notanemail" });
      assert.instanceOf(user, joi.ValidationError);
    });

    it("create should create and find user", async () => {
      const user = await AppUser.create({
        email: "foo@example.com",
        name: "Foo",
        created_by: "test",
        is_active: true,
        user_role: "admin"
      });
      assert.equal(user.email, "foo@example.com");

      const user2 = await AppUser.findByEmail("foo@example.com");
      AppUser.props.forEach(prop => ass(user2[prop], user[prop], `=${prop}`));

      const user3 = await AppUser.findById(user.id);
      AppUser.props.forEach(prop => ass(user3[prop], user[prop], `=${prop}`));
    });

    it("should error on duplicate email", async () => {
      nassert.rejects(async () => {
        await AppUser.create({
          email: "foo@example.com",
          name: "Foo Dupe",
          created_by: "test",
          is_active: true,
          user_role: "admin"
        });
      }, /duplicate key value violates unique constraint/);
    });
  })
);

describe("Model::Session", () => {
  it(
    "creates sessions",
    withDb(async db => {
      const AppUser = User(db);
      const user = await AppUser.create({
        email: "foosession@example.com",
        name: "Foo",
        created_by: "test",
        is_active: true,
        user_role: "admin"
      });
      const sessions = Sessions(db);
      const { uuid } = await sessions.createSession(user.id);
      assert.typeOf(uuid, "string");
      assert.lengthOf(uuid, 36); // uuidv4 is 36 chars long with delimiters
    })
  );
});
